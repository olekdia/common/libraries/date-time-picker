package com.olekdia.sample

import com.olekdia.datetimepicker.interfaces.DtRangeChangeListener
import com.olekdia.datetimepicker.interfaces.HeaderRangeSelectionListener

open class BaseDtRangeFrag : BaseDtFrag() {

    protected var headerRangeSelectionListener: HeaderRangeSelectionListener? = null
    protected var dtRangeChangeListener: DtRangeChangeListener? = null

    fun setHeaderRangeListener(
        headerRangeSelectionListener: HeaderRangeSelectionListener
    ) {
        this.headerRangeSelectionListener = headerRangeSelectionListener
    }

    fun setDateTimeChangeListener(dtRangeChangeListener: DtRangeChangeListener) {
        this.dtRangeChangeListener = dtRangeChangeListener
    }
}