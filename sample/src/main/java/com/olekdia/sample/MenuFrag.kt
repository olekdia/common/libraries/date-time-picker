package com.olekdia.sample

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment

class MenuFrag : Fragment() {

    private var listener: View.OnClickListener? = null

    fun setListener(listener: View.OnClickListener) {
        this.listener = listener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val view: View = inflater.inflate(R.layout.fragment_menu, null)

        view.findViewById<TextView>(R.id.date).setOnClickListener(listener)
        view.findViewById<TextView>(R.id.time).setOnClickListener(listener)
        view.findViewById<TextView>(R.id.dt).setOnClickListener(listener)
        view.findViewById<TextView>(R.id.month).setOnClickListener(listener)
        view.findViewById<TextView>(R.id.year).setOnClickListener(listener)

        view.findViewById<TextView>(R.id.date_range).setOnClickListener(listener)
        view.findViewById<TextView>(R.id.time_range).setOnClickListener(listener)
        view.findViewById<TextView>(R.id.dt_range).setOnClickListener(listener)
        view.findViewById<TextView>(R.id.month_range).setOnClickListener(listener)

        return view
    }

    companion object {
        const val TAG = "MENU_FRAG_TAG"
    }
}