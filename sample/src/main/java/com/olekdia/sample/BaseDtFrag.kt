package com.olekdia.sample

import androidx.fragment.app.Fragment
import com.olekdia.androidcommon.extensions.AR_LANG
import com.olekdia.androidcommon.extensions.FA_LANG
import com.olekdia.common.NumeralSystem
import com.olekdia.datetimepicker.Picker
import java.util.*

open class BaseDtFrag : Fragment() {

    lateinit var picker: Picker
        protected set

    protected val numSystem: Int
        get() {
            return when (Locale.getDefault().language) {
                AR_LANG -> NumeralSystem.EASTERN_ARABIC
                FA_LANG -> NumeralSystem.PERSO_ARABIC
                else -> NumeralSystem.WESTERN_ARABIC
            }
        }
}