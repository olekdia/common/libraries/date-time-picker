package com.olekdia.sample

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.olekdia.datetimepicker.Picker
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.common.PickerIndex
import com.olekdia.datetimepicker.common.RangePart
import com.olekdia.datetimepicker.interfaces.DtChangeListener
import com.olekdia.datetimepicker.interfaces.DtRangeChangeListener
import com.olekdia.datetimepicker.interfaces.HeaderRangeSelectionListener
import com.olekdia.datetimepicker.interfaces.HeaderSelectionListener
import com.olekdia.sample.dtFragments.*

class SampleActivity :
    AppCompatActivity(),
    View.OnClickListener,
    HeaderSelectionListener,
    HeaderRangeSelectionListener,
    DtChangeListener,
    DtRangeChangeListener {

    private var currFragIndex: Int = MENU_FRAG
    private var currDtFrag: BaseDtFrag? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        setContentView(R.layout.activity_sample)

        savedInstanceState?.let {
            currFragIndex = it.getInt(CURR_FRAG_INDEX_KEY)
        }
        updateFragment()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(CURR_FRAG_INDEX_KEY, currFragIndex)
    }

    private fun updateFragment() {
        val fragmentManager = supportFragmentManager

        var fragment: Fragment?
        val tag: String

        when (currFragIndex) {
            DATE_FRAG -> {
                fragment = fragmentManager.findFragmentByTag(DateFrag.TAG)
                fragment = fragment ?: DateFrag()
                tag = DateFrag.TAG
            }

            TIME_FRAG -> {
                fragment = fragmentManager.findFragmentByTag(TimeFrag.TAG)
                fragment = fragment ?: TimeFrag()
                tag = TimeFrag.TAG
            }

            DT_FRAG -> {
                fragment = fragmentManager.findFragmentByTag(DtFrag.TAG)
                fragment = fragment ?: DtFrag()
                tag = DtFrag.TAG
            }

            MONTH_FRAG -> {
                fragment = fragmentManager.findFragmentByTag(MonthFrag.TAG)
                fragment = fragment ?: MonthFrag()
                tag = MonthFrag.TAG
            }

            YEAR_FRAG -> {
                fragment = fragmentManager.findFragmentByTag(YearFrag.TAG)
                fragment = fragment ?: YearFrag()
                tag = YearFrag.TAG
            }

            DATE_RANGE_FRAG -> {
                fragment = fragmentManager.findFragmentByTag(DateRangeFrag.TAG)
                fragment = fragment ?: DateRangeFrag()
                tag = DateRangeFrag.TAG
            }

            TIME_RANGE_FRAG -> {
                fragment = fragmentManager.findFragmentByTag(TimeRangeFrag.TAG)
                fragment = fragment ?: TimeRangeFrag()
                tag = TimeRangeFrag.TAG
            }

            DT_RANGE_FRAG -> {
                fragment = fragmentManager.findFragmentByTag(DtRangeFrag.TAG)
                fragment = fragment ?: DtRangeFrag()
                tag = DtRangeFrag.TAG
            }

            MONTH_RANGE_FRAG -> {
                fragment = fragmentManager.findFragmentByTag(MonthRangeFrag.TAG)
                fragment = fragment ?: MonthRangeFrag()
                tag = MonthRangeFrag.TAG
            }

            else -> {
                fragment = fragmentManager.findFragmentByTag(MenuFrag.TAG)
                fragment = fragment ?: MenuFrag()
                tag = MenuFrag.TAG
            }
        }

        (fragment as? BaseDtWithoutRangeFrag)?.setDateTimeChangeListener(this)
        (fragment as? BaseDtRangeFrag)?.setDateTimeChangeListener(this)

        (fragment as? BaseDtWithoutRangeFrag)?.setHeaderListener(this)
        (fragment as? BaseDtRangeFrag)?.setHeaderRangeListener(this)

        (fragment as? MenuFrag)?.setListener(this)

        fragment?.let {
            (it as? BaseDtFrag)
                ?.let { baseDtFrag ->
                    currDtFrag = baseDtFrag
                }

            if (!it.isAdded) {
                val transaction = fragmentManager.beginTransaction()

                transaction.replace(
                    R.id.fragment_container,
                    it,
                    tag
                )

                transaction.commit()
            }
        }

        invalidateOptionsMenu()
    }

//--------------------------------------------------------------------------------------------------
//  OptionsMenu
//--------------------------------------------------------------------------------------------------

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.sample_activity_menu, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        when (currFragIndex) {
            MENU_FRAG -> {
                menu.findItem(R.id.back_to_menu).isVisible = false

                menu.findItem(R.id.selected_date).isVisible = false
                menu.findItem(R.id.selected_time).isVisible = false
                menu.findItem(R.id.selected_dt).isVisible = false

                menu.findItem(R.id.selected_start_date).isVisible = false
                menu.findItem(R.id.selected_start_time).isVisible = false
                menu.findItem(R.id.selected_start_dt).isVisible = false

                menu.findItem(R.id.selected_end_date).isVisible = false
                menu.findItem(R.id.selected_end_time).isVisible = false
                menu.findItem(R.id.selected_end_dt).isVisible = false
            }
            in DATE_FRAG..YEAR_FRAG -> {
                menu.findItem(R.id.back_to_menu).isVisible = true

                menu.findItem(R.id.selected_date).isVisible = true
                menu.findItem(R.id.selected_time).isVisible = true
                menu.findItem(R.id.selected_dt).isVisible = true

                menu.findItem(R.id.selected_start_date).isVisible = false
                menu.findItem(R.id.selected_start_time).isVisible = false
                menu.findItem(R.id.selected_start_dt).isVisible = false

                menu.findItem(R.id.selected_end_date).isVisible = false
                menu.findItem(R.id.selected_end_time).isVisible = false
                menu.findItem(R.id.selected_end_dt).isVisible = false
            }
            in DATE_RANGE_FRAG..MONTH_RANGE_FRAG -> {
                menu.findItem(R.id.back_to_menu).isVisible = true

                menu.findItem(R.id.selected_date).isVisible = false
                menu.findItem(R.id.selected_time).isVisible = false
                menu.findItem(R.id.selected_dt).isVisible = false

                menu.findItem(R.id.selected_start_date).isVisible = true
                menu.findItem(R.id.selected_start_time).isVisible = true
                menu.findItem(R.id.selected_start_dt).isVisible = true

                menu.findItem(R.id.selected_end_date).isVisible = true
                menu.findItem(R.id.selected_end_time).isVisible = true
                menu.findItem(R.id.selected_end_dt).isVisible = true
            }
        }

        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(@NonNull item: MenuItem): Boolean {
        if (item.itemId == R.id.back_to_menu) {
            currFragIndex = MENU_FRAG
            updateFragment()
        }

        val picker: Picker = currDtFrag?.picker ?: return false

        when (item.itemId) {
            R.id.selected_date -> {
                val date: Triple<Int, Int, Int> = picker.getSelectedDate()
                val text: String = dateToStr(date)
                showToast(text)
            }
            R.id.selected_time -> {
                val time: Triple<Int, Int, Int> = picker.getSelectedTime()
                val text: String = timeToStr(time)
                showToast(text)
            }
            R.id.selected_dt -> {
                val dt: DateTime = picker.getSelectedDt()
                val text: String = dtToStr(dt)
                showToast(text)
            }


            R.id.selected_start_date -> {
                val date: Triple<Int, Int, Int>? = picker.getSelectedStartDate()

                val text: String = date?.let {
                    dateToStr(it)
                } ?: "invalid"

                showToast(text)
            }
            R.id.selected_start_time -> {
                val time: Triple<Int, Int, Int>? = picker.getSelectedStartTime()

                val text: String = time?.let {
                    timeToStr(it)
                } ?: "invalid"

                showToast(text)
            }
            R.id.selected_start_dt -> {
                val dt: DateTime? = picker.getSelectedStartDt()

                val text: String = dt?.let {
                    dtToStr(it)
                } ?: "invalid"

                showToast(text)
            }


            R.id.selected_end_date -> {
                val date: Triple<Int, Int, Int>? = picker.getSelectedEndDate()

                val text: String = date?.let {
                    dateToStr(it)
                } ?: "invalid"

                showToast(text)
            }
            R.id.selected_end_time -> {
                val time: Triple<Int, Int, Int>? = picker.getSelectedEndTime()

                val text: String = time?.let {
                    timeToStr(it)
                } ?: "invalid"

                showToast(text)
            }
            R.id.selected_end_dt -> {
                val dt: DateTime? = picker.getSelectedEndDt()

                val text: String = dt?.let {
                    dtToStr(it)
                } ?: "invalid"

                showToast(text)
            }
        }

        return super.onOptionsItemSelected(item)
    }

//--------------------------------------------------------------------------------------------------
//  DtsChangeListener, HeaderSelectionListener, OnClickListener
//--------------------------------------------------------------------------------------------------

    override fun onDateChanged(
        year: Int, month: Int, day: Int,
        hour: Int, minute: Int, second: Int
    ) {
        val text: String = dateToStr(year, month, day) +
                " - " +
                timeToStr(hour, minute, second)
        showToast(text)
    }

    override fun onRangeDateChanged(
        startYear: Int, startMonth: Int, startDay: Int,
        startHour: Int, startMinute: Int, startSecond: Int,

        endYear: Int, endMonth: Int, endDay: Int,
        endHour: Int, endMinute: Int, endSecond: Int
    ) {
        val text: String =
            dateToStr(startYear, startMonth, startDay) +
                    " - " +
                    timeToStr(startHour, startMinute, startSecond) +
                    "\n" +
                    dateToStr(endYear, endMonth, endDay) +
                    " - " +
                    timeToStr(endHour, endMinute, endSecond)

        showToast(text)
    }

    override fun onHeaderSelection(index: Int) {
        val text: String = "header selection changed:\nindex - ${indexToItemTitle(index)}"
        showToast(text)
    }

    override fun onHeaderSelection(index: Int, part: Int) {
        val text: String = "header selection changed:\n" +
                "index - ${indexToItemTitle(index)}, part - ${partToRangePartTitle(part)}"
        showToast(text)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.date ->
                currFragIndex = DATE_FRAG

            R.id.time ->
                currFragIndex = TIME_FRAG

            R.id.dt ->
                currFragIndex = DT_FRAG

            R.id.month ->
                currFragIndex = MONTH_FRAG

            R.id.year ->
                currFragIndex = YEAR_FRAG

            R.id.date_range ->
                currFragIndex = DATE_RANGE_FRAG

            R.id.time_range ->
                currFragIndex = TIME_RANGE_FRAG

            R.id.dt_range ->
                currFragIndex = DT_RANGE_FRAG

            R.id.month_range ->
                currFragIndex = MONTH_RANGE_FRAG
        }

        updateFragment()
    }

//--------------------------------------------------------------------------------------------------
//  Common
//--------------------------------------------------------------------------------------------------

    private fun showToast(text: String) {
        val toast: Toast = Toast.makeText(
            applicationContext,
            text,
            Toast.LENGTH_LONG
        )
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
    }

    private fun dateToStr(year: Int, month: Int, day: Int): String =
        "$year $month $day"

    private fun dateToStr(date: Triple<Int, Int, Int>): String =
        dateToStr(date.first, date.second, date.third)

    private fun timeToStr(hour: Int, minute: Int, second: Int): String =
        "$hour:$minute:$second"

    private fun timeToStr(time: Triple<Int, Int, Int>): String =
        timeToStr(time.first, time.second, time.third)

    private fun dtToStr(dt: DateTime): String =
        dateToStr(dt.year, dt.month, dt.day) +
                " - " +
                timeToStr(dt.hour24, dt.minute, dt.second)

    private fun indexToItemTitle(index: Int): String =
        when (index) {
            PickerIndex.YEAR -> "year"
            PickerIndex.MONTH -> "month"
            PickerIndex.DAY -> "day"

            PickerIndex.HOUR -> "hour"
            PickerIndex.MINUTE -> "minute"
            PickerIndex.SECOND -> "second"

            else -> "unknown"
        }

    private fun partToRangePartTitle(part: Int): String =
        when (part) {
            RangePart.START -> "start"
            RangePart.END -> "end"
            else -> "unknown"
        }

    companion object {
        const val CURR_FRAG_INDEX_KEY = "CURR FRAG INDEX KEY"

        const val MENU_FRAG = 0

        const val DATE_FRAG = 1
        const val TIME_FRAG = 2
        const val DT_FRAG = 3
        const val MONTH_FRAG = 4
        const val YEAR_FRAG = 5

        const val DATE_RANGE_FRAG = 6
        const val TIME_RANGE_FRAG = 7
        const val DT_RANGE_FRAG = 8
        const val MONTH_RANGE_FRAG = 9
    }
}