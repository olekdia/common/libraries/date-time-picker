package com.olekdia.sample

import com.olekdia.datetimepicker.interfaces.DtChangeListener
import com.olekdia.datetimepicker.interfaces.HeaderSelectionListener

open class BaseDtWithoutRangeFrag : BaseDtFrag() {

    protected var headerSelectionListener: HeaderSelectionListener? = null
    protected var dtChangeListener: DtChangeListener? = null

    fun setHeaderListener(
        headerSelectionListener: HeaderSelectionListener
    ) {
        this.headerSelectionListener = headerSelectionListener
    }

    fun setDateTimeChangeListener(dtChangeListener: DtChangeListener) {
        this.dtChangeListener = dtChangeListener
    }
}