package com.olekdia.sample.dtFragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.olekdia.datetimepicker.PickerBuilder
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.sample.BaseDtWithoutRangeFrag

class TimeFrag : BaseDtWithoutRangeFrag() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val dt = DateTime(System.currentTimeMillis())

        picker = PickerBuilder(requireContext())
            .vibrate(true)
            .is24HourMode(true)
            .allowAutoAdvance(true)
            .amPmText("AM", "PM")
            .selectedTime(dt.hour24, dt.minute)
            .also {
                headerSelectionListener?.let { listener ->
                    it.headerSelectionListener(listener)
                }

//                dtChangeListener?.let { listener ->
//                    it.dtChangeListener(listener)
//                }
            }
            .build()

        return picker
    }

    companion object {
        const val TAG = "TIME_FRAG_TAG"
    }
}