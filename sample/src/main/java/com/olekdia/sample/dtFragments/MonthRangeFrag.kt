package com.olekdia.sample.dtFragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.olekdia.datetimepicker.PickerRangeBuilder
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.sample.BaseDtRangeFrag

class MonthRangeFrag : BaseDtRangeFrag() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val dtStart = DateTime(System.currentTimeMillis())
        val dtEnd = DateTime(System.currentTimeMillis() + 1000 * 60 * 60 * 24)

        picker = PickerRangeBuilder(requireContext())
            .vibrate(true)
//            .currentItem(DAY)
//            .also {
//                dtRangeChangeListener?.let { listener ->
//                    it.dtRangeChangeListener(listener)
//                }
//            }
            .selectedStartDate(year = dtStart.year, month = dtStart.month)
            .selectedEndDate(year = dtEnd.year, month = dtEnd.month)
            .build()

        return picker
    }

    companion object {
        const val TAG = "MONTH_RANGE_FRAG_TAG"
    }
}