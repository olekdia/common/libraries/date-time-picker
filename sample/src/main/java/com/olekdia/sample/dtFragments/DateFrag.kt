package com.olekdia.sample.dtFragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.olekdia.datetimepicker.PickerBuilder
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.sample.BaseDtWithoutRangeFrag
import java.util.*

class DateFrag : BaseDtWithoutRangeFrag() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val dt = DateTime(System.currentTimeMillis())

        picker = PickerBuilder(requireContext())
            .vibrate(true)
            .firstDayOfWeek(Calendar.WEDNESDAY)
//            .currentItem(YEAR)
//            .also {
//                dtChangeListener?.let { listener ->
//                    it.dtChangeListener(listener)
//                }
//            }
            .selectedDate(dt.year, dt.month, dt.day)
            .build()

        return picker
    }

    companion object {
        const val TAG = "DATE_FRAG_TAG"
    }
}