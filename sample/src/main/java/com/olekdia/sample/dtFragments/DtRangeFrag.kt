package com.olekdia.sample.dtFragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.olekdia.datetimepicker.PickerRangeBuilder
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.sample.BaseDtRangeFrag

class DtRangeFrag : BaseDtRangeFrag() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val dtStart = DateTime(System.currentTimeMillis())
        val dtEnd = DateTime(System.currentTimeMillis() + 1000 * 60 * 60 * 24)

        picker = PickerRangeBuilder(requireContext())
            .vibrate(true)
            .selectedStartDate(dtStart.year, dtStart.month, dtStart.day)
            .selectedStartTime(dtStart.hour24, dtStart.minute, dtStart.second)
            .selectedEndDate(dtEnd.year, dtEnd.month, dtEnd.day)
            .selectedEndTime(dtEnd.hour24, dtEnd.minute, dtEnd.second)
            .also {
                headerRangeSelectionListener?.let { listener ->
                    it.headerRangeSelectionListener(listener)
                }

//                dtRangeChangeListener?.let { listener ->
//                    it.dtRangeChangeListener(listener)
//                }
            }
            .build()

        return picker
    }

    companion object {
        const val TAG = "DT_RANGE_FRAG_TAG"
    }
}