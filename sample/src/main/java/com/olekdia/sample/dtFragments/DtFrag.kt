package com.olekdia.sample.dtFragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.olekdia.common.NumeralSystem.Companion.EASTERN_ARABIC
import com.olekdia.common.NumeralSystem.Companion.PERSO_ARABIC
import com.olekdia.common.NumeralSystem.Companion.WESTERN_ARABIC
import com.olekdia.datetimepicker.PickerBuilder
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.sample.BaseDtWithoutRangeFrag
import java.util.*

class DtFrag : BaseDtWithoutRangeFrag() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val dt = DateTime(System.currentTimeMillis())

        picker = PickerBuilder(requireContext())
            .vibrate(true)
            .numeralSystem(
                when (Locale.getDefault().language) {
                    "ar" -> EASTERN_ARABIC
                    "fa" -> PERSO_ARABIC
                    else -> WESTERN_ARABIC
                }
            )
            .selectedDate(dt.year, dt.month, dt.day)
            .selectedTime(dt.hour24, dt.minute, dt.second)
            .also {
                headerSelectionListener?.let { listener ->
                    it.headerSelectionListener(listener)
                }

//                dtChangeListener?.let { listener ->
//                    it.dtChangeListener(listener)
//                }
            }
            .build()

        return picker
    }

    companion object {
        const val TAG = "DT_FRAG_TAG"
    }
}