package com.olekdia.sample.dtFragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.olekdia.datetimepicker.PickerRangeBuilder
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.sample.BaseDtRangeFrag

class TimeRangeFrag : BaseDtRangeFrag() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val dtStart = DateTime(System.currentTimeMillis())
        val dtEnd = DateTime(System.currentTimeMillis() + 1000 * 60 * 60)

        picker = PickerRangeBuilder(requireContext())
            .vibrate(true)
//            .currentItem(HOUR)
//            .also {
//                dtRangeChangeListener?.let { listener ->
//                    it.dtRangeChangeListener(listener)
//                }
//            }
            .selectedStartTime(dtStart.hour24, dtStart.minute, dtStart.second)
            .selectedEndTime(dtEnd.hour24, dtEnd.minute, dtEnd.second)
            .build()

        return picker
    }

    companion object {
        const val TAG = "TIME_RANGE_FRAG_TAG"
    }
}