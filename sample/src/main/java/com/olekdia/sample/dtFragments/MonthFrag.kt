package com.olekdia.sample.dtFragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.olekdia.datetimepicker.PickerBuilder
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.sample.BaseDtWithoutRangeFrag

class MonthFrag : BaseDtWithoutRangeFrag() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val dt = DateTime(System.currentTimeMillis())

        picker = PickerBuilder(requireContext())
            .vibrate(true)
//            .currentItem(DAY)
//            .also {
//                dtChangeListener?.let { listener ->
//                    it.dtChangeListener(listener)
//                }
//            }
            .selectedDate(year = dt.year, month = dt.month)
            .build()

        return picker
    }

    companion object {
        const val TAG = "MONTH_FRAG_TAG"
    }
}