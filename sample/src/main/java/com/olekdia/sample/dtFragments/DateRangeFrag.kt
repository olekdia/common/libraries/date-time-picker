package com.olekdia.sample.dtFragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.olekdia.datetimepicker.PickerRangeBuilder
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.sample.BaseDtRangeFrag

class DateRangeFrag : BaseDtRangeFrag() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val dtStart = DateTime(System.currentTimeMillis())
        val dtEnd = DateTime(System.currentTimeMillis() + 1000 * 60 * 60 * 24)

        picker = PickerRangeBuilder(requireContext())
            .vibrate(true)
//            .currentItem(YEAR)
//            .also {
//                dtRangeChangeListener?.let { listener ->
//                    it.dtRangeChangeListener(listener)
//                }
//            }
            .selectedStartDate(dtStart.year, dtStart.month, dtStart.day)
            .selectedEndDate(dtEnd.year, dtEnd.month, dtEnd.day)
            .build()

        return picker
    }

    companion object {
        const val TAG = "DATE_RANGE_FRAG_TAG"
    }
}