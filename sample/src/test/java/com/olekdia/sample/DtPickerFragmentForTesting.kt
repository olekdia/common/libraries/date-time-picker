package com.olekdia.sample

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.olekdia.datetimepicker.PickerBuilder
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.common.PickerIndex

class DtPickerFragmentForTesting : BaseDtPickerFragmentForTesting() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val currItem: Int = arguments?.getInt(CURR_ITEM_KEY) ?: PickerIndex.YEAR

        val dt = DateTime(
            arguments?.getLong(DT_MILLIS_KEY) ?: System.currentTimeMillis()
        )

        picker = PickerBuilder(requireContext())
            .selectedDt(dt)
            .currentItem(currItem)
            .build()

        arguments = null

        return picker
    }
}