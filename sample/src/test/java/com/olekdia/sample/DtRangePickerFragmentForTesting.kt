package com.olekdia.sample

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.olekdia.datetimepicker.PickerRangeBuilder
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.common.PickerIndex
import com.olekdia.datetimepicker.common.RangePart

class DtRangePickerFragmentForTesting : BaseDtPickerFragmentForTesting() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val currItem: Int = arguments?.getInt(CURR_ITEM_KEY) ?: PickerIndex.YEAR
        val currPart: Int = arguments?.getInt(CURR_PART_KEY) ?: RangePart.START

        val startDt = DateTime(
            arguments?.getLong(START_DT_MILLIS_KEY) ?: System.currentTimeMillis()
        )
        val endDt = DateTime(
            arguments?.getLong(END_DT_MILLIS_KEY) ?: System.currentTimeMillis()
        )

        picker = PickerRangeBuilder(requireContext())
            .selectedStartDt(startDt)
            .selectedEndDt(endDt)
            .currentItem(currItem)
            .currentRangePart(currPart)
            .build()

        arguments = null

        return picker
    }
}