package com.olekdia.sample

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

open class BaseDtPickerFragmentForTesting : BaseDtWithoutRangeFrag() {

    fun show(fm: FragmentManager, args: Bundle?, tag: String?) {
        arguments = args

        val ft: FragmentTransaction = fm.beginTransaction()
        ft.add(this, tag)
        ft.commit()
    }

    companion object {
        const val TAG = "DT_FRAG_FOR_TESTING"

        const val CURR_ITEM_KEY = "CURR_ITEM_KEY"

        const val DT_MILLIS_KEY = "DT_MILLIS_KEY"

        const val CURR_PART_KEY = "CURR_PART_KEY"
        const val START_DT_MILLIS_KEY = "START_DT_MILLIS_KEY"
        const val END_DT_MILLIS_KEY = "END_DT_MILLIS_KEY"
    }
}