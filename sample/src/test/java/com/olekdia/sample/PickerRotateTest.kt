package com.olekdia.sample

import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.test.core.app.ActivityScenario
import androidx.test.platform.app.InstrumentationRegistry
import com.olekdia.datetimepicker.common.PickerIndex
import com.olekdia.datetimepicker.common.RangePart
import com.olekdia.sample.BaseDtPickerFragmentForTesting.Companion.CURR_ITEM_KEY
import com.olekdia.sample.BaseDtPickerFragmentForTesting.Companion.CURR_PART_KEY
import com.olekdia.sample.BaseDtPickerFragmentForTesting.Companion.DT_MILLIS_KEY
import com.olekdia.sample.BaseDtPickerFragmentForTesting.Companion.END_DT_MILLIS_KEY
import com.olekdia.sample.BaseDtPickerFragmentForTesting.Companion.START_DT_MILLIS_KEY
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.annotation.LooperMode

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE, sdk = [Build.VERSION_CODES.KITKAT])
@LooperMode(LooperMode.Mode.LEGACY)
class PickerRotateTest {

    private lateinit var context: Context
    private lateinit var scenario: ActivityScenario<SampleActivity>

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
        scenario = ActivityScenario.launch(SampleActivity::class.java)
    }

    @Test
    fun `create picker, without range, curr item is day, dt, rotate - curr item and dt are valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, PickerIndex.DAY)
            putLong(DT_MILLIS_KEY, 1000L)
        }

        scenario.onActivity { activity ->
            val fragment = DtPickerFragmentForTesting()
            fragment.show(activity.supportFragmentManager, args, BaseDtPickerFragmentForTesting.TAG)

            assertEquals(PickerIndex.DAY, fragment.picker.currentItem)
            assertEquals(1000L, fragment.picker.getSelectedDt().timeInMillis)
        }

        scenario.recreate()

        scenario.onActivity { activity ->
            val fragment: DtPickerFragmentForTesting = activity
                .supportFragmentManager
                .findFragmentByTag(BaseDtPickerFragmentForTesting.TAG) as DtPickerFragmentForTesting

            assertEquals(PickerIndex.DAY, fragment.picker.currentItem)
            assertEquals(1000L, fragment.picker.getSelectedDt().timeInMillis)
        }
    }

    @Test
    fun `create picker, with range, curr item is day, curr part is end, dt, rotate - curr item, part and dt are valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, PickerIndex.DAY)
            putInt(CURR_PART_KEY, RangePart.END)
            putLong(START_DT_MILLIS_KEY, 1000L)
            putLong(END_DT_MILLIS_KEY, 2000L)
        }

        scenario.onActivity { activity ->
            val fragment = DtRangePickerFragmentForTesting()
            fragment.show(activity.supportFragmentManager, args, BaseDtPickerFragmentForTesting.TAG)

            assertEquals(PickerIndex.DAY, fragment.picker.currentItem)
            assertEquals(RangePart.END, fragment.picker.currentRangePart)

            assertEquals(1000L, fragment.picker.getSelectedStartDt()?.timeInMillis)
            assertEquals(2000L, fragment.picker.getSelectedEndDt()?.timeInMillis)
        }

        scenario.recreate()

        scenario.onActivity { activity ->
            val fragment: DtRangePickerFragmentForTesting = activity
                .supportFragmentManager
                .findFragmentByTag(BaseDtPickerFragmentForTesting.TAG) as DtRangePickerFragmentForTesting

            assertEquals(PickerIndex.DAY, fragment.picker.currentItem)
            assertEquals(RangePart.END, fragment.picker.currentRangePart)

            assertEquals(1000L, fragment.picker.getSelectedStartDt()?.timeInMillis)
            assertEquals(2000L, fragment.picker.getSelectedEndDt()?.timeInMillis)
        }
    }
}