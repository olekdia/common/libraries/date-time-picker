plugins {
    id("com.android.application")
    kotlin("android")
}

android {
    compileSdk = Versions.sdk.compile
    buildToolsVersion = Versions.buildTools

    defaultConfig {
        minSdk = Versions.sdk.min
        targetSdk = Versions.sdk.target
        applicationId = "com.olekdia.sample"
        versionCode = 1
        versionName = "1.0.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

    testOptions.unitTests.isIncludeAndroidResources = true

    compileOptions {
        encoding = "UTF-8"
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    implementation(kotlin("stdlib", Versions.kotlin))
    implementation(Libs.olekdia.common_jvm)
    implementation(Libs.olekdia.common_android)
    implementation(project(":date-time-picker"))

    implementation(Libs.androidx.annotations)
    implementation(Libs.androidx.fragment)
    implementation(Libs.androidx.appcompat)
    implementation(Libs.androidx.material)

    testImplementation(Libs.junit)
    testImplementation(Libs.robolectric)
    testImplementation(Libs.androidx.test_core)
    testImplementation(Libs.androidx.test_runner)
    testImplementation(Libs.androidx.test_rules)
}