package com.olekdia.datetimepicker

import android.content.Context
import com.olekdia.common.extensions.ifNotNull
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.common.NOT_SET
import com.olekdia.datetimepicker.common.RangePart.Companion.END
import com.olekdia.datetimepicker.common.RangePart.Companion.START
import com.olekdia.datetimepicker.interfaces.DtRangeChangeListener
import com.olekdia.datetimepicker.interfaces.HeaderRangeSelectionListener

class PickerRangeBuilder(
    context: Context
) : BasePickerBuilder<PickerRangeBuilder>(context) {

    override val self: PickerRangeBuilder
        get() = this

//--------------------------------------------------------------------------------------------------
//  BaseController
//--------------------------------------------------------------------------------------------------

    @JvmOverloads
    fun selectedStartDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): PickerRangeBuilder {
        setSelectedDate(
            START,
            year, month, day
        )

        return this
    }

    @JvmOverloads
    fun selectedStartTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerRangeBuilder {
        setSelectedTime(
            START,
            hour, minute, second
        )

        return this
    }

    @JvmOverloads
    fun selectedStartDateTime(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerRangeBuilder {
        setSelectedDate(
            START,
            year, month, day
        )
        setSelectedTime(
            START,
            hour, minute, second
        )

        return this
    }

    fun selectedStartDt(dt: DateTime): PickerRangeBuilder {
        setSelectedDt(
            START,
            dt
        )

        return this
    }

    @JvmOverloads
    fun selectedEndDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): PickerRangeBuilder {
        setSelectedDate(
            END,
            year, month, day
        )

        return this
    }

    @JvmOverloads
    fun selectedEndTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerRangeBuilder {
        setSelectedTime(
            END,
            hour, minute, second
        )

        return this
    }

    @JvmOverloads
    fun selectedEndDateTime(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerRangeBuilder {
        setSelectedDate(
            END,
            year, month, day
        )
        setSelectedTime(
            END,
            hour, minute, second
        )

        return this
    }

    fun selectedEndDt(dt: DateTime): PickerRangeBuilder {
        setSelectedDt(
            END,
            dt
        )

        return this
    }

    fun currentRangePart(currentRangePart: Int): PickerRangeBuilder {
        this.currentRangePart = currentRangePart
        return this
    }

//--------------------------------------------------------------------------------------------------
//  HeaderController
//--------------------------------------------------------------------------------------------------

    override var isRangeAvailable: Boolean = true

//--------------------------------------------------------------------------------------------------
//  Common
//--------------------------------------------------------------------------------------------------

    //selectable

    fun selectableStartDate(vararg dates: Triple<Int, Int, Int>): PickerRangeBuilder {
        setSelectableDate(
            START,
            *dates
        )

        return this
    }

    fun selectableStartTime(vararg times: Triple<Int, Int, Int>): PickerRangeBuilder {
        setSelectableTime(
            START,
            *times
        )

        return this
    }

    fun selectableStartDt(selectableDt: Array<DateTime>?): PickerRangeBuilder {
        setSelectableDt(
            START,
            selectableDt
        )

        return this
    }

    fun selectableEndDate(vararg dates: Triple<Int, Int, Int>): PickerRangeBuilder {
        setSelectableDate(
            END,
            *dates
        )

        return this
    }

    fun selectableEndTime(vararg times: Triple<Int, Int, Int>): PickerRangeBuilder {
        setSelectableTime(
            END,
            *times
        )

        return this
    }

    fun selectableEndDt(selectableDt: Array<DateTime>?): PickerRangeBuilder {
        setSelectableDt(
            END,
            selectableDt
        )

        return this
    }

    //min

    fun minStartDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): PickerRangeBuilder {
        setMinDate(
            START,
            year, month, day
        )

        return this
    }

    fun minStartTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerRangeBuilder {
        setMinTime(
            START,
            hour, minute, second
        )

        return this
    }

    fun minStartDateTime(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerRangeBuilder {
        setMinDate(
            START,
            year, month, day
        )
        setMinTime(
            START,
            hour, minute, second
        )

        return this
    }

    fun minStartDt(minDt: DateTime?): PickerRangeBuilder {
        setMinDt(
            START,
            minDt
        )

        return this
    }

    fun minEndDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): PickerRangeBuilder {
        setMinDate(
            END,
            year, month, day
        )

        return this
    }

    fun minEndTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerRangeBuilder {
        setMinTime(
            END,
            hour, minute, second
        )

        return this
    }

    fun minEndDateTime(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerRangeBuilder {
        setMinDate(
            END,
            year, month, day
        )
        setMinTime(
            END,
            hour, minute, second
        )

        return this
    }

    fun minEndDt(minDt: DateTime?): PickerRangeBuilder {
        setMinDt(
            END,
            minDt
        )

        return this
    }

    //max

    fun maxStartDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): PickerRangeBuilder {
        setMaxDate(
            START,
            year, month, day
        )

        return this
    }

    fun maxStartTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerRangeBuilder {
        setMaxTime(
            START,
            hour, minute, second
        )

        return this
    }

    fun maxStartDateTime(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerRangeBuilder {
        setMaxDate(
            START,
            year, month, day
        )
        setMaxTime(
            START,
            hour, minute, second
        )

        return this
    }

    fun maxStartDt(maxDt: DateTime?): PickerRangeBuilder {
        setMaxDt(
            START,
            maxDt
        )

        return this
    }

    fun maxEndDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): PickerRangeBuilder {
        setMaxDate(
            END,
            year, month, day
        )

        return this
    }

    fun maxEndTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerRangeBuilder {
        setMaxTime(
            END,
            hour, minute, second
        )

        return this
    }

    fun maxEndDateTime(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerRangeBuilder {
        setMaxDate(
            END,
            year, month, day
        )
        setMaxTime(
            END,
            hour, minute, second
        )

        return this
    }

    fun maxEndDt(maxDt: DateTime?): PickerRangeBuilder {
        setMaxDt(
            END,
            maxDt
        )

        return this
    }

    fun headerRangeSelectionListener(
        headerRangeSelectionListener: HeaderRangeSelectionListener
    ): PickerRangeBuilder {
        this.headerRangeSelectionListener = headerRangeSelectionListener
        return this
    }

    fun dtRangeChangeListener(dtRangeChangeListener: DtRangeChangeListener): PickerRangeBuilder {
        this.dtRangeChangeListener = dtRangeChangeListener
        return this
    }

//--------------------------------------------------------------------------------------------------
//  Requirements
//--------------------------------------------------------------------------------------------------

    override fun checkIsValuesValid() {
        super.checkIsValuesValid()

        checkStartEndHaveSameItems()

        checkSelected()
        checkSelectable()
        checkMinMax(END)
        checkMinStartEnd()
        checkMaxStartEnd()
    }

    private fun checkStartEndHaveSameItems() {
        for (i in startDtItemsAvailability.indices) {
            require(
                startDtItemsAvailability[i] == endDtItemsAvailability[i]
            ) { "range start and end must have the same items" }
        }
    }

    private fun checkSelected() {
        require(
            selectedDts[START] <= selectedDts[END]
        ) { "start selected dt > end selected dt" }
    }

    private fun checkSelectable() {
        selectableDt[START]?.let {
            require(
                selectableDt[END] != null
            ) { "start selectable exists, so end selectable must exist too" }
        }

        selectableDt[END]?.let {
            require(
                selectableDt[START] != null
            ) { "end selectable exists, so start selectable must exist too" }
        }

        ifNotNull(
            selectableDt[START],
            selectableDt[END]
        ) { start, end ->

            require(
                start.isNotEmpty() && end.isNotEmpty()
            ) { "selectable can't be empty" }

            if (start.isNotEmpty() && end.isNotEmpty()) {
                require(
                    start[0] <= end[0]
                ) { "start min selectable > end min selectable" }

                require(
                    start[start.size - 1] <= end[end.size - 1]
                ) { "start max selectable > end max selectable" }
            }
        }
    }

    private fun checkMinStartEnd() {
        minDt[START]?.let {
            require(
                minDt[END] != null
            ) { "start min exists, so end min must exist too" }
        }

        minDt[END]?.let {
            require(
                minDt[START] != null
            ) { "end min exists, so start min must exist too" }
        }

        ifNotNull(
            minDt[START],
            minDt[END]
        ) { startMin, endMin ->
            require(
                startMin <= endMin
            ) { "start min > end min" }
        }
    }

    private fun checkMaxStartEnd() {
        maxDt[START]?.let {
            require(
                maxDt[END] != null
            ) { "start max exists, so end max must exist too" }
        }

        maxDt[END]?.let {
            require(
                maxDt[START] != null
            ) { "end max exists, so start max must exist too" }
        }

        ifNotNull(
            maxDt[START],
            maxDt[END]
        ) { startMax, endMax ->
            require(
                startMax <= endMax
            ) { "start max > end max" }
        }
    }
}