package com.olekdia.datetimepicker.time

import android.animation.Keyframe
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.animation.ValueAnimator.AnimatorUpdateListener
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Paint.Align
import android.graphics.Typeface
import android.view.View
import com.olekdia.androidcommon.extensions.getDimensionPixelSize
import com.olekdia.androidcommon.extensions.getFloat
import com.olekdia.androidcommon.extensions.getTypeface
import com.olekdia.common.INVALID
import com.olekdia.datetimepicker.R
import com.olekdia.datetimepicker.interfaces.TimePickerController
import kotlin.math.min
import kotlin.math.sqrt

class RadialTextsView(context: Context) :
    View(context) {

    private val validPaint = Paint()
    private val invalidPaint = Paint()
    private val highlightedPaint = Paint()

    private var drawValuesReady: Boolean = false
    private var isInitialized: Boolean = false

    internal var selection: Int = INVALID
    private var validator: SelectionValidator? = null

    private var typefaceLight: Typeface? = null
    private var typefaceRegular: Typeface? = null

    private var texts: Array<String>? = null
    private var innerTexts: Array<String>? = null

    private var is24HourMode: Boolean = false
    private var hasInnerCircle: Boolean = false

    private var circleRadiusMultiplier: Float = 0F
    private var amPmCircleRadiusMultiplier: Float = 0F
    private var numbersRadiusMultiplier: Float = 0F
    private var innerNumbersRadiusMultiplier: Float = 0F
    private var textSizeMultiplier: Float = 0F
    private var innerTextSizeMultiplier: Float = 0F

    private var xCenter: Float = 0F
    private var yCenter: Float = 0F
    private var circleRadius: Float = 0F

    private var textGridValuesDirty: Boolean = false
    private var textSize: Float = 0F
    private var innerTextSize: Float = 0F
    private val textGridHeights: FloatArray = FloatArray(7)
    private val textGridWidths: FloatArray = FloatArray(7)
    private val innerTextGridHeights: FloatArray = FloatArray(7)
    private val innerTextGridWidths: FloatArray = FloatArray(7)

    private var animationRadiusMultiplier: Float = 0F
        set(value) {
            field = value
            textGridValuesDirty = true
        }

    private var transitionMidRadiusMultiplier: Float = 0F
    private var transitionEndRadiusMultiplier: Float = 0F

    internal var disappearAnimator: ObjectAnimator? = null
        get() = if (!isInitialized || !drawValuesReady || field == null) {
            null
        } else {
            field
        }
    internal var reappearAnimator: ObjectAnimator? = null
        get() = if (!isInitialized || !drawValuesReady || field == null) {
            null
        } else {
            field
        }

    private var invalidateUpdateListener: InvalidateUpdateListener? = null

    internal fun initialize(
        context: Context,
        texts: Array<String>, innerTexts: Array<String>?,
        controller: TimePickerController,
        validator: SelectionValidator,
        disappearsOut: Boolean
    ) {
        if (isInitialized) return

        val typefaceFamily: String = context.getString(R.string.dtp_radial_numbers_typeface)
        typefaceLight = context.getTypeface(typefaceFamily, Typeface.NORMAL)
        val typefaceFamilyRegular: String = context.getString(R.string.dtp_sans_serif)
        typefaceRegular = context.getTypeface(typefaceFamilyRegular, Typeface.NORMAL)

        validPaint.apply {
            color = controller.primaryTextColor
            isAntiAlias = true
            textAlign = Align.CENTER
        }

        invalidPaint.apply {
            color = controller.disabledTextColor
            isAntiAlias = true
            textAlign = Align.CENTER
        }

        highlightedPaint.apply {
            color = controller.highlightedTextColor
            isAntiAlias = true
            textAlign = Align.CENTER
        }

        this.texts = texts
        this.innerTexts = innerTexts
        is24HourMode = controller.is24HourMode
        hasInnerCircle = innerTexts != null

        if (is24HourMode) {
            circleRadiusMultiplier = context.getFloat(R.dimen.dtp_circle_radius_multiplier_24HourMode)
        } else {
            circleRadiusMultiplier = context.getFloat(R.dimen.dtp_circle_radius_multiplier)

            amPmCircleRadiusMultiplier = context.getFloat(R.dimen.dtp_ampm_circle_radius_multiplier)
        }

        if (hasInnerCircle) {
            numbersRadiusMultiplier = context.getFloat(R.dimen.dtp_numbers_radius_multiplier_outer)
            textSizeMultiplier = context.getFloat(R.dimen.dtp_time_text_size_multiplier_outer)
            innerNumbersRadiusMultiplier = context.getFloat(R.dimen.dtp_numbers_radius_multiplier_inner)
            innerTextSizeMultiplier = context.getFloat(R.dimen.dtp_time_text_size_multiplier_inner)
        } else {
            numbersRadiusMultiplier = context.getFloat(R.dimen.dtp_numbers_radius_multiplier_normal)
            textSizeMultiplier = context.getFloat(R.dimen.dtp_time_text_size_multiplier_normal)
        }

        animationRadiusMultiplier = 1F
        transitionMidRadiusMultiplier = 1F + 0.05F * if (disappearsOut) -1 else 1
        transitionEndRadiusMultiplier = 1F + 0.3F * if (disappearsOut) 1 else -1
        invalidateUpdateListener = InvalidateUpdateListener()

        this.validator = validator

        textGridValuesDirty = true

        isInitialized = true
    }

    override fun hasOverlappingRendering(): Boolean = false

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)

        xCenter = width / 2F
        yCenter = height / 2F
        circleRadius = min(
            min(xCenter, yCenter) * circleRadiusMultiplier,
            context.getDimensionPixelSize(R.dimen.dtp_circle_radius_max).toFloat()
        )

        if (!is24HourMode) {
            val amPmCircleRadius: Float = min(
                circleRadius * amPmCircleRadiusMultiplier,
                context.getDimensionPixelSize(R.dimen.dtp_am_pm_radius_max).toFloat()
            )
            yCenter -= (amPmCircleRadius * 0.75).toInt()
        }

        textSize = circleRadius * textSizeMultiplier
        if (hasInnerCircle) {
            innerTextSize = circleRadius * innerTextSizeMultiplier
        }

        // Because the text positions will be static, pre-render the animations.
        renderAnimations()

        textGridValuesDirty = true
        drawValuesReady = true

        if (textGridValuesDirty) {
            val numbersRadius: Float =
                circleRadius * numbersRadiusMultiplier * animationRadiusMultiplier

            calculateGridSizes(
                numbersRadius,
                xCenter, yCenter,
                textSize,
                textGridHeights, textGridWidths
            )
            if (hasInnerCircle) {
                val innerNumbersRadius: Float =
                    circleRadius * innerNumbersRadiusMultiplier * animationRadiusMultiplier
                calculateGridSizes(
                    innerNumbersRadius,
                    xCenter, yCenter,
                    innerTextSize,
                    innerTextGridHeights, innerTextGridWidths
                )
            }
            textGridValuesDirty = false
        }
    }

    public override fun onDraw(canvas: Canvas) {
        val viewWidth: Int = width
        if (viewWidth == 0 || !isInitialized) {
            return
        }

        drawTexts(
            canvas,
            textSize,
            typefaceLight,
            texts,
            textGridWidths, textGridHeights
        )
        if (hasInnerCircle) {
            drawTexts(
                canvas,
                innerTextSize,
                typefaceRegular,
                innerTexts,
                innerTextGridWidths, innerTextGridHeights
            )
        }
    }

    /**
     * Using the trigonometric Unit Circle, calculate the positions that the text will need to be
     * drawn at based on the specified circle radius. Place the values in the textGridHeights and
     * textGridWidths parameters.
     */
    private fun calculateGridSizes(
        numbersRadius: Float,
        xCenter: Float, yCenter: Float,
        textSize: Float,
        textGridHeights: FloatArray, textGridWidths: FloatArray
    ) {
        var newYCenter: Float = yCenter
        /*
         * The numbers need to be drawn in a 7x7 grid, representing the points on the Unit Circle.
         */
        // cos(30) = a / r => r * cos(30) = a => r * √3/2 = a
        val offset2: Float = numbersRadius * sqrt(3.0F) / 2F
        // sin(30) = o / r => r * sin(30) = o => r / 2 = a
        val offset3: Float = numbersRadius / 2F

        validPaint.textSize = textSize
        highlightedPaint.textSize = textSize
        invalidPaint.textSize = textSize

        // We'll need yTextBase to be slightly lower to account for the text's baseline.
        newYCenter -= (validPaint.descent() + validPaint.ascent()) / 2

        textGridHeights[0] = newYCenter - numbersRadius
        textGridWidths[0] = xCenter - numbersRadius
        textGridHeights[1] = newYCenter - offset2
        textGridWidths[1] = xCenter - offset2
        textGridHeights[2] = newYCenter - offset3
        textGridWidths[2] = xCenter - offset3
        textGridHeights[3] = newYCenter
        textGridWidths[3] = xCenter
        textGridHeights[4] = newYCenter + offset3
        textGridWidths[4] = xCenter + offset3
        textGridHeights[5] = newYCenter + offset2
        textGridWidths[5] = xCenter + offset2
        textGridHeights[6] = newYCenter + numbersRadius
        textGridWidths[6] = xCenter + numbersRadius
    }

    /**
     * Draw the 12 text values at the positions specified by the textGrid parameters.
     */
    private fun drawTexts(
        canvas: Canvas,
        textSize: Float,
        typeface: Typeface?,
        texts: Array<String>?,
        textGridWidths: FloatArray, textGridHeights: FloatArray
    ) {
        validPaint.apply {
            this.textSize = textSize
            this.typeface = typeface
        }

        texts?.let {
            val textPaints: Array<Paint> = assignTextColors(it)
            canvas.drawText(it[0], textGridWidths[3], textGridHeights[0], textPaints[0])
            canvas.drawText(it[1], textGridWidths[4], textGridHeights[1], textPaints[1])
            canvas.drawText(it[2], textGridWidths[5], textGridHeights[2], textPaints[2])
            canvas.drawText(it[3], textGridWidths[6], textGridHeights[3], textPaints[3])
            canvas.drawText(it[4], textGridWidths[5], textGridHeights[4], textPaints[4])
            canvas.drawText(it[5], textGridWidths[4], textGridHeights[5], textPaints[5])
            canvas.drawText(it[6], textGridWidths[3], textGridHeights[6], textPaints[6])
            canvas.drawText(it[7], textGridWidths[2], textGridHeights[5], textPaints[7])
            canvas.drawText(it[8], textGridWidths[1], textGridHeights[4], textPaints[8])
            canvas.drawText(it[9], textGridWidths[0], textGridHeights[3], textPaints[9])
            canvas.drawText(it[10], textGridWidths[1], textGridHeights[2], textPaints[10])
            canvas.drawText(it[11], textGridWidths[2], textGridHeights[1], textPaints[11])
        }
    }

    private fun assignTextColors(texts: Array<String>): Array<Paint> {
        val paints: Array<Paint> = Array(texts.size) { Paint() }

        var text: Int
        for (i in texts.indices) {
            text = texts[i].toInt()
            when {
                text == selection -> paints[i] = highlightedPaint
                validator?.isValidSelection(text) == true -> paints[i] = validPaint
                else -> paints[i] = invalidPaint
            }
        }

        return paints
    }

    /**
     * Render the animations for appearing and disappearing.
     */
    private fun renderAnimations() {
        var kf0: Keyframe
        var kf1: Keyframe
        var kf2: Keyframe
        val kf3: Keyframe
        var midwayPoint = 0.2F
        val duration = 500

        // Set up animator for disappearing.
        kf0 = Keyframe.ofFloat(0F, 1F)
        kf1 = Keyframe.ofFloat(midwayPoint, transitionMidRadiusMultiplier)
        kf2 = Keyframe.ofFloat(1F, transitionEndRadiusMultiplier)
        val radiusDisappear: PropertyValuesHolder =
            PropertyValuesHolder.ofKeyframe("animationRadiusMultiplier", kf0, kf1, kf2)

        kf0 = Keyframe.ofFloat(0F, 1F)
        kf1 = Keyframe.ofFloat(1F, 0F)
        val fadeOut: PropertyValuesHolder =
            PropertyValuesHolder.ofKeyframe("alpha", kf0, kf1)

        disappearAnimator =
            ObjectAnimator.ofPropertyValuesHolder(
                this, fadeOut
            ).setDuration(duration.toLong())
        disappearAnimator?.addUpdateListener(invalidateUpdateListener)


        // Set up animator for reappearing.
        val delayMultiplier = 0.25F
        val transitionDurationMultiplier = 1F
        val totalDurationMultiplier: Float = transitionDurationMultiplier + delayMultiplier
        val totalDuration: Float = duration * totalDurationMultiplier
        val delayPoint: Float = delayMultiplier * duration / totalDuration
        midwayPoint = 1 - midwayPoint * (1 - delayPoint)

        kf0 = Keyframe.ofFloat(0F, transitionEndRadiusMultiplier)
        kf1 = Keyframe.ofFloat(delayPoint, transitionEndRadiusMultiplier)
        kf2 = Keyframe.ofFloat(midwayPoint, transitionMidRadiusMultiplier)
        kf3 = Keyframe.ofFloat(1F, 1F)
        val radiusReappear: PropertyValuesHolder =
            PropertyValuesHolder.ofKeyframe(
                "animationRadiusMultiplier", kf0, kf1, kf2, kf3
            )

        kf0 = Keyframe.ofFloat(0F, 0F)
        kf1 = Keyframe.ofFloat(delayPoint, 0F)
        kf2 = Keyframe.ofFloat(1F, 1F)
        val fadeIn: PropertyValuesHolder =
            PropertyValuesHolder.ofKeyframe("alpha", kf0, kf1, kf2)

        reappearAnimator =
            ObjectAnimator.ofPropertyValuesHolder(
                this, fadeIn
            ).setDuration(totalDuration.toLong())
        reappearAnimator?.addUpdateListener(invalidateUpdateListener)
    }

    private inner class InvalidateUpdateListener : AnimatorUpdateListener {
        override fun onAnimationUpdate(animation: ValueAnimator) {
            this@RadialTextsView.invalidate()
        }
    }

    internal interface SelectionValidator {
        fun isValidSelection(selection: Int): Boolean
    }
}
