package com.olekdia.datetimepicker.time

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.view.View
import com.olekdia.androidcommon.extensions.getDimensionPixelSize
import com.olekdia.androidcommon.extensions.getFloat
import com.olekdia.common.extensions.adjustDarkness
import com.olekdia.datetimepicker.R
import com.olekdia.datetimepicker.interfaces.TimePickerController
import kotlin.math.min

class CircleView(
    context: Context,
    private val controller: TimePickerController
) : View(context) {

    private val paint = Paint()

    private var is24HourMode: Boolean = false

    private var circleRadiusMultiplier: Float = 0F
    private var amPmCircleRadiusMultiplier: Float = 0F

    private var isInitialized: Boolean = false

    private var drawValuesReady: Boolean = false
    private var xCenter: Float = 0F
    private var yCenter: Float = 0F
    private var circleRadius: Float = 0F

    fun initialize() {
        if (isInitialized) return

        paint.isAntiAlias = true

        is24HourMode = controller.is24HourMode
        if (is24HourMode) {
            circleRadiusMultiplier = context.getFloat(R.dimen.dtp_circle_radius_multiplier_24HourMode)
        } else {
            circleRadiusMultiplier = context.getFloat(R.dimen.dtp_circle_radius_multiplier)
            amPmCircleRadiusMultiplier = context.getFloat(R.dimen.dtp_ampm_circle_radius_multiplier)
        }

        isInitialized = true
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)

        xCenter = width / 2F
        yCenter = height / 2F
        circleRadius = min(
            min(xCenter, yCenter) * circleRadiusMultiplier,
            context.getDimensionPixelSize(R.dimen.dtp_circle_radius_max).toFloat()
        )

        if (!is24HourMode) {
            // We'll need to draw the AM/PM circles, so the main circle will need to have
            // a slightly higher center. To keep the entire view centered vertically, we'll
            // have to push it up by half the radius of the AM/PM circles.
            val amPmCircleRadius: Float = min(
                circleRadius * amPmCircleRadiusMultiplier,
                context.getDimensionPixelSize(R.dimen.dtp_am_pm_radius_max).toFloat()
            )
            yCenter -= amPmCircleRadius * 0.75F
        }

        drawValuesReady = true
    }

    public override fun onDraw(canvas: Canvas) {
        val viewWidth: Int = width
        if (viewWidth == 0 || !isInitialized) {
            return
        }

        val backgroundColor: Int = controller.ternaryBackgroundColor

        paint.color = backgroundColor
        canvas.drawCircle(
            xCenter, yCenter,
            circleRadius,
            paint
        )

        paint.color = controller.accentColor
        canvas.drawCircle(
            xCenter, yCenter,
            8F,
            paint
        )
    }

    companion object {

        val TimePickerController.ternaryBackgroundColor: Int
            get() = if (this.isDarkTheme) {
                this.secondaryBackgroundColor.adjustDarkness(0.87F)
            } else {
                this.secondaryBackgroundColor.adjustDarkness(0.97F)
            }
    }
}
