package com.olekdia.datetimepicker.time

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewConfiguration
import android.widget.FrameLayout
import com.olekdia.common.INVALID
import com.olekdia.common.extensions.toLocal2DigNumerals
import com.olekdia.common.extensions.toLocalNumerals
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.common.NOT_SET
import com.olekdia.datetimepicker.common.PickerIndex.Companion.HOUR
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MINUTE
import com.olekdia.datetimepicker.common.PickerIndex.Companion.SECOND
import com.olekdia.datetimepicker.interfaces.TimePickerController
import java.util.*
import kotlin.math.abs

@SuppressLint("ViewConstructor")
class TimePicker(
    context: Context,
    private val controller: TimePickerController
) : FrameLayout(context),
    OnTouchListener {

    private val circleView: CircleView
    private val amPmCirclesView: AmPmCirclesView
    private val hourRadialTextsView: RadialTextsView
    private val minuteRadialTextsView: RadialTextsView
    private val secondRadialTextsView: RadialTextsView
    private val hourRadialSelectorView: RadialSelectorView
    private val minuteRadialSelectorView: RadialSelectorView
    private val secondRadialSelectorView: RadialSelectorView

    var showedTime: DateTime = controller.currentSelectedDt
    var currentItemShowing: Int = NOT_SET

    private var lastValueSelected: DateTime? = null

    private val touchSlop: Int
    private val tapTimeout: Int

    private var snapPrefer30sMap: IntArray? = null
    private var amOrPm = INVALID
    private var doingMove: Boolean = false
    private var downDegrees: Int = 0
    private var downX: Float = 0F
    private var downY: Float = 0F
    private var downMillis: Long = 0L

    private var transition: AnimatorSet? = null
    private val touchHandler = Handler(Looper.getMainLooper())

    init {
        setOnTouchListener(this)

        touchSlop = ViewConfiguration.get(context).scaledTouchSlop
        tapTimeout = ViewConfiguration.getTapTimeout()

        circleView = CircleView(context, controller)
        addView(circleView)

        amPmCirclesView = AmPmCirclesView(context, controller)
        addView(amPmCirclesView)

        hourRadialSelectorView = RadialSelectorView(context)
        addView(hourRadialSelectorView)
        minuteRadialSelectorView = RadialSelectorView(context)
        addView(minuteRadialSelectorView)
        secondRadialSelectorView = RadialSelectorView(context)
        addView(secondRadialSelectorView)

        hourRadialTextsView = RadialTextsView(context)
        addView(hourRadialTextsView)
        minuteRadialTextsView = RadialTextsView(context)
        addView(minuteRadialTextsView)
        secondRadialTextsView = RadialTextsView(context)
        addView(secondRadialTextsView)

        preparePrefer30sMap()
    }

    fun initialize(
        context: Context,
        initialTime: DateTime
    ) {
        showedTime = initialTime

        circleView.apply {
            initialize()
            invalidate()
        }

        if (!controller.is24HourMode &&
            controller.isHourAvailable
        ) {
            amPmCirclesView.apply {
                initialize(initialTime.amPm)
                invalidate()
            }
        }

        val secondValidator = object : RadialTextsView.SelectionValidator {
            override fun isValidSelection(selection: Int): Boolean {
                val selectionDt =
                    DateTime(showedTime.timeInMillis)
                        .apply {
                            second = selection
                        }
                return !controller.isOutOfRange(SECOND, selectionDt)
            }
        }
        val minuteValidator = object : RadialTextsView.SelectionValidator {
            override fun isValidSelection(selection: Int): Boolean {
                val selectionDt =
                    DateTime(showedTime.timeInMillis)
                        .apply {
                            minute = selection
                        }
                return !controller.isOutOfRange(MINUTE, selectionDt)
            }
        }
        val hourValidator = object : RadialTextsView.SelectionValidator {
            override fun isValidSelection(selection: Int): Boolean {
                val selectionDt =
                    DateTime(showedTime.timeInMillis)
                        .apply {
                            if (controller.is24HourMode) {
                                hour24 = selection
                            } else {
                                hour12 = selection % 12
                            }
                        }

                return !controller.isOutOfRange(HOUR, selectionDt)
            }
        }

        val hours: IntArray = intArrayOf(12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)
        val hours24: IntArray = intArrayOf(0, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)
        val minutes: IntArray = intArrayOf(0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55)
        val seconds: IntArray = intArrayOf(0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55)

        val hoursTexts: Array<String> = Array(12) { "" }
        val innerHoursTexts: Array<String> = Array(12) { "" }
        val minutesTexts: Array<String> = Array(12) { "" }
        val secondsTexts: Array<String> = Array(12) { "" }

        val numSystem = controller.numeralSystem

        for (i in 0..11) {
            hoursTexts[i] =
                if (controller.is24HourMode) {
                    hours24[i].toLocal2DigNumerals(numSystem)
                } else {
                    hours[i].toLocalNumerals(numSystem)
                }

            innerHoursTexts[i] = hours[i].toLocalNumerals(numSystem)
            minutesTexts[i] = minutes[i].toLocal2DigNumerals(numSystem)
            secondsTexts[i] = seconds[i].toLocal2DigNumerals(numSystem)
        }

        hourRadialTextsView.apply {
            initialize(
                context,
                hoursTexts,
                if (controller.is24HourMode) innerHoursTexts else null,
                controller,
                hourValidator,
                true
            )
            selection =
                if (controller.is24HourMode) initialTime.hour24 else hours[initialTime.hour12]
            invalidate()
        }

        minuteRadialTextsView.apply {
            initialize(
                context,
                minutesTexts,
                null,
                controller,
                minuteValidator,
                false
            )
            selection = initialTime.minute
            invalidate()
        }

        secondRadialTextsView.apply {
            initialize(
                context,
                secondsTexts,
                null,
                controller,
                secondValidator,
                false
            )
            selection = initialTime.second
            invalidate()
        }

        val hourDegrees = initialTime.hour12 * HOUR_VALUE_TO_DEGREES_STEP_SIZE
        hourRadialSelectorView.apply {
            initialize(
                context,
                controller,
                controller.is24HourMode,
                true,
                hourDegrees,
                isHourInnerCircle(initialTime.hour24)
            )
            invalidate()
        }

        val minuteDegrees = initialTime.minute * MINUTE_VALUE_TO_DEGREES_STEP_SIZE
        minuteRadialSelectorView.apply {
            initialize(
                context,
                controller,
                false,
                disappearsOut = false,
                selectionDegrees = minuteDegrees,
                isInnerCircle = false
            )
            invalidate()
        }

        val secondDegrees = initialTime.second * SECOND_VALUE_TO_DEGREES_STEP_SIZE
        secondRadialSelectorView.apply {
            initialize(
                context,
                controller,
                hasInnerCircle = false,
                disappearsOut = false,
                selectionDegrees = secondDegrees,
                isInnerCircle = false
            )
            invalidate()
        }
    }

    fun setItem(index: Int, time: DateTime) {
        controller.roundToNearest(time, index)
        showedTime = time

        reselectSelector(showedTime, false, index)
        controller.onTimeSelected(showedTime, index)
    }

    fun setAmOrPm(amOrPm: Int) {
        amPmCirclesView.apply {
            this.amOrPm = amOrPm
            invalidate()
        }

        showedTime.amPm = amOrPm
        controller.roundToNearest(showedTime, HOUR, true)

        reselectSelector(showedTime, false, HOUR)
        controller.onTimeSelected(showedTime, HOUR)
    }

    private fun isHourInnerCircle(hourOfDay: Int): Boolean =
        controller.is24HourMode && hourOfDay <= 12 && hourOfDay != 0

    /**
     * Split up the 360 degrees of the circle among the 60 selectable values. Assigns a larger
     * selectable area to each of the 12 visible values, such that the ratio of space apportioned
     * to a visible value : space apportioned to a non-visible value will be 14 : 4.
     * E.g. the output of 30 degrees should have a higher range of input associated with it than
     * the output of 24 degrees, because 30 degrees corresponds to a visible number on the clock
     * circle (5 on the minutes, 1 or 13 on the hours).
     */
    private fun preparePrefer30sMap() {
        // We'll split up the visible output and the non-visible output such that each visible
        // output will correspond to a range of 14 associated input degrees, and each non-visible
        // output will correspond to a range of 4 associate input degrees, so visible numbers
        // are more than 3 times easier to get than non-visible numbers:
        // {354-359,0-7}:0, {8-11}:6, {12-15}:12, {16-19}:18, {20-23}:24, {24-37}:30, etc.
        //
        // If an output of 30 degrees should correspond to a range of 14 associated degrees, then
        // we'll need any input between 24 - 37 to snap to 30. Working out from there, 20-23 should
        // snap to 24, while 38-41 should snap to 36. This is somewhat counter-intuitive, that you
        // can be touching 36 degrees but have the selection snapped to 30 degrees; however, this
        // inconsistency isn't noticeable at such fine-grained degrees, and it affords us the
        // ability to aggressively prefer the visible values by a factor of more than 3:1, which
        // greatly contributes to the selectability of these values.

        // Our input will be 0 through 360.
        snapPrefer30sMap = IntArray(361)

        // The first output is 0, and each following output will increment by 6 {0, 6, 12, ...}.
        var snappedOutputDegrees = 0
        // Count of how many inputs we've designated to the specified output.
        var count = 1
        // How many input we expect for a specified output. This will be 14 for output divisible
        // by 30, and 4 for the remaining output. We'll special case the outputs of 0 and 360, so
        // the caller can decide which they need.
        var expectedCount = 8

        snapPrefer30sMap?.let {
            // Iterate through the input.
            for (degrees in 0..360) {
                // Save the input-output mapping.
                it[degrees] = snappedOutputDegrees
                // If this is the last input for the specified output, calculate the next output and
                // the next expected count.
                if (count == expectedCount) {
                    snappedOutputDegrees += 6
                    expectedCount = when {
                        snappedOutputDegrees == 360 -> 7
                        snappedOutputDegrees % 30 == 0 -> 14
                        else -> 4
                    }
                    count = 1
                } else {
                    count++
                }
            }
        }
    }

    /**
     * Returns mapping of any input degrees (0 to 360) to one of 60 selectable output degrees,
     * where the degrees corresponding to visible numbers (i.e. those divisible by 30) will be
     * weighted heavier than the degrees corresponding to non-visible numbers.
     * See [.preparePrefer30sMap] documentation for the rationale and generation of the
     * mapping.
     */
    private fun snapPrefer30s(degrees: Int): Int =
        snapPrefer30sMap?.get(degrees) ?: INVALID

    /**
     * Returns mapping of any input degrees (0 to 360) to one of 12 visible output degrees (all
     * multiples of 30), where the input will be "snapped" to the closest visible degrees.
     *
     * @param degrees            The input degrees
     * @param forceHigherOrLower The output may be forced to either the higher or lower step, or may
     * be allowed to snap to whichever is closer. Use 1 to force strictly higher, -1 to force
     * strictly lower, and 0 to snap to the closer one.
     * @return output degrees, will be a multiple of 30
     */
    private fun snapOnly30s(degrees: Int, forceHigherOrLower: Int): Int {
        var newDegrees: Int = degrees
        val stepSize: Int = HOUR_VALUE_TO_DEGREES_STEP_SIZE
        var floor: Int = newDegrees / stepSize * stepSize
        val ceiling: Int = floor + stepSize

        when (forceHigherOrLower) {
            1 -> newDegrees = ceiling

            INVALID -> {
                if (newDegrees == floor) {
                    floor -= stepSize
                }
                newDegrees = floor
            }

            else -> newDegrees =
                if (newDegrees - floor < ceiling - newDegrees) {
                    floor
                } else {
                    ceiling
                }
        }
        return newDegrees
    }

    private fun reselectSelector(
        newSelection: DateTime?,
        forceDrawDot: Boolean,
        index: Int
    ) {
        newSelection?.let {
            val degrees: Int

            when (index) {
                HOUR -> {
                    var hour: Int = it.hour24
                    val isInnerCircle: Boolean = isHourInnerCircle(hour)
                    degrees = hour % 12 * 360 / 12

                    if (!controller.is24HourMode) hour %= 12
                    if (!controller.is24HourMode && hour == 0) hour += 12

                    hourRadialSelectorView.setSelection(degrees, isInnerCircle, forceDrawDot)
                    hourRadialTextsView.selection = hour

                    val minDegrees: Int = it.minute * 360 / 60
                    minuteRadialSelectorView.setSelection(minDegrees, isInnerCircle, forceDrawDot)
                    minuteRadialTextsView.selection = it.minute

                    val secDegrees: Int = it.second * 360 / 60
                    secondRadialSelectorView.setSelection(secDegrees, isInnerCircle, forceDrawDot)
                    secondRadialTextsView.selection = it.second
                }

                MINUTE -> {
                    degrees = it.minute * 360 / 60

                    minuteRadialSelectorView.setSelection(degrees, false, forceDrawDot)
                    minuteRadialTextsView.selection = it.minute

                    val secDegrees: Int = it.second * 360 / 60
                    secondRadialSelectorView.setSelection(secDegrees, false, forceDrawDot)
                    secondRadialTextsView.selection = it.second
                }

                SECOND -> {
                    degrees = it.second * 360 / 60

                    secondRadialSelectorView.setSelection(degrees, false, forceDrawDot)
                    secondRadialTextsView.selection = it.second
                }
            }

            when (currentItemShowing) {
                HOUR -> {
                    hourRadialSelectorView.invalidate()
                    hourRadialTextsView.invalidate()
                }
                MINUTE -> {
                    minuteRadialSelectorView.invalidate()
                    minuteRadialTextsView.invalidate()
                }
                SECOND -> {
                    secondRadialSelectorView.invalidate()
                    secondRadialTextsView.invalidate()
                }
            }
        }
    }

    private fun getTimeFromDegrees(
        degrees: Int,
        isInnerCircle: Boolean,
        forceToVisibleValue: Boolean
    ): DateTime? {
        var degreesVar: Int = degrees

        if (degreesVar == INVALID) {
            return null
        }

        val stepSize: Int =
            when (currentItemShowing) {
                HOUR -> HOUR_VALUE_TO_DEGREES_STEP_SIZE
                MINUTE -> MINUTE_VALUE_TO_DEGREES_STEP_SIZE
                else -> SECOND_VALUE_TO_DEGREES_STEP_SIZE
            }
        val allowFineGrained =
            !forceToVisibleValue &&
                    (currentItemShowing == MINUTE || currentItemShowing == SECOND)

        degreesVar =
            if (allowFineGrained) {
                snapPrefer30s(degreesVar)
            } else {
                snapOnly30s(degreesVar, 0)
            }

        if (currentItemShowing == HOUR) {
            if (controller.is24HourMode) {
                if (degreesVar == 0 && isInnerCircle) {
                    degreesVar = 360
                } else if (degreesVar == 360 && !isInnerCircle) {
                    degreesVar = 0
                }
            } else if (degreesVar == 0) {
                degreesVar = 360
            }
        } else if (degreesVar == 360 &&
            (currentItemShowing == MINUTE || currentItemShowing == SECOND)
        ) {
            degreesVar = 0
        }

        var value = degreesVar / stepSize

        if (currentItemShowing == HOUR &&
            controller.is24HourMode &&
            !isInnerCircle &&
            degreesVar != 0
        ) {
            value += 12
        }

        val newSelection: DateTime?
        when (currentItemShowing) {
            HOUR -> {
                var hour = value
                if (!controller.is24HourMode &&
                    showedTime.amPm == Calendar.PM &&
                    degreesVar != 360
                ) {
                    hour += 12
                }
                if (!controller.is24HourMode &&
                    showedTime.amPm == Calendar.AM &&
                    degreesVar == 360
                ) {
                    hour = 0
                }
                newSelection = DateTime(showedTime.timeInMillis)
                    .apply {
                        hour24 = hour
                    }
            }

            MINUTE -> newSelection =
                DateTime(showedTime.timeInMillis)
                    .apply {
                        minute = value
                    }

            SECOND -> newSelection =
                DateTime(showedTime.timeInMillis)
                    .apply {
                        second = value
                    }

            else -> newSelection = showedTime
        }

        return newSelection
    }

    private fun getDegreesFromCoords(
        pointX: Float,
        pointY: Float,
        forceLegal: Boolean,
        isInnerCircle: Array<Boolean>
    ): Int {
        return when (currentItemShowing) {
            HOUR ->
                hourRadialSelectorView.getDegreesFromCoords(
                    pointX, pointY, forceLegal, isInnerCircle
                )
            MINUTE ->
                minuteRadialSelectorView.getDegreesFromCoords(
                    pointX, pointY, forceLegal, isInnerCircle
                )
            SECOND ->
                secondRadialSelectorView.getDegreesFromCoords(
                    pointX, pointY, forceLegal, isInnerCircle
                )
            else -> INVALID
        }
    }

    fun setCurrentItemShowing(index: Int, animate: Boolean) {
        if (index != HOUR && index != MINUTE && index != SECOND) {
            return
        }

        val lastIndex: Int = currentItemShowing
        currentItemShowing = index

        if (animate && index != lastIndex) {
            val anims: Array<ObjectAnimator?> = arrayOfNulls(4)

            if (index == MINUTE && lastIndex == HOUR) {
                anims[0] = hourRadialTextsView.disappearAnimator
                anims[1] = hourRadialSelectorView.disappearAnimator
                anims[2] = minuteRadialTextsView.reappearAnimator
                anims[3] = minuteRadialSelectorView.reappearAnimator
            } else if (index == HOUR && lastIndex == MINUTE) {
                anims[0] = hourRadialTextsView.reappearAnimator
                anims[1] = hourRadialSelectorView.reappearAnimator
                anims[2] = minuteRadialTextsView.disappearAnimator
                anims[3] = minuteRadialSelectorView.disappearAnimator
            } else if (index == MINUTE && lastIndex == SECOND) {
                anims[0] = secondRadialTextsView.disappearAnimator
                anims[1] = secondRadialSelectorView.disappearAnimator
                anims[2] = minuteRadialTextsView.reappearAnimator
                anims[3] = minuteRadialSelectorView.reappearAnimator
            } else if (index == HOUR && lastIndex == SECOND) {
                anims[0] = secondRadialTextsView.disappearAnimator
                anims[1] = secondRadialSelectorView.disappearAnimator
                anims[2] = hourRadialTextsView.reappearAnimator
                anims[3] = hourRadialSelectorView.reappearAnimator
            } else if (index == SECOND && lastIndex == MINUTE) {
                anims[0] = secondRadialTextsView.reappearAnimator
                anims[1] = secondRadialSelectorView.reappearAnimator
                anims[2] = minuteRadialTextsView.disappearAnimator
                anims[3] = minuteRadialSelectorView.disappearAnimator
            } else if (index == SECOND && lastIndex == HOUR) {
                anims[0] = secondRadialTextsView.reappearAnimator
                anims[1] = secondRadialSelectorView.reappearAnimator
                anims[2] = hourRadialTextsView.disappearAnimator
                anims[3] = hourRadialSelectorView.disappearAnimator
            } else {
                return
            }

            transition?.let {
                if (it.isRunning) {
                    it.end()
                }
            }

            transition = AnimatorSet()
            transition?.apply {
                playTogether(*anims)
                start()
            }

        } else {
            val hourAlpha = if (index == HOUR) 1 else 0
            val minuteAlpha = if (index == MINUTE) 1 else 0
            val secondAlpha = if (index == SECOND) 1 else 0

            hourRadialTextsView.alpha = hourAlpha.toFloat()
            hourRadialSelectorView.alpha = hourAlpha.toFloat()
            minuteRadialTextsView.alpha = minuteAlpha.toFloat()
            minuteRadialSelectorView.alpha = minuteAlpha.toFloat()
            secondRadialTextsView.alpha = secondAlpha.toFloat()
            secondRadialSelectorView.alpha = secondAlpha.toFloat()
        }
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                onDown(event)
                return true
            }

            MotionEvent.ACTION_MOVE -> {
                onMove(event)
                return true
            }

            MotionEvent.ACTION_UP -> {
                onUp(event)
                return true
            }
        }
        return false
    }

    private fun onDown(event: MotionEvent) {
        downX = event.x
        downY = event.y
        downMillis = System.currentTimeMillis()

        lastValueSelected = null
        doingMove = false

        amOrPm = INVALID
        var isAmOrPmTouched = false

        if (!controller.is24HourMode) {
            amOrPm = amPmCirclesView.getIsTouchingAmOrPm(event.x, event.y)
            isAmOrPmTouched = amOrPm == Calendar.AM || amOrPm == Calendar.PM
        }

        if (isAmOrPmTouched) {
            onAmPmDown()
        } else {
            onTimeDown(event)
        }
    }

    private fun onMove(event: MotionEvent) {
        val dX: Float = abs(event.x - downX)
        val dY: Float = abs(event.y - downY)

        val isSlightAccidentalMovement: Boolean =
            !doingMove && dX <= touchSlop && dY <= touchSlop
        if (isSlightAccidentalMovement) {
            return
        }

        val isAmOrPmTouched = amOrPm == Calendar.AM || amOrPm == Calendar.PM
        if (!isAmOrPmTouched) {
            onTimeMove(event)
        }
    }

    private fun onUp(event: MotionEvent) {
        touchHandler.removeCallbacksAndMessages(null)

        val isAmOrPmTouched = amOrPm == Calendar.AM || amOrPm == Calendar.PM
        if (isAmOrPmTouched) {
            onAmPmUp(event)
        } else {
            onTimeUp(event)
        }
    }

    private fun onAmPmDown() {
        controller.tryVibrate()
        downDegrees = INVALID
        touchHandler.postDelayed(
            {
                amPmCirclesView.amOrPmPressed = amOrPm
                amPmCirclesView.invalidate()
            },
            tapTimeout.toLong()
        )
    }

    private fun onAmPmUp(event: MotionEvent) {
        val upOnAmOrPm: Int = amPmCirclesView.getIsTouchingAmOrPm(event.x, event.y)

        amPmCirclesView.apply {
            amOrPmPressed = INVALID
            invalidate()
        }

        if (upOnAmOrPm != INVALID
            && amOrPm == upOnAmOrPm
        ) {
            amPmCirclesView.amOrPm = upOnAmOrPm

            if (showedTime.amPm != upOnAmOrPm) {
                showedTime.amPm = upOnAmOrPm
                controller.roundToNearest(showedTime, HOUR, true)

                reselectSelector(showedTime, false, HOUR)
                controller.onTimeSelected(showedTime, HOUR)
            }
        }

        amOrPm = INVALID
    }

    private fun onTimeDown(event: MotionEvent) {
        val isInnerCircle: Array<Boolean> = Array(1) { false }

        downDegrees =
            getDegreesFromCoords(event.x, event.y, false, isInnerCircle)
        val downTime: DateTime? =
            getTimeFromDegrees(downDegrees, isInnerCircle[0], false)

        downTime?.let {
            if (controller.isOutOfRange(currentItemShowing, it)) {
                downDegrees = INVALID
            }
        }

        if (downDegrees != INVALID) {
            controller.tryVibrate()
            parent.requestDisallowInterceptTouchEvent(true)
            touchHandler.postDelayed(
                {
                    doingMove = true
                    lastValueSelected = downTime

                    lastValueSelected?.let {
                        reselectSelector(it, true, currentItemShowing)
                        controller.onTimeSelected(it, currentItemShowing)
                    }
                },
                tapTimeout.toLong()
            )
        }
    }

    private fun onTimeMove(event: MotionEvent) {
        val isInnerCircle: Array<Boolean> = Array(1) { false }

        if (downDegrees != INVALID) {
            touchHandler.removeCallbacksAndMessages(null)
            doingMove = true

            val moveDegree: Int =
                getDegreesFromCoords(event.x, event.y, true, isInnerCircle)
            val moveTime: DateTime? =
                getTimeFromDegrees(moveDegree, isInnerCircle[0], false)

            moveTime?.let {
                if (controller.isOutOfRange(currentItemShowing, it)) {
                    controller.roundToNearest(it, INVALID)
                    controller.onTimeSelected(it, currentItemShowing)

                    reselectSelector(it, true, currentItemShowing)

                    lastValueSelected = it
                    showedTime = it

                    return
                }

                if (lastValueSelected == null || lastValueSelected != it) {
                    controller.tryVibrate()
                    lastValueSelected = it

                    reselectSelector(it, true, currentItemShowing)
                    controller.onTimeSelected(it, currentItemShowing)
                }
            }
        }
    }

    private fun onTimeUp(event: MotionEvent) {
        val isInnerCircle: Array<Boolean> = Array(1) { false }

        if (downDegrees != INVALID) {
            val upDegree: Int =
                getDegreesFromCoords(event.x, event.y, doingMove, isInnerCircle)
            val upTime: DateTime? =
                getTimeFromDegrees(upDegree, isInnerCircle[0], !doingMove)

            upTime?.let {
                if (controller.isOutOfRange(currentItemShowing, it)) {
                    return
                }

                showedTime = it

                reselectSelector(it, false, currentItemShowing)
                controller.onTimeSelected(it, currentItemShowing, true)
            }
        }
        parent.requestDisallowInterceptTouchEvent(false)
        doingMove = false
    }

    companion object {
        private const val VISIBLE_DEGREES_STEP_SIZE = 30
        const val HOUR_VALUE_TO_DEGREES_STEP_SIZE = VISIBLE_DEGREES_STEP_SIZE
        const val MINUTE_VALUE_TO_DEGREES_STEP_SIZE = 6
        const val SECOND_VALUE_TO_DEGREES_STEP_SIZE = 6
    }
}