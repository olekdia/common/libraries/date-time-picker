package com.olekdia.datetimepicker.time

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Paint.Align
import android.graphics.Typeface
import android.view.View
import com.olekdia.common.extensions.MAX_COLOR_COMPONENT
import com.olekdia.androidcommon.extensions.getDimensionPixelSize
import com.olekdia.androidcommon.extensions.getFloat
import com.olekdia.androidcommon.extensions.getTypeface
import com.olekdia.common.INVALID
import com.olekdia.common.INVALID_F
import com.olekdia.datetimepicker.R
import com.olekdia.datetimepicker.interfaces.TimePickerController
import com.olekdia.datetimepicker.time.CircleView.Companion.ternaryBackgroundColor
import java.util.*
import kotlin.math.abs
import kotlin.math.min
import kotlin.math.pow
import kotlin.math.sqrt

class AmPmCirclesView(
    context: Context,
    private val controller: TimePickerController
) : View(context) {

    private val paint = Paint()

    private var selectedAlpha: Int = 0

    private var circleRadiusMultiplier: Float = 0F
    private var amPmCircleRadiusMultiplier: Float = 0F

    private var amText: String = ""
    private var pmText: String = ""

    private var amDisabled: Boolean = false
    private var pmDisabled: Boolean = false

    private var isInitialized: Boolean = false
    private var drawValuesReady: Boolean = false

    private var amPmCircleRadius: Float = 0F
    private var amXCenter: Float = 0F
    private var pmXCenter: Float = 0F
    private var amPmYCenter: Float = 0F

    private var textSize: Float = 0F

    internal var amOrPm: Int = 0
    internal var amOrPmPressed: Int = 0

    fun initialize(
        amOrPm: Int
    ) {
        amDisabled = controller.isAmDisabled()
        pmDisabled = controller.isPmDisabled()
        this.amOrPm = amOrPm

        if (isInitialized) return

        selectedAlpha = MAX_COLOR_COMPONENT

        val typefaceFamily: String = context.getString(R.string.dtp_sans_serif)
        val tf: Typeface? = context.getTypeface(typefaceFamily, Typeface.NORMAL)

        paint.apply {
            typeface = tf
            isAntiAlias = true
            textAlign = Align.CENTER
        }

        circleRadiusMultiplier = context.getFloat(R.dimen.dtp_circle_radius_multiplier)
        amPmCircleRadiusMultiplier = context.getFloat(R.dimen.dtp_ampm_circle_radius_multiplier)

        amText = controller.amText
        pmText = controller.pmText
        amOrPmPressed = -1

        isInitialized = true
    }

    fun getIsTouchingAmOrPm(
        xCoord: Float, yCoord: Float
    ): Int {
        if (!drawValuesReady) return INVALID

        val squaredYDistance: Double = (yCoord - amPmYCenter).toDouble().pow(2.0)

        val distanceToAmCenter: Double =
            sqrt((xCoord - amXCenter).toDouble().pow(2.0) + squaredYDistance)

        if (distanceToAmCenter <= amPmCircleRadius && !amDisabled) {
            return Calendar.AM
        }

        val distanceToPmCenter: Double =
            sqrt((xCoord - pmXCenter).toDouble().pow(2.0) + squaredYDistance)

        return if (distanceToPmCenter <= amPmCircleRadius && !pmDisabled) {
            Calendar.PM
        } else {
            INVALID
        }
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)

        val layoutXCenter: Float = width / 2F
        var layoutYCenter: Float = height / 2F
        val circleRadius: Float = min(
            min(layoutXCenter, layoutYCenter) * circleRadiusMultiplier,
            context.getDimensionPixelSize(R.dimen.dtp_circle_radius_max).toFloat()
        )

        amPmCircleRadius = min(
            circleRadius * amPmCircleRadiusMultiplier,
            context.getDimensionPixelSize(R.dimen.dtp_am_pm_radius_max).toFloat()
        )
        layoutYCenter += amPmCircleRadius * 0.75F

        textSize = min(
            amPmCircleRadius * 3F / 4F,
            context.getDimensionPixelSize(R.dimen.dtp_extra_extra_large_font_size_limit).toFloat()
        )
        paint.textSize = textSize

        // Line up the vertical center of the AM/PM circles with the bottom of the main circle.
        amPmYCenter = layoutYCenter - amPmCircleRadius + circleRadius
        // Line up the horizontal edges of the AM/PM circles with the horizontal edges
        // of the main circle.
        amXCenter = layoutXCenter - circleRadius + amPmCircleRadius
        pmXCenter = layoutXCenter + circleRadius - amPmCircleRadius

        drawValuesReady = true
    }

    public override fun onDraw(canvas: Canvas) {
        val viewWidth = width
        if (viewWidth == 0 || !isInitialized) {
            return
        }

        paint.textSize = textSize

        // We'll need to draw either a lighter blue (for selection), a darker blue (for touching)
        // or white (for not selected).
        val backgroundColor: Int = controller.ternaryBackgroundColor

        var amColor: Int = backgroundColor
        var amAlpha = 255
        var amTextColor: Int = controller.secondaryTextColor
        var pmColor: Int = backgroundColor
        var pmAlpha = 255
        var pmTextColor: Int = controller.secondaryTextColor

        if (amOrPm == Calendar.AM) {
            amColor = controller.accentColor
            amAlpha = selectedAlpha
            amTextColor = controller.highlightedTextColor
        } else if (amOrPm == Calendar.PM) {
            pmColor = controller.accentColor
            pmAlpha = selectedAlpha
            pmTextColor = controller.highlightedTextColor
        }

        if (amOrPmPressed == Calendar.AM) {
            amColor = controller.accentColor
            amAlpha = selectedAlpha
            amTextColor = controller.highlightedTextColor
        } else if (amOrPmPressed == Calendar.PM) {
            pmColor = controller.accentColor
            pmAlpha = selectedAlpha
            pmTextColor = controller.highlightedTextColor
        }

        if (amDisabled) {
            amTextColor = controller.disabledTextColor
        }
        if (pmDisabled) {
            pmTextColor = controller.disabledTextColor
        }

        // Draw the two circles.
        paint.apply {
            color = amColor
            alpha = amAlpha
        }

        canvas.drawCircle(
            amXCenter,
            amPmYCenter,
            amPmCircleRadius,
            paint
        )

        paint.apply {
            color = pmColor
            alpha = pmAlpha
        }
        canvas.drawCircle(
            pmXCenter,
            amPmYCenter,
            amPmCircleRadius,
            paint
        )

        // Draw the AM/PM texts on top.
        paint.color = amTextColor
        val textYCenter: Float = amPmYCenter - (paint.descent() + paint.ascent()) / 2
        canvas.drawText(amText, amXCenter, textYCenter, paint)
        paint.color = pmTextColor
        canvas.drawText(pmText, pmXCenter, textYCenter, paint)
    }
}
