package com.olekdia.datetimepicker.time

import android.animation.Keyframe
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.animation.ValueAnimator.AnimatorUpdateListener
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.view.View
import com.olekdia.common.extensions.MAX_COLOR_COMPONENT
import com.olekdia.androidcommon.extensions.getDimensionPixelSize
import com.olekdia.androidcommon.extensions.getFloat
import com.olekdia.common.INVALID
import com.olekdia.datetimepicker.R
import com.olekdia.datetimepicker.interfaces.TimePickerController
import java.lang.Math.pow
import kotlin.math.*

class RadialSelectorView(context: Context) : View(context) {

    private val paint = Paint()

    private var isInitialized: Boolean = false
    private var drawValuesReady: Boolean = false

    private var circleRadiusMultiplier: Float = 0F
    private var amPmCircleRadiusMultiplier: Float = 0F
    private var innerNumbersRadiusMultiplier: Float = 0F
    private var outerNumbersRadiusMultiplier: Float = 0F
    private var numbersRadiusMultiplier: Float = 0F
    private var selectionRadiusMultiplier: Float = 0F
    internal var animationRadiusMultiplier: Float = 0F

    private var is24HourMode: Boolean = false
    private var hasInnerCircle: Boolean = false
    private var selectionAlpha: Int = 0

    private var xCenter: Float = 0F
    private var yCenter: Float = 0F
    private var circleRadius: Float = 0F

    private var transitionMidRadiusMultiplier: Float = 0F
    private var transitionEndRadiusMultiplier: Float = 0F

    private var lineLength: Float = 0F
    private var lineStrokeWidth: Float = context.getDimensionPixelSize(R.dimen.dtp_circle_line_stroke_width).toFloat()
    private var selectionRadius: Float = 0F

    private var selectionDegrees: Int = 0
    private var selectionRadians: Double = 0.0

    private var forceDrawDot: Boolean = false

    private var invalidateUpdateListener: InvalidateUpdateListener? = null

    val disappearAnimator: ObjectAnimator?
        get() {
            if (!isInitialized || !drawValuesReady) {
                return null
            }

            var kf0: Keyframe
            var kf1: Keyframe
            val kf2: Keyframe
            val midwayPoint = 0.2F
            val duration = 500L

            kf0 = Keyframe.ofFloat(0F, 1F)
            kf1 = Keyframe.ofFloat(midwayPoint, transitionMidRadiusMultiplier)
            kf2 = Keyframe.ofFloat(1F, transitionEndRadiusMultiplier)
            val radiusDisappear: PropertyValuesHolder =
                PropertyValuesHolder.ofKeyframe(
                    "animationRadiusMultiplier", kf0, kf1, kf2
                )

            kf0 = Keyframe.ofFloat(0F, 1F)
            kf1 = Keyframe.ofFloat(1F, 0F)
            val fadeOut: PropertyValuesHolder =
                PropertyValuesHolder.ofKeyframe("alpha", kf0, kf1)

            val disappearAnimator: ObjectAnimator =
                ObjectAnimator.ofPropertyValuesHolder(
                    this, radiusDisappear, fadeOut
                ).setDuration(duration)

            disappearAnimator.addUpdateListener(invalidateUpdateListener)

            return disappearAnimator
        }

    // The time points are half of what they would normally be, because this animation is
    // staggered against the disappear so they happen seamlessly. The reappear starts
    // halfway into the disappear.
    val reappearAnimator: ObjectAnimator?
        get() {
            if (!isInitialized || !drawValuesReady) {
                return null
            }

            var kf0: Keyframe
            var kf1: Keyframe
            var kf2: Keyframe
            val kf3: Keyframe
            var midwayPoint = 0.2F
            val duration = 500

            val delayMultiplier = 0.25F
            val transitionDurationMultiplier = 1F
            val totalDurationMultiplier: Float = transitionDurationMultiplier + delayMultiplier
            val totalDuration: Float = duration * totalDurationMultiplier
            val delayPoint: Float = delayMultiplier * duration / totalDuration
            midwayPoint = 1 - midwayPoint * (1 - delayPoint)

            kf0 = Keyframe.ofFloat(0F, transitionEndRadiusMultiplier)
            kf1 = Keyframe.ofFloat(delayPoint, transitionEndRadiusMultiplier)
            kf2 = Keyframe.ofFloat(midwayPoint, transitionMidRadiusMultiplier)
            kf3 = Keyframe.ofFloat(1F, 1F)
            val radiusReappear: PropertyValuesHolder =
                PropertyValuesHolder.ofKeyframe(
                    "animationRadiusMultiplier", kf0, kf1, kf2, kf3
                )

            kf0 = Keyframe.ofFloat(0F, 0F)
            kf1 = Keyframe.ofFloat(delayPoint, 0F)
            kf2 = Keyframe.ofFloat(1F, 1F)
            val fadeIn: PropertyValuesHolder =
                PropertyValuesHolder.ofKeyframe("alpha", kf0, kf1, kf2)

            val reappearAnimator: ObjectAnimator =
                ObjectAnimator.ofPropertyValuesHolder(
                    this, radiusReappear, fadeIn
                ).setDuration(totalDuration.toLong())

            reappearAnimator.addUpdateListener(invalidateUpdateListener)

            return reappearAnimator
        }

    fun initialize(
        context: Context,
        controller: TimePickerController,
        hasInnerCircle: Boolean,
        disappearsOut: Boolean,
        selectionDegrees: Int,
        isInnerCircle: Boolean
    ) {
        if (isInitialized) {
            setSelection(selectionDegrees, isInnerCircle, false)
            return
        }

        paint.apply {
            color = controller.accentColor
            isAntiAlias = true
        }

        selectionAlpha = MAX_COLOR_COMPONENT

        // Calculate values for the circle radius size.
        is24HourMode = controller.is24HourMode
        if (is24HourMode) {
            circleRadiusMultiplier = context.getFloat(R.dimen.dtp_circle_radius_multiplier_24HourMode)
        } else {
            circleRadiusMultiplier = context.getFloat(R.dimen.dtp_circle_radius_multiplier)
            amPmCircleRadiusMultiplier = context.getFloat(R.dimen.dtp_ampm_circle_radius_multiplier)
        }

        // Calculate values for the radius size(s) of the numbers circle(s).
        this.hasInnerCircle = hasInnerCircle
        if (hasInnerCircle) {
            innerNumbersRadiusMultiplier = context.getFloat(R.dimen.dtp_numbers_radius_multiplier_inner)
            outerNumbersRadiusMultiplier = context.getFloat(R.dimen.dtp_numbers_radius_multiplier_outer)
        } else {
            numbersRadiusMultiplier = context.getFloat(R.dimen.dtp_numbers_radius_multiplier_normal)
        }
        selectionRadiusMultiplier = context.getFloat(R.dimen.dtp_selection_radius_multiplier)

        // Calculate values for the transition mid-way states.
        animationRadiusMultiplier = 1F
        transitionMidRadiusMultiplier = 1F + 0.05F * if (disappearsOut) -1 else 1
        transitionEndRadiusMultiplier = 1F + 0.3F * if (disappearsOut) 1 else -1
        invalidateUpdateListener = InvalidateUpdateListener()

        setSelection(selectionDegrees, isInnerCircle, false)

        isInitialized = true
    }

    fun setSelection(
        selectionDegrees: Int,
        isInnerCircle: Boolean,
        forceDrawDot: Boolean
    ) {
        this.selectionDegrees = selectionDegrees
        selectionRadians = selectionDegrees * Math.PI / 180
        this.forceDrawDot = forceDrawDot

        if (hasInnerCircle) {
            numbersRadiusMultiplier =
                if (isInnerCircle) {
                    innerNumbersRadiusMultiplier
                } else {
                    outerNumbersRadiusMultiplier
                }
        }
    }

    override fun hasOverlappingRendering(): Boolean {
        return false
    }

    fun getDegreesFromCoords(
        pointX: Float, pointY: Float,
        forceLegal: Boolean,
        isInnerCircle: Array<Boolean>
    ): Int {
        if (!drawValuesReady) {
            return INVALID
        }

        val hypotenuse: Double =
            sqrt(
                pow((pointY - yCenter).toDouble(), 2.0) +
                        pow((pointX - xCenter).toDouble(), 2.0)
            )

        // Check if we're outside the range
        if (hasInnerCircle) {
            if (forceLegal) {
                // If we're told to force the coordinates to be legal, we'll set the isInnerCircle
                // boolean based based off whichever number the coordinates are closer to.
                val innerNumberRadius: Float = circleRadius * innerNumbersRadiusMultiplier
                val distanceToInnerNumber: Double = abs(hypotenuse - innerNumberRadius)
                val outerNumberRadius: Float = circleRadius * outerNumbersRadiusMultiplier
                val distanceToOuterNumber: Double = abs(hypotenuse - outerNumberRadius)

                isInnerCircle[0] = distanceToInnerNumber <= distanceToOuterNumber
            } else {
                // Otherwise, if we're close enough to either number (with the space between the
                // two allotted equally), set the isInnerCircle boolean as the closer one.
                // appropriately, but otherwise return -1.
                val minAllowedHypotenuseForInnerNumber: Float =
                    circleRadius * innerNumbersRadiusMultiplier - selectionRadius
                val maxAllowedHypotenuseForOuterNumber: Float =
                    circleRadius * outerNumbersRadiusMultiplier + selectionRadius
                val halfwayHypotenusePoint: Float =
                    circleRadius *
                            ((outerNumbersRadiusMultiplier + innerNumbersRadiusMultiplier) / 2)

                when (hypotenuse) {
                    in minAllowedHypotenuseForInnerNumber..halfwayHypotenusePoint ->
                        isInnerCircle[0] = true
                    in halfwayHypotenusePoint..maxAllowedHypotenuseForOuterNumber ->
                        isInnerCircle[0] = false
                    else -> return INVALID
                }
            }
        } else {
            // If there's just one circle, we'll need to return -1 if:
            // we're not told to force the coordinates to be legal, and
            // the coordinates' distance to the number is within the allowed distance.
            if (!forceLegal) {
                val distanceToNumber: Double = abs(hypotenuse - lineLength)
                // The max allowed distance will be defined as the distance from the center of the
                // number to the edge of the circle.
                val maxAllowedDistance: Float = circleRadius * (1 - numbersRadiusMultiplier)

                if (distanceToNumber > maxAllowedDistance) {
                    return INVALID
                }
            }
        }


        val opposite: Float = abs(pointY - yCenter)
        val radians: Double = asin(opposite / hypotenuse)
        var degrees: Int = (radians * 180 / Math.PI).toInt()

        // Now we have to translate to the correct quadrant.
        val rightSide = pointX > xCenter
        val topSide = pointY < yCenter

        if (rightSide && topSide) {
            degrees = 90 - degrees
        } else if (rightSide && !topSide) {
            degrees += 90
        } else if (!rightSide && !topSide) {
            degrees = 270 - degrees
        } else if (!rightSide && topSide) {
            degrees += 270
        }
        return degrees
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)

        xCenter = width / 2F
        yCenter = height / 2F
        circleRadius = min(
            min(xCenter, yCenter) * circleRadiusMultiplier,
            context.getDimensionPixelSize(R.dimen.dtp_circle_radius_max).toFloat()
        )

        if (!is24HourMode) {
            // We'll need to draw the AM/PM circles, so the main circle will need to have
            // a slightly higher center. To keep the entire view centered vertically, we'll
            // have to push it up by half the radius of the AM/PM circles.
            val amPmCircleRadius: Float = min(
                circleRadius * amPmCircleRadiusMultiplier,
                context.getDimensionPixelSize(R.dimen.dtp_am_pm_radius_max).toFloat()
            )
            yCenter -= amPmCircleRadius * 0.75F
        }

        selectionRadius = circleRadius * selectionRadiusMultiplier
        drawValuesReady = true
    }

    public override fun onDraw(canvas: Canvas) {
        val viewWidth: Int = width
        if (viewWidth == 0 || !isInitialized) {
            return
        }

        // Calculate the current radius at which to place the selection circle.
        lineLength =
            circleRadius * numbersRadiusMultiplier * animationRadiusMultiplier

        var pointX: Float = (xCenter + (lineLength * sin(selectionRadians))).toFloat()
        var pointY: Float = (yCenter - (lineLength * cos(selectionRadians))).toFloat()

        // Draw the selection circle.
        paint.alpha = selectionAlpha
        canvas.drawCircle(
            pointX, pointY,
            selectionRadius,
            paint
        )

        if (forceDrawDot or (selectionDegrees % 30 != 0)) {
            // We're not on a direct tick (or we've been told to draw the dot anyway).
            paint.alpha = MAX_COLOR_COMPONENT
            canvas.drawCircle(
                pointX, pointY,
                selectionRadius * 2 / 7,
                paint
            )
        } else {
            // We're not drawing the dot, so shorten the line to only go as far as the edge of the
            // selection circle.
            var lineLength: Float = lineLength
            lineLength -= selectionRadius
            pointX = (xCenter + (lineLength * sin(selectionRadians))).toFloat()
            pointY = (yCenter - (lineLength * cos(selectionRadians))).toFloat()
        }

        // Draw the line from the center of the circle.
        paint.apply {
            alpha = 255
            strokeWidth = lineStrokeWidth
        }
        canvas.drawLine(
            xCenter, yCenter,
            pointX, pointY,
            paint
        )
    }

    private inner class InvalidateUpdateListener : AnimatorUpdateListener {
        override fun onAnimationUpdate(animation: ValueAnimator) {
            this@RadialSelectorView.invalidate()
        }
    }
}
