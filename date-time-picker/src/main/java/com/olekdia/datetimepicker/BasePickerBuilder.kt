package com.olekdia.datetimepicker

import android.content.Context
import android.graphics.Color
import android.os.Build
import androidx.annotation.IntRange
import com.olekdia.androidcommon.extensions.getColorCompat
import com.olekdia.androidcommon.extensions.resolveColor
import com.olekdia.common.INVALID
import com.olekdia.common.NumeralSystem
import com.olekdia.common.extensions.ifNotNull
import com.olekdia.common.extensions.isDarkColor
import com.olekdia.common.extensions.withAlpha
import com.olekdia.datetimepicker.common.*
import com.olekdia.datetimepicker.common.PickerIndex.Companion.DAY
import com.olekdia.datetimepicker.common.PickerIndex.Companion.HOUR
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MINUTE
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MONTH
import com.olekdia.datetimepicker.common.PickerIndex.Companion.SECOND
import com.olekdia.datetimepicker.common.PickerIndex.Companion.YEAR
import com.olekdia.datetimepicker.common.RangePart.Companion.END
import com.olekdia.datetimepicker.common.RangePart.Companion.START
import com.olekdia.datetimepicker.interfaces.DtChangeListener
import com.olekdia.datetimepicker.interfaces.DtRangeChangeListener
import com.olekdia.datetimepicker.interfaces.HeaderRangeSelectionListener
import com.olekdia.datetimepicker.interfaces.HeaderSelectionListener
import java.util.*

abstract class BasePickerBuilder<T : BasePickerBuilder<T>>(
    val context: Context
) {

    abstract val self: T

//--------------------------------------------------------------------------------------------------
//  BaseController
//--------------------------------------------------------------------------------------------------

    @JvmField
    internal var primaryTextColor: Int = 0
    @JvmField
    internal var primaryTextColorSet = false
    @JvmField
    internal var secondaryTextColor: Int = 0
    @JvmField
    internal var secondaryTextColorSet = false
    @JvmField
    internal var highlightedTextColor: Int = 0
    @JvmField
    internal var highlightedTextColorSet = false
    @JvmField
    internal var disabledTextColor: Int = 0
    @JvmField
    internal var disabledTextColorSet = false

    @JvmField
    internal var numeralSystem: Int = NumeralSystem.WESTERN_ARABIC

    @JvmField
    internal var is24HourMode: Boolean = false

    @JvmField
    internal val selectedDts: Array<DateTime> = Array(2) { DateTime(0) }

    open var currentRangePart: Int = START
        protected set

    @JvmField
    internal var monthsOfYearLong: Array<String>? = null
    @JvmField
    internal var monthsOfYearShort: Array<String>? = null

    @JvmField
    internal var amPmText: Array<String>? = null

//--------------------------------------------------------------------------------------------------
//  HeaderController
//--------------------------------------------------------------------------------------------------

    open var isRangeAvailable: Boolean = false
        protected set

    @JvmField
    internal val startDtItemsAvailability: BooleanArray = BooleanArray(6) { false }
    @JvmField
    internal val endDtItemsAvailability: BooleanArray = BooleanArray(6) { false }

    @PickerIndex
    @JvmField
    internal var currentItem: Int = NOT_SET

//--------------------------------------------------------------------------------------------------
//  BasePickerController
//--------------------------------------------------------------------------------------------------

    @JvmField
    internal var isDarkTheme: Boolean = false

    @JvmField
    internal var accentColor: Int = 0
    @JvmField
    internal var accentColorSet = false
    @JvmField
    internal var primaryBackgroundColor: Int = 0
    @JvmField
    internal var primaryBackgroundColorSet = false
    @JvmField
    internal var secondaryBackgroundColor: Int = 0
    @JvmField
    internal var secondaryBackgroundColorSet = false

    @JvmField
    internal var vibrate: Boolean = false

//--------------------------------------------------------------------------------------------------
//  DatePickerController
//--------------------------------------------------------------------------------------------------

    @JvmField
    internal var daysOfWeek: Array<String>? = null

    @JvmField
    internal var firstDayOfWeek: Int = INVALID

//--------------------------------------------------------------------------------------------------
//  Common
//--------------------------------------------------------------------------------------------------

    @JvmField
    internal val selectableDt: Array<Array<DateTime>?> = Array(2) { null }
    @JvmField
    internal val minDt: Array<DateTime?> = Array(2) { null }
    @JvmField
    internal val maxDt: Array<DateTime?> = Array(2) { null }

    //allows picker go to choosing next item after previous was selected
    @JvmField
    internal var allowAutoAdvance: Boolean = false

    @JvmField
    internal var headerSelectionListener: HeaderSelectionListener? = null

    @JvmField
    internal var headerRangeSelectionListener: HeaderRangeSelectionListener? = null

    @JvmField
    internal var dtChangeListener: DtChangeListener? = null

    @JvmField
    internal var dtRangeChangeListener: DtRangeChangeListener? = null

//--------------------------------------------------------------------------------------------------
//  HeaderController methods
//--------------------------------------------------------------------------------------------------

    fun currentItem(@PickerIndex currentItem: Int): T {
        this.currentItem = currentItem
        return self
    }

//--------------------------------------------------------------------------------------------------
//  BaseController methods
//--------------------------------------------------------------------------------------------------

    fun numeralSystem(numeralSystem: Int): T {
        this.numeralSystem = numeralSystem
        return self
    }

    fun is24HourMode(is24HourMode: Boolean): T {
        this.is24HourMode = is24HourMode
        return self
    }

    fun monthsOfYearLong(monthsOfYearLong: Array<String>): T {
        checkMonthOfYearValid(monthsOfYearLong)

        this.monthsOfYearLong = monthsOfYearLong
        return self
    }

    fun monthsOfYearShort(monthsOfYearShort: Array<String>): T {
        checkMonthOfYearValid(monthsOfYearShort)

        this.monthsOfYearShort = monthsOfYearShort
        return self
    }

    fun amPmText(amText: String, pmText: String): T {
        this.amPmText = arrayOf(amText, pmText)
        return self
    }

//--------------------------------------------------------------------------------------------------
//  BasePickerController methods
//--------------------------------------------------------------------------------------------------

    fun isDarkTheme(isDarkTheme: Boolean): T {
        this.isDarkTheme = isDarkTheme
        return self
    }

    fun accentColor(accentColor: Int): T {
        this.accentColor = accentColor.withAlpha(1F)
        accentColorSet = true
        return self
    }

    fun primaryBackgroundColor(primaryBackgroundColor: Int): T {
        this.primaryBackgroundColor = primaryBackgroundColor
        primaryBackgroundColorSet = true
        return self
    }

    fun secondaryBackgroundColor(secondaryBackgroundColor: Int): T {
        this.secondaryBackgroundColor = secondaryBackgroundColor
        secondaryBackgroundColorSet = true
        return self
    }

    fun primaryTextColor(primaryTextColor: Int): T {
        this.primaryTextColor = primaryTextColor
        primaryTextColorSet = true
        return self
    }

    fun secondaryTextColor(secondaryTextColor: Int): T {
        this.secondaryTextColor = secondaryTextColor
        secondaryTextColorSet = true
        return self
    }

    fun disabledTextColor(disabledTextColor: Int): T {
        this.disabledTextColor = disabledTextColor
        disabledTextColorSet = true
        return self
    }

    fun highlightedTextColor(highlightedTextColor: Int): T {
        this.highlightedTextColor = highlightedTextColor
        highlightedTextColorSet = true
        return self
    }

    fun vibrate(vibrate: Boolean): T {
        this.vibrate = vibrate
        return self
    }

//--------------------------------------------------------------------------------------------------
//  DatePickerController methods
//--------------------------------------------------------------------------------------------------

    fun daysOfWeek(daysOfWeek: Array<String>): T {
        checkDaysOfWeekValid(daysOfWeek)

        this.daysOfWeek = daysOfWeek
        return self
    }

    fun firstDayOfWeek(
        @IntRange(from = Calendar.SUNDAY.toLong(), to = Calendar.SATURDAY.toLong()) firstDayOfWeek: Int
    ): T {
        checkFirstDayOfWeekValid(firstDayOfWeek)

        this.firstDayOfWeek = firstDayOfWeek
        return self
    }

//--------------------------------------------------------------------------------------------------
//  Common methods
//--------------------------------------------------------------------------------------------------

    fun allowAutoAdvance(allowAutoAdvance: Boolean): T {
        this.allowAutoAdvance = allowAutoAdvance
        return self
    }

//--------------------------------------------------------------------------------------------------
//  Selected
//--------------------------------------------------------------------------------------------------

    protected fun setSelectedDate(
        part: Int,
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ) {
        checkDateHasAtLeastOneValidItem(year, month, day)
        checkDateHasValidValues(year, month, day)
        checkSelectedSetBeforeLimitations(part)

        val itemsAvailability: BooleanArray
        val selectedDt: DateTime

        if (part == START) {
            itemsAvailability = startDtItemsAvailability
            selectedDt = selectedDts[START]
        } else {
            itemsAvailability = endDtItemsAvailability
            selectedDt = selectedDts[END]
        }

        if (year != NOT_SET) {
            itemsAvailability[YEAR] = true
            selectedDt.year = year
        }

        if (month != NOT_SET) {
            itemsAvailability[MONTH] = true
            selectedDt.month = month
        }

        if (day != NOT_SET) {
            itemsAvailability[DAY] = true
            selectedDt.day = day
        }
    }

    protected fun setSelectedTime(
        part: Int,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ) {
        checkTimeHasAtLeastOneValidItem(hour, minute, second)
        checkTimeHasValidValues(hour, minute, second)
        checkSelectedSetBeforeLimitations(part)

        val itemsAvailability: BooleanArray
        val selectedDt: DateTime

        if (part == START) {
            itemsAvailability = startDtItemsAvailability
            selectedDt = selectedDts[START]
        } else {
            itemsAvailability = endDtItemsAvailability
            selectedDt = selectedDts[END]
        }

        if (hour != NOT_SET) {
            itemsAvailability[HOUR] = true
            selectedDt.hour24 = hour
        }

        if (minute != NOT_SET) {
            itemsAvailability[MINUTE] = true
            selectedDt.minute = minute
        }

        if (second != NOT_SET) {
            itemsAvailability[SECOND] = true
            selectedDt.second = second
        }
    }

    protected fun setSelectedDt(
        part: Int,
        dt: DateTime
    ) {
        checkSelectedSetBeforeLimitations(part)

        val itemsAvailability: BooleanArray =
            if (part == START) {
                startDtItemsAvailability
            } else {
                endDtItemsAvailability
            }

        for (i in YEAR..SECOND) {
            itemsAvailability[i] = true
        }
        selectedDts[part] = dt
    }

//--------------------------------------------------------------------------------------------------
//  Selectable
//--------------------------------------------------------------------------------------------------

    protected fun setSelectableDate(
        part: Int,
        vararg dates: Triple<Int, Int, Int>
    ) {
        val size: Int = dates.size

        if (size > 0) {
            if (selectableDt[part] == null) {
                selectableDt[part] = Array(size) { DateTime(0) }
            } else {
                checkSelectableTimeAndDateHaveSameSize(part, size)
            }

            for (i in dates.indices) {
                val date: Triple<Int, Int, Int> = dates[i]
                checkDateHasValidValues(date.first, date.second, date.third)
                checkLimitationsHaveSameDateItems(
                    part,
                    date.toList()
                )

                selectableDt[part]?.get(i)
                    ?.apply {
                        if (date.first != NOT_SET) year = date.first
                        if (date.second != NOT_SET) month = date.second
                        if (date.third != NOT_SET) day = date.third
                    }
            }
        }
    }

    protected fun setSelectableTime(
        part: Int,
        vararg times: Triple<Int, Int, Int>
    ) {
        val size: Int = times.size

        if (size > 0) {
            if (selectableDt[part] == null) {
                selectableDt[part] = Array(size) { DateTime(0) }
            } else {
                checkSelectableTimeAndDateHaveSameSize(part, size)
            }

            for (i in times.indices) {
                val time: Triple<Int, Int, Int> = times[i]
                checkTimeHasValidValues(time.first, time.second, time.third)
                checkLimitationsHaveSameTimeItems(
                    part,
                    time.toList()
                )

                selectableDt[part]?.get(i)
                    ?.apply {
                        if (time.first != NOT_SET) hour24 = time.first
                        if (time.second != NOT_SET) minute = time.second
                        if (time.third != NOT_SET) second = time.third
                    }
            }
        }
    }

    protected fun setSelectableDt(
        part: Int,
        selectableDt: Array<DateTime>?
    ) {
        this.selectableDt[part] = selectableDt
    }

//--------------------------------------------------------------------------------------------------
// Min
//--------------------------------------------------------------------------------------------------

    protected fun setMinDate(
        part: Int,
        year: Int,
        month: Int,
        day: Int
    ) {
        checkDateHasValidValues(year, month, day)
        checkLimitationsHaveSameDateItems(
            part,
            listOf(year, month, day)
        )

        if (minDt[part] == null) {
            minDt[part] = DateTime(0)
        }

        minDt[part]?.apply {
            if (year != NOT_SET) this.year = year
            if (month != NOT_SET) this.month = month
            if (day != NOT_SET) this.day = day
        }
    }

    protected fun setMinTime(
        part: Int,
        hour: Int,
        minute: Int,
        second: Int
    ) {
        checkTimeHasValidValues(hour, minute, second)
        checkLimitationsHaveSameTimeItems(
            part,
            listOf(hour, minute, second)
        )

        if (minDt[part] == null) {
            minDt[part] = DateTime(0)
        }

        minDt[part]?.apply {
            if (hour != NOT_SET) this.hour24 = hour
            if (minute != NOT_SET) this.minute = minute
            if (second != NOT_SET) this.second = second
        }
    }

    protected fun setMinDt(
        part: Int,
        minDt: DateTime?
    ) {
        this.minDt[part] = minDt
    }

//--------------------------------------------------------------------------------------------------
// Max
//--------------------------------------------------------------------------------------------------

    protected fun setMaxDate(
        part: Int,
        year: Int,
        month: Int,
        day: Int
    ) {
        checkDateHasValidValues(year, month, day)
        checkLimitationsHaveSameDateItems(
            part,
            listOf(year, month, day)
        )

        if (maxDt[part] == null) {
            maxDt[part] = DateTime(0)
        }

        maxDt[part]?.apply {
            if (year != NOT_SET) this.year = year
            if (month != NOT_SET) this.month = month
            if (day != NOT_SET) this.day = day
        }
    }

    protected fun setMaxTime(
        part: Int,
        hour: Int,
        minute: Int,
        second: Int
    ) {
        checkTimeHasValidValues(hour, minute, second)
        checkLimitationsHaveSameTimeItems(
            part,
            listOf(hour, minute, second)
        )

        if (maxDt[part] == null) {
            maxDt[part] = DateTime(0)
        }

        maxDt[part]?.apply {
            if (hour != NOT_SET) this.hour24 = hour
            if (minute != NOT_SET) this.minute = minute
            if (second != NOT_SET) this.second = second
        }
    }

    protected fun setMaxDt(
        part: Int,
        maxDt: DateTime?
    ) {
        this.maxDt[part] = maxDt
    }

//--------------------------------------------------------------------------------------------------
//  Build
//--------------------------------------------------------------------------------------------------

    fun build(): Picker {
        invalidateColors()
        invalidateDts()
        invalidateLimitations()

        checkIsValuesValid()

        return Picker(context, this)
    }

    private fun invalidateColors() {
        if (!accentColorSet) {
            val accentColorFallback: Int = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                context.resolveColor(android.R.attr.colorAccent)
            } else {
                0
            }
            accentColor = context.resolveColor(
                R.attr.dtp_accent_color,
                accentColorFallback
            )
        }

        if (!primaryBackgroundColorSet) {
            val primaryBgColorFallback: Int = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                context.resolveColor(android.R.attr.colorPrimary)
            } else {
                0
            }
            primaryBackgroundColor = context.resolveColor(
                R.attr.dtp_primary_background_color,
                primaryBgColorFallback
            )
        }

        if (!secondaryBackgroundColorSet) {
            val secondaryBgColorFallback: Int = context.resolveColor(android.R.attr.colorBackground)
            secondaryBackgroundColor = context.resolveColor(
                R.attr.dtp_secondary_background_color,
                secondaryBgColorFallback
            )
        }

        isDarkTheme = secondaryBackgroundColor.isDarkColor()

        if (!primaryTextColorSet) {
            primaryTextColor = context.resolveColor(android.R.attr.textColorPrimary)
        }

        if (!secondaryTextColorSet) {
            secondaryTextColor = context.resolveColor(android.R.attr.textColorSecondary)
        }

        if (!highlightedTextColorSet) {
            highlightedTextColor = Color.WHITE
        }

        if (!disabledTextColorSet) {
            val disabledColorFallback: Int = context.getColorCompat(
                if (isDarkTheme) {
                    R.color.dtp_disabled_text_lt
                } else {
                    R.color.dtp_disabled_text_dt
                }
            )
            disabledTextColor = context.resolveColor(
                R.attr.dtp_disabled_text_color,
                disabledColorFallback
            )
        }
    }

    private fun invalidateDts() {
        val defaultDt = DateTime(0)
        var defaultValue: Int

        for (i in YEAR..SECOND) {
            if (!startDtItemsAvailability[i]) {
                defaultValue = defaultDt.get(i)

                selectedDts[START].set(i, defaultValue)

                minDt[START]?.set(i, defaultValue)
                maxDt[START]?.set(i, defaultValue)

                selectableDt[START]?.let {
                    for (dt in it) {
                        dt.set(i, defaultValue)
                    }
                }
            }
        }

        if (isRangeAvailable) {
            for (i in YEAR..SECOND) {
                if (!endDtItemsAvailability[i]) {
                    defaultValue = defaultDt.get(i)

                    selectedDts[END].set(i, defaultValue)

                    minDt[END]?.set(i, defaultValue)
                    maxDt[END]?.set(i, defaultValue)

                    selectableDt[END]?.let {
                        for (dt in it) {
                            dt.set(i, defaultValue)
                        }
                    }
                }
            }
        }
    }

    private fun invalidateLimitations() {
        selectableDt[START]
            ?.let {
                Arrays.sort(it)
            }

        selectableDt[END]
            ?.let {
                Arrays.sort(it)
            }
    }

//--------------------------------------------------------------------------------------------------
//  Requirements
//--------------------------------------------------------------------------------------------------

    protected open fun checkIsValuesValid() {
        checkSelectedRangePartAndItem()
        checkMinMax(START)
    }

    private fun checkSelectedRangePartAndItem() {
        require(
            currentRangePart in START..END
        ) { "invalid range part" }

        val itemsAvailability: BooleanArray =
            if (currentRangePart == START) {
                startDtItemsAvailability
            } else {
                endDtItemsAvailability
            }

        if (currentItem == NOT_SET) {
            for (i in DAY downTo YEAR) {
                if (itemsAvailability[i]) {
                    currentItem = i
                    break
                }
            }
        }

        if (currentItem == NOT_SET) {
            for (i in HOUR..SECOND) {
                if (itemsAvailability[i]) {
                    currentItem = i
                    break
                }
            }
        }

        require(
            currentItem in YEAR..SECOND
        ) { "invalid item index" }

        require(
            itemsAvailability[currentItem]
        ) { "item disable but selected" }
    }

    protected fun checkMinMax(part: Int) {
        ifNotNull(
            minDt[part],
            maxDt[part]
        ) { min, max ->
            require(
                min <= max
            ) { if (part == START) "start min > start max" else "end min > end max" }
        }
    }

    private fun checkSelectedSetBeforeLimitations(part: Int) {
        require(
            minDt[part] == null &&
                    maxDt[part] == null &&
                    selectableDt[part] == null
        ) { "selected must be set before limitations" }
    }

    private fun checkDateHasAtLeastOneValidItem(
        year: Int,
        month: Int,
        day: Int
    ) {
        val isValid = year != NOT_SET ||
                month != NOT_SET ||
                day != NOT_SET

        require(
            isValid
        ) { "at least one item should be valid" }
    }

    private fun checkTimeHasAtLeastOneValidItem(
        hour: Int,
        minute: Int,
        second: Int
    ) {
        val isValid = hour != NOT_SET ||
                minute != NOT_SET ||
                second != NOT_SET

        require(
            isValid
        ) { "at least one item should be valid" }
    }

    private fun checkDateHasValidValues(
        year: Int,
        month: Int,
        day: Int
    ) {
        require(
            year in 1900..2200 ||
                    year == NOT_SET
        ) { "year must be in range [1900 - 2200]" }

        require(
            month in Calendar.JANUARY..Calendar.DECEMBER ||
                    month == NOT_SET
        ) { "month must be in range [0 - 11]" }

        val checkCalendar: Calendar = Calendar.getInstance()
        checkCalendar.apply {
            set(Calendar.DAY_OF_MONTH, 1)
            set(Calendar.MONTH, month)
            set(Calendar.YEAR, year)
        }

        val daysInMonth: Int = checkCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)

        require(
            day in 1..daysInMonth ||
                    day == NOT_SET
        ) { "day must be in range [1 - num days in month]" }
    }

    private fun checkTimeHasValidValues(
        hour: Int,
        minute: Int,
        second: Int
    ) {
        require(
            hour in 0..23 ||
                    hour == NOT_SET
        ) { "hour must be in range [0 - 23]" }

        require(
            minute in 0..59 ||
                    minute == NOT_SET
        ) { "minute must be in range [0 - 59]" }

        require(
            second in 0..59 ||
                    second == NOT_SET
        ) { "second must be in range [0 - 59]" }
    }

    private fun checkLimitationsHaveSameDateItems(
        part: Int,
        date: List<Int>
    ) {
        val itemsAvailability: BooleanArray =
            if (part == START) {
                startDtItemsAvailability
            } else {
                endDtItemsAvailability
            }

        for (i in date.indices) {
            if (itemsAvailability[i]) {
                require(
                    date[i] != NOT_SET
                ) { "limitations must have same values as selected" }
            }
        }
    }

    private fun checkLimitationsHaveSameTimeItems(
        part: Int,
        time: List<Int>
    ) {
        val itemsAvailability: BooleanArray =
            if (part == START) {
                startDtItemsAvailability
            } else {
                endDtItemsAvailability
            }

        for (i in time.indices) {
            if (itemsAvailability[i + 3]) {
                require(
                    time[i] != NOT_SET
                ) { "limitations must have same values as selected" }
            }
        }
    }

    private fun checkSelectableTimeAndDateHaveSameSize(
        part: Int,
        size: Int
    ) {
        require(
            selectableDt[part]?.size == size
        ) { "selected date and time must be the same size" }
    }

    private fun checkDaysOfWeekValid(daysOfWeek: Array<String>) {
        require(
            daysOfWeek.size == DAYS_IN_WEEK
        ) { "array must contain only values for all days of week" }
    }

    private fun checkMonthOfYearValid(monthsOfYear: Array<String>) {
        require(
            monthsOfYear.size == MONTHS_IN_YEAR
        ) { "array must contain only values for all months of year" }
    }

    private fun checkFirstDayOfWeekValid(firstDayOfWeek: Int) {
        require(
            firstDayOfWeek in Calendar.SUNDAY..Calendar.SATURDAY
        ) { "firstDayOfWeek must be in range ${Calendar.SUNDAY}..${Calendar.SATURDAY}" }
    }
}