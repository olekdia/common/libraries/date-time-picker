package com.olekdia.datetimepicker

import android.content.Context
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.common.NOT_SET
import com.olekdia.datetimepicker.common.RangePart.Companion.START
import com.olekdia.datetimepicker.interfaces.DtChangeListener
import com.olekdia.datetimepicker.interfaces.HeaderSelectionListener

class PickerBuilder(
    context: Context
) : BasePickerBuilder<PickerBuilder>(context) {

    override val self: PickerBuilder
        get() = this

//--------------------------------------------------------------------------------------------------
//  BaseController
//--------------------------------------------------------------------------------------------------

    override var currentRangePart: Int = START

    @JvmOverloads
    fun selectedDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): PickerBuilder {
        setSelectedDate(
            START,
            year, month, day
        )

        return this
    }

    @JvmOverloads
    fun selectedTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerBuilder {
        setSelectedTime(
            START,
            hour, minute, second
        )

        return this
    }

    @JvmOverloads
    fun selectedDateTime(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerBuilder {
        setSelectedDate(
            START,
            year, month, day
        )
        setSelectedTime(
            START,
            hour, minute, second
        )

        return this
    }

    fun selectedDt(dt: DateTime): PickerBuilder {
        setSelectedDt(
            START,
            dt
        )

        return this
    }

//--------------------------------------------------------------------------------------------------
//  HeaderController
//--------------------------------------------------------------------------------------------------

    override var isRangeAvailable: Boolean = false

//--------------------------------------------------------------------------------------------------
//  Common
//--------------------------------------------------------------------------------------------------

    //selectable

    fun selectableDate(vararg dates: Triple<Int, Int, Int>): PickerBuilder {
        setSelectableDate(
            START,
            *dates
        )

        return this
    }

    fun selectableTime(vararg times: Triple<Int, Int, Int>): PickerBuilder {
        setSelectableTime(
            START,
            *times
        )

        return this
    }

    fun selectableDt(selectableDt: Array<DateTime>?): PickerBuilder {
        setSelectableDt(
            START,
            selectableDt
        )

        return this
    }

    //min

    fun minDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): PickerBuilder {
        setMinDate(
            START,
            year, month, day
        )

        return this
    }

    fun minTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerBuilder {
        setMinTime(
            START,
            hour, minute, second
        )

        return this
    }

    fun minDateTime(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerBuilder {
        setMinDate(
            START,
            year, month, day
        )
        setMinTime(
            START,
            hour, minute, second
        )

        return this
    }

    fun minDt(minDt: DateTime?): PickerBuilder {
        setMinDt(
            START,
            minDt
        )

        return this
    }

    //max

    fun maxDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): PickerBuilder {
        setMaxDate(
            START,
            year, month, day
        )

        return this
    }

    fun maxTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerBuilder {
        setMaxTime(
            START,
            hour, minute, second
        )

        return this
    }

    fun maxDateTime(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): PickerBuilder {
        setMaxDate(
            START,
            year, month, day
        )
        setMaxTime(
            START,
            hour, minute, second
        )

        return this
    }

    fun maxDt(maxDt: DateTime?): PickerBuilder {
        setMaxDt(
            START,
            maxDt
        )

        return this
    }

    fun headerSelectionListener(headerSelectionListener: HeaderSelectionListener): PickerBuilder {
        this.headerSelectionListener = headerSelectionListener
        return this
    }

    fun dtChangeListener(dtChangeListener: DtChangeListener): PickerBuilder {
        this.dtChangeListener = dtChangeListener
        return this
    }
}