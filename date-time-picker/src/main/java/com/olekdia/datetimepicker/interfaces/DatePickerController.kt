package com.olekdia.datetimepicker.interfaces

import com.olekdia.datetimepicker.common.DateTime

interface DatePickerController :
    BasePickerController {

    val startDt: DateTime
    val endDt: DateTime

    val minYear: Int
    val maxYear: Int

    val firstDayOfWeek: Int
    var daysOfWeek: Array<String>

    fun onDaySelected(year: Int, month: Int, day: Int)
    fun onMonthSelected(year: Int, month: Int)
    fun onYearSelected(year: Int)
}
