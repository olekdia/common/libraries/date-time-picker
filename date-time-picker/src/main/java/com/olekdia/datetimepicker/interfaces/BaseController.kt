package com.olekdia.datetimepicker.interfaces

import com.olekdia.datetimepicker.common.DateTime

interface BaseController {

    val primaryTextColor: Int
    val secondaryTextColor: Int
    val highlightedTextColor: Int
    val disabledTextColor: Int

    val numeralSystem: Int

    val is24HourMode: Boolean

    val isYearAvailable: Boolean
    val isMonthAvailable: Boolean
    val isDayAvailable: Boolean

    val isHourAvailable: Boolean
    val isMinuteAvailable: Boolean
    val isSecondAvailable: Boolean

    val monthsOfYearLong: Array<String>
    val monthsOfYearShort: Array<String>
    val amText: String
    val pmText: String

    val selectedDts: Array<DateTime>
    var currentRangePart: Int
    val currentSelectedDt: DateTime
}