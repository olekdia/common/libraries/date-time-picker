package com.olekdia.datetimepicker.interfaces

interface HeaderSelectionListener {

    fun onHeaderSelection(index: Int)

}