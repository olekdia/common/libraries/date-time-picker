package com.olekdia.datetimepicker.interfaces

import com.olekdia.datetimepicker.common.DateTime

interface TimePickerController :
    BasePickerController {

    fun isAmDisabled(part: Int = currentRangePart): Boolean
    fun isPmDisabled(part: Int = currentRangePart): Boolean

    fun onTimeSelected(
        dt: DateTime,
        index: Int,
        autoAdvance: Boolean = false
    )
}
