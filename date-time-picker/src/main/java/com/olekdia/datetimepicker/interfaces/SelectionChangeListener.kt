package com.olekdia.datetimepicker.interfaces

interface SelectionChangeListener {

    fun onHeaderSelectionChanged(changePicker: Boolean, updatePicker: Boolean)
    fun onAmPmSelectionChanged(updatePicker: Boolean, amPm: Int, part: Int)

    fun onDtSelectionChanged(changePicker: Boolean, updatePicker: Boolean = false)
}