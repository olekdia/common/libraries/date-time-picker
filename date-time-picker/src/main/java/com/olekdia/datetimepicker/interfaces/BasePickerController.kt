package com.olekdia.datetimepicker.interfaces

import com.olekdia.datetimepicker.common.DateTime
import java.util.*

interface BasePickerController :
    BaseController {

    val isDarkTheme: Boolean

    val accentColor: Int
    val primaryBackgroundColor: Int
    val secondaryBackgroundColor: Int

    fun tryVibrate()

    fun isOutOfRange(index: Int, dt: DateTime): Boolean
    fun isOutOfRange(
        index: Int,
        year: Int,
        month: Int = Calendar.JANUARY,
        day: Int = 1
    ): Boolean

    fun roundToNearest(
        dt: DateTime,
        index: Int,
        checkPartOfDay: Boolean = false
    )

    fun roundToNearest(
        dt: DateTime,
        index: Int,
        rangePart: Int,
        checkPartOfDay: Boolean = false
    )
}