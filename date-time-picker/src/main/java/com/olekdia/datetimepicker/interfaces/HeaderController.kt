package com.olekdia.datetimepicker.interfaces

import com.olekdia.datetimepicker.common.PickerIndex
import com.olekdia.datetimepicker.common.RangePart

interface HeaderController :
    BaseController {

    val isRangeAvailable: Boolean

    val startDtItemsAvailability: BooleanArray
    val endDtItemsAvailability: BooleanArray

    val isDateAvailable: Boolean
    val isTimeAvailable: Boolean

    @PickerIndex
    var currentItem: Int

    fun onPickHeaderItem(
        @PickerIndex index: Int,
        @RangePart part: Int
    )

    fun onToggleAmPm(
        @RangePart part: Int
    )
}