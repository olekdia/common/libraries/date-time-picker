package com.olekdia.datetimepicker.interfaces

interface DtChangeListener {

    fun onDateChanged(
        year: Int, month: Int, day: Int,
        hour: Int, minute: Int, second: Int
    )

}