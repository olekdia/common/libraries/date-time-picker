package com.olekdia.datetimepicker.interfaces

interface DtRangeChangeListener {

    fun onRangeDateChanged(
        startYear: Int, startMonth: Int, startDay: Int,
        startHour: Int, startMinute: Int, startSecond: Int,

        endYear: Int, endMonth: Int, endDay: Int,
        endHour: Int, endMinute: Int, endSecond: Int
    )

}