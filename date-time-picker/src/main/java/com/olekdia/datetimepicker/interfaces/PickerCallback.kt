package com.olekdia.datetimepicker.interfaces

import com.olekdia.datetimepicker.common.DateTime

interface PickerCallback {

    fun getSelectedDate(): Triple<Int, Int, Int>
    fun getSelectedTime(): Triple<Int, Int, Int>
    fun getSelectedDt(): DateTime

    fun getSelectedStartDate(): Triple<Int, Int, Int>?
    fun getSelectedStartTime(): Triple<Int, Int, Int>?
    fun getSelectedStartDt(): DateTime?

    fun getSelectedEndDate(): Triple<Int, Int, Int>?
    fun getSelectedEndTime(): Triple<Int, Int, Int>?
    fun getSelectedEndDt(): DateTime?
}