package com.olekdia.datetimepicker.interfaces

interface HeaderRangeSelectionListener {

    fun onHeaderSelection(index: Int, part: Int)

}