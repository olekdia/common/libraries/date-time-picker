package com.olekdia.datetimepicker

import android.content.Context
import com.olekdia.common.INVALID
import com.olekdia.common.INVALID_L
import com.olekdia.common.extensions.ifNotNull
import com.olekdia.common.extensions.roundTo
import com.olekdia.datetimepicker.common.*
import com.olekdia.datetimepicker.common.PickerIndex.Companion.DAY
import com.olekdia.datetimepicker.common.PickerIndex.Companion.HOUR
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MINUTE
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MONTH
import com.olekdia.datetimepicker.common.PickerIndex.Companion.SECOND
import com.olekdia.datetimepicker.common.PickerIndex.Companion.YEAR
import com.olekdia.datetimepicker.common.RangePart.Companion.END
import com.olekdia.datetimepicker.common.RangePart.Companion.START
import com.olekdia.datetimepicker.interfaces.*
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.max

class Controller constructor(
    context: Context,
    private val builder: BasePickerBuilder<*>,
    private val selectionChangeListener: SelectionChangeListener,
    private val headerSelectionListener: HeaderSelectionListener? = null,
    private val headerRangeSelectionListener: HeaderRangeSelectionListener? = null,
    private val dtChangeListener: DtChangeListener? = null,
    private val dtRangeChangeListener: DtRangeChangeListener? = null
) : HeaderController,
    TimePickerController,
    DatePickerController {

    private val selectableDt: Array<Array<DateTime>?>
    val minDt: Array<DateTime?>
    private val maxDt: Array<DateTime?>

    private val allowAutoAdvance: Boolean
        get() = builder.allowAutoAdvance

    private val calendar: Calendar = Calendar.getInstance()

//--------------------------------------------------------------------------------------------------
//  BaseController
//--------------------------------------------------------------------------------------------------

    override val primaryTextColor: Int
        get() = builder.primaryTextColor

    override val secondaryTextColor: Int
        get() = builder.secondaryTextColor

    override val highlightedTextColor: Int
        get() = builder.highlightedTextColor

    override val disabledTextColor: Int
        get() = builder.disabledTextColor

    override val numeralSystem: Int
        get() = builder.numeralSystem

    override val is24HourMode: Boolean
        get() = builder.is24HourMode

    override val isYearAvailable: Boolean
        get() = startDtItemsAvailability[YEAR] ||
                endDtItemsAvailability[YEAR]

    override val isMonthAvailable: Boolean
        get() = startDtItemsAvailability[MONTH] ||
                endDtItemsAvailability[MONTH]

    override val isDayAvailable: Boolean
        get() = startDtItemsAvailability[DAY] ||
                endDtItemsAvailability[DAY]

    override val isHourAvailable: Boolean
        get() = startDtItemsAvailability[HOUR] ||
                endDtItemsAvailability[HOUR]

    override val isMinuteAvailable: Boolean
        get() = startDtItemsAvailability[MINUTE] ||
                endDtItemsAvailability[MINUTE]

    override val isSecondAvailable: Boolean
        get() = startDtItemsAvailability[SECOND] ||
                endDtItemsAvailability[SECOND]

    private var _monthsOfYearLong: Array<String> = Array(MONTHS_IN_YEAR) { "" }
    override val monthsOfYearLong: Array<String>
        get() = _monthsOfYearLong

    private var _monthsOfYearShort: Array<String> = Array(MONTHS_IN_YEAR) { "" }
    override val monthsOfYearShort: Array<String>
        get() = _monthsOfYearShort

    private var _amText: String = ""
    override val amText: String
        get() = _amText

    private var _pmText: String = ""
    override val pmText: String
        get() = _pmText

    override val selectedDts: Array<DateTime>
    override var currentRangePart: Int = START
    override val currentSelectedDt: DateTime
        get() = selectedDts[currentRangePart]

//--------------------------------------------------------------------------------------------------
//  HeaderController
//--------------------------------------------------------------------------------------------------

    override val isRangeAvailable: Boolean
        get() = builder.isRangeAvailable

    override val startDtItemsAvailability: BooleanArray
        get() = builder.startDtItemsAvailability
    override val endDtItemsAvailability: BooleanArray
        get() = builder.endDtItemsAvailability

    override val isDateAvailable: Boolean
        get() = isYearAvailable ||
                isMonthAvailable ||
                isDayAvailable

    override val isTimeAvailable: Boolean
        get() = isHourAvailable ||
                isMinuteAvailable ||
                isSecondAvailable

    var previousItem: Int = NOT_SET
    override var currentItem: Int = NOT_SET

//--------------------------------------------------------------------------------------------------
//  BasePickerController
//--------------------------------------------------------------------------------------------------

    override val isDarkTheme: Boolean
        get() = builder.isDarkTheme

    override val accentColor: Int
        get() = builder.accentColor
    override val primaryBackgroundColor: Int
        get() = builder.primaryBackgroundColor
    override val secondaryBackgroundColor: Int
        get() = builder.secondaryBackgroundColor

    val hapticFeedbackController: HapticFeedbackController =
        HapticFeedbackController(context)

    override fun tryVibrate() {
        if (builder.vibrate) {
            hapticFeedbackController.tryVibrate()
        }
    }

    override fun isOutOfRange(index: Int, dt: DateTime): Boolean =
        dt.isOutOfRange(
            index,
            selectableDt[currentRangePart],
            minDt[currentRangePart], maxDt[currentRangePart]
        )

    override fun isOutOfRange(index: Int, year: Int, month: Int, day: Int): Boolean =
        isOutOfRange(
            year, month, day,
            index,
            selectableDt[currentRangePart],
            minDt[currentRangePart], maxDt[currentRangePart]
        )

    override fun roundToNearest(dt: DateTime, index: Int, checkPartOfDay: Boolean) =
        dt.roundToNearest(
            index,
            selectableDt[currentRangePart],
            minDt[currentRangePart], maxDt[currentRangePart],
            checkPartOfDay
        )

    override fun roundToNearest(
        dt: DateTime,
        index: Int,
        rangePart: Int,
        checkPartOfDay: Boolean
    ) {
        dt.roundToNearest(
            index,
            selectableDt[rangePart],
            minDt[rangePart], maxDt[rangePart],
            checkPartOfDay
        )
    }

//--------------------------------------------------------------------------------------------------
//  TimePickerController
//--------------------------------------------------------------------------------------------------

    override fun isAmDisabled(part: Int): Boolean =
        selectedDts[part].isAmPmDisable(
            Calendar.AM,
            selectableDt[part],
            minDt[part], maxDt[part]
        )

    override fun isPmDisabled(part: Int): Boolean =
        selectedDts[part].isAmPmDisable(
            Calendar.PM,
            selectableDt[part],
            minDt[part], maxDt[part]
        )

//--------------------------------------------------------------------------------------------------
//  DatePickerController
//--------------------------------------------------------------------------------------------------

    override val startDt: DateTime
        get() = findStartDate(
            selectableDt[currentRangePart],
            minDt[currentRangePart],
            minYear
        )

    override val endDt: DateTime
        get() = findEndDate(
            selectableDt[currentRangePart],
            maxDt[currentRangePart],
            maxYear
        )

    override val minYear: Int = (calendar.get(Calendar.YEAR) - 100).roundTo(100)
        get() {
            val year: Int =
                findMinYear(
                    selectableDt[currentRangePart],
                    minDt[currentRangePart],
                    field
                )

            return if (year == INVALID) field else year
        }
    override val maxYear: Int = (calendar.get(Calendar.YEAR) + 200).roundTo(100)
        get() {
            val year: Int =
                findMaxYear(
                    selectableDt[currentRangePart],
                    maxDt[currentRangePart],
                    field
                )

            return if (year == INVALID) field else year
        }

    override val firstDayOfWeek: Int = if (builder.firstDayOfWeek == INVALID) {
        calendar.firstDayOfWeek
    } else {
        builder.firstDayOfWeek
    }

    override var daysOfWeek: Array<String> = Array(DAYS_IN_WEEK) { "" }

//--------------------------------------------------------------------------------------------------
//  Constructors, init
//--------------------------------------------------------------------------------------------------

    init {
        currentRangePart = builder.currentRangePart
        currentItem = builder.currentItem

        selectableDt = builder.selectableDt
        minDt = arrayOfNulls(2)
        minDt[START] = builder.minDt[START]
        builder.minDt[END]
            ?.let {
                minDt[END] = DateTime(it.timeInMillis)
            }
        maxDt = builder.maxDt

        selectedDts = builder.selectedDts
        roundToNearest(selectedDts[START], NOT_SET, START)
        roundToNearest(selectedDts[END], NOT_SET, END)

        updatePossibleSelection()

        builder.amPmText?.let {
            _amText = it[0]
            _pmText = it[1]
        } ?: createAmPmLabels()

        builder.daysOfWeek?.let {
            daysOfWeek = it
        } ?: createDaysOfWeek()

        ifNotNull(
            builder.monthsOfYearLong,
            builder.monthsOfYearShort
        ) { long, short ->
            _monthsOfYearLong = long
            _monthsOfYearShort = short
        } ?: createMonthOfYear()
    }

//--------------------------------------------------------------------------------------------------
//  Create values methods
//--------------------------------------------------------------------------------------------------

    private fun createAmPmLabels() {
        if (!startDtItemsAvailability[HOUR]
            && !endDtItemsAvailability[HOUR]
        ) {
            return
        }

        val amPmTexts: Array<String> = DateFormatSymbols().amPmStrings
        _amText = amPmTexts[0]
        _pmText = amPmTexts[1]
    }

    private fun createDaysOfWeek() {
        if (!startDtItemsAvailability[DAY]
            && !endDtItemsAvailability[DAY]
        ) {
            return
        }

        val calendar: Calendar = Calendar.getInstance()

        var day = Calendar.MONDAY
        for (i in 0 until DAYS_IN_WEEK) {
            calendar.set(Calendar.DAY_OF_WEEK, day)
            daysOfWeek[i] = calendar.getDisplayName(
                Calendar.DAY_OF_WEEK,
                Calendar.SHORT,
                Locale.getDefault()
            ) ?: ""
            day = day % DAYS_IN_WEEK + 1
        }
    }

    private fun createMonthOfYear() {
        if (!startDtItemsAvailability[MONTH]
            && !endDtItemsAvailability[MONTH]
        ) {
            return
        }

        val calendar: Calendar = Calendar.getInstance()
            .apply {
                set(Calendar.DAY_OF_MONTH, 1)
            }

        val formatterLong = SimpleDateFormat("LLLL", Locale.getDefault())
        val formatterShort = SimpleDateFormat("LLL", Locale.getDefault())

        for (month in Calendar.JANUARY..Calendar.DECEMBER) {
            calendar.set(Calendar.MONTH, month)

            monthsOfYearLong[month] = formatterLong.format(calendar.time)
            monthsOfYearShort[month] = formatterShort.format(calendar.time)
        }
    }

//--------------------------------------------------------------------------------------------------
//  Event handlers
//--------------------------------------------------------------------------------------------------

    override fun onPickHeaderItem(index: Int, part: Int) {
        tryVibrate()

        val changePicker: Boolean = currentItem != index
        val updatePicker: Boolean = currentRangePart != part

        if (changePicker || updatePicker) {
            currentRangePart = part
            previousItem = currentItem
            currentItem = index
        }

        selectionChangeListener.onHeaderSelectionChanged(changePicker, updatePicker)
        onHeaderSelectionChanged()
    }

    override fun onToggleAmPm(part: Int) {
        if (isAmDisabled(part) || isPmDisabled(part)) {
            return
        }

        tryVibrate()

        var startDistanceMillis: Long = INVALID_L
        if (isRangeAvailable && part == START) {
            startDistanceMillis =
                selectedDts[END].distance(selectedDts[START])
        }

        val amPm: Int = if (selectedDts[part].amPm == Calendar.AM) Calendar.PM else Calendar.AM
        selectedDts[part].amPm = amPm
        roundToNearest(selectedDts[part], HOUR, true)

        val updatePicker: Boolean =
            updateStartToEndDistance(startDistanceMillis) ||
                    currentItem in HOUR..SECOND
        updatePossibleSelection()

        selectionChangeListener.onAmPmSelectionChanged(updatePicker, amPm, part)

        notifyDtsChangeListener()
    }

    override fun onYearSelected(year: Int) {
        var startDistanceMillis: Long = INVALID_L
        if (currentRangePart == START) {
            startDistanceMillis =
                selectedDts[END].distance(selectedDts[START])
        }

        currentSelectedDt.day = invalidateDayOfMonth(
            year,
            currentSelectedDt.month,
            currentSelectedDt.day
        )
        currentSelectedDt.year = year

        roundToNearest(currentSelectedDt, MONTH)

        updateStartToEndDistance(startDistanceMillis)
        updatePossibleSelection()

        val changePicker: Boolean = advancePicker(YEAR)
        selectionChangeListener.onDtSelectionChanged(changePicker)

        notifyDtsChangeListener()
    }

    override fun onMonthSelected(year: Int, month: Int) {
        var startDistanceMillis: Long = INVALID_L
        if (currentRangePart == START) {
            startDistanceMillis =
                selectedDts[END].distance(selectedDts[START])
        }

        currentSelectedDt.day = invalidateDayOfMonth(
            year,
            month,
            currentSelectedDt.day
        )
        currentSelectedDt.month = month
        currentSelectedDt.year = year

        roundToNearest(currentSelectedDt, DAY)

        updateStartToEndDistance(startDistanceMillis)
        updatePossibleSelection()

        val changePicker: Boolean = advancePicker(MONTH)
        selectionChangeListener.onDtSelectionChanged(changePicker)

        notifyDtsChangeListener()
    }

    override fun onDaySelected(year: Int, month: Int, day: Int) {
        var startDistanceMillis: Long = INVALID_L
        if (currentRangePart == START) {
            startDistanceMillis =
                selectedDts[END].distance(selectedDts[START])
        }

        currentSelectedDt.year = year
        currentSelectedDt.month = month
        currentSelectedDt.day = day

        roundToNearest(currentSelectedDt, HOUR)

        updateStartToEndDistance(startDistanceMillis)
        updatePossibleSelection()

        val changePicker: Boolean = advancePicker(DAY)
        selectionChangeListener.onDtSelectionChanged(changePicker)

        notifyDtsChangeListener()
    }

    override fun onTimeSelected(
        dt: DateTime,
        index: Int,
        autoAdvance: Boolean
    ) {
        var startDistanceMillis: Long = INVALID_L
        if (currentRangePart == START) {
            startDistanceMillis =
                selectedDts[END].distance(selectedDts[START])
        }

        currentSelectedDt.hour24 = dt.hour24
        currentSelectedDt.minute = dt.minute
        currentSelectedDt.second = dt.second

        roundToNearest(currentSelectedDt, index)

        updateStartToEndDistance(startDistanceMillis)
        updatePossibleSelection()

        val updatePicker: Boolean = autoAdvance && advancePicker(index)
        selectionChangeListener.onDtSelectionChanged(false, updatePicker)

        notifyDtsChangeListener()
    }

    private fun advancePicker(index: Int): Boolean {
        if (!allowAutoAdvance || index !in YEAR..MINUTE) {
            return false
        }

        val availability: BooleanArray =
            if (currentRangePart == START) {
                startDtItemsAvailability
            } else {
                endDtItemsAvailability
            }

        if (availability[index + 1]) {
            previousItem = currentItem
            currentItem = index + 1

            onHeaderSelectionChanged()

            return true
        }

        return false
    }

    private fun updatePossibleSelection() {
        if (isRangeAvailable) {
            val selectedStartDtMillis: Long =
                selectedDts[START].timeInMillis

            if (builder.minDt[END] == null) {
                minDt[END] = DateTime(selectedStartDtMillis)
            } else {
                val minEndDtMillis: Long =
                    builder.minDt[END]?.timeInMillis ?: Long.MAX_VALUE

                minDt[END]?.timeInMillis = max(selectedStartDtMillis, minEndDtMillis)
            }
        }
    }

    private fun updateStartToEndDistance(startDistanceMillis: Long): Boolean {
        if (startDistanceMillis != INVALID_L) {
            val newDistanceMillis: Long =
                selectedDts[END].distance(selectedDts[START])

            if (newDistanceMillis < 0) {
                selectedDts[END].timeInMillis =
                    selectedDts[START].timeInMillis + startDistanceMillis

                roundToNearest(selectedDts[END], NOT_SET, END)

                return true
            }
        }

        return false
    }

    private fun onHeaderSelectionChanged() {
        if (isRangeAvailable) {
            headerRangeSelectionListener?.onHeaderSelection(currentItem, currentRangePart)
        } else {
            headerSelectionListener?.onHeaderSelection(currentItem)
        }
    }

//--------------------------------------------------------------------------------------------------
//  DtsChangeListener
//--------------------------------------------------------------------------------------------------

    private fun notifyDtsChangeListener() {
        val startDt: DateTime = selectedDts[START]
        val endDt: DateTime = selectedDts[END]

        if (isRangeAvailable) {
            dtRangeChangeListener?.onRangeDateChanged(
                if (startDtItemsAvailability[YEAR]) startDt.year else INVALID,
                if (startDtItemsAvailability[MONTH]) startDt.month else INVALID,
                if (startDtItemsAvailability[DAY]) startDt.day else INVALID,

                if (startDtItemsAvailability[HOUR]) startDt.hour24 else INVALID,
                if (startDtItemsAvailability[MINUTE]) startDt.minute else INVALID,
                if (startDtItemsAvailability[SECOND]) startDt.second else INVALID,

                if (endDtItemsAvailability[YEAR]) endDt.year else INVALID,
                if (endDtItemsAvailability[MONTH]) endDt.month else INVALID,
                if (endDtItemsAvailability[DAY]) endDt.day else INVALID,

                if (endDtItemsAvailability[HOUR]) endDt.hour24 else INVALID,
                if (endDtItemsAvailability[MINUTE]) endDt.minute else INVALID,
                if (endDtItemsAvailability[SECOND]) endDt.second else INVALID
            )
        } else {
            dtChangeListener?.onDateChanged(
                if (startDtItemsAvailability[YEAR]) startDt.year else INVALID,
                if (startDtItemsAvailability[MONTH]) startDt.month else INVALID,
                if (startDtItemsAvailability[DAY]) startDt.day else INVALID,

                if (startDtItemsAvailability[HOUR]) startDt.hour24 else INVALID,
                if (startDtItemsAvailability[MINUTE]) startDt.minute else INVALID,
                if (startDtItemsAvailability[SECOND]) startDt.second else INVALID
            )
        }
    }

//--------------------------------------------------------------------------------------------------
//  PickerCallback
//--------------------------------------------------------------------------------------------------

    fun getSelectedDate(): Triple<Int, Int, Int> {
        val startDt: DateTime = selectedDts[START]

        return Triple(
            if (startDtItemsAvailability[YEAR]) startDt.year else INVALID,
            if (startDtItemsAvailability[MONTH]) startDt.month else INVALID,
            if (startDtItemsAvailability[DAY]) startDt.day else INVALID
        )
    }

    fun getSelectedTime(): Triple<Int, Int, Int> {
        val startDt: DateTime = selectedDts[START]

        return Triple(
            if (startDtItemsAvailability[HOUR]) startDt.hour24 else INVALID,
            if (startDtItemsAvailability[MINUTE]) startDt.minute else INVALID,
            if (startDtItemsAvailability[SECOND]) startDt.second else INVALID
        )
    }

    fun getSelectedDt(): DateTime =
        selectedDts[START]

    fun getSelectedStartDate(): Triple<Int, Int, Int>? =
        if (isRangeAvailable) getSelectedDate() else null

    fun getSelectedStartTime(): Triple<Int, Int, Int>? =
        if (isRangeAvailable) getSelectedTime() else null

    fun getSelectedStartDt(): DateTime? =
        if (isRangeAvailable) getSelectedDt() else null

    fun getSelectedEndDate(): Triple<Int, Int, Int>? {
        if (!isRangeAvailable) {
            return null
        }

        val endDt: DateTime = selectedDts[END]

        return Triple(
            if (endDtItemsAvailability[YEAR]) endDt.year else INVALID,
            if (endDtItemsAvailability[MONTH]) endDt.month else INVALID,
            if (endDtItemsAvailability[DAY]) endDt.day else INVALID
        )
    }

    fun getSelectedEndTime(): Triple<Int, Int, Int>? {
        if (!isRangeAvailable) {
            return null
        }

        val endDt: DateTime = selectedDts[END]

        return Triple(
            if (endDtItemsAvailability[HOUR]) endDt.hour24 else INVALID,
            if (endDtItemsAvailability[MINUTE]) endDt.minute else INVALID,
            if (endDtItemsAvailability[SECOND]) endDt.second else INVALID
        )
    }

    fun getSelectedEndDt(): DateTime? =
        if (isRangeAvailable) selectedDts[END] else null
}