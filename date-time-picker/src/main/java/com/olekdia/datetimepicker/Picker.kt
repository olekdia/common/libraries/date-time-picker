package com.olekdia.datetimepicker

import android.annotation.SuppressLint
import android.content.Context
import android.os.Parcelable
import android.view.AbsSavedState
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AlphaAnimation
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.ViewAnimator
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.common.NOT_SET
import com.olekdia.datetimepicker.common.PickerIndex.Companion.DAY
import com.olekdia.datetimepicker.common.PickerIndex.Companion.HOUR
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MINUTE
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MONTH
import com.olekdia.datetimepicker.common.PickerIndex.Companion.SECOND
import com.olekdia.datetimepicker.common.PickerIndex.Companion.YEAR
import com.olekdia.datetimepicker.common.RangePart.Companion.END
import com.olekdia.datetimepicker.common.RangePart.Companion.START
import com.olekdia.datetimepicker.day.DayPicker
import com.olekdia.datetimepicker.interfaces.PickerCallback
import com.olekdia.datetimepicker.interfaces.SelectionChangeListener
import com.olekdia.datetimepicker.month.MonthPicker
import com.olekdia.datetimepicker.time.TimePicker
import com.olekdia.datetimepicker.year.YearPicker

@SuppressLint("ViewConstructor")
class Picker constructor(
    context: Context,
    builder: BasePickerBuilder<*>
) : LinearLayout(context),
    SelectionChangeListener,
    PickerCallback {

    private var rootContainer: PickerContainer
    private var headerContainer: FrameLayout
    private var pickerContainer: ViewAnimator

    private var yearPicker: YearPicker? = null
    private var monthPicker: MonthPicker? = null
    private var dayPicker: DayPicker? = null
    private var timePicker: TimePicker? = null

    private val controller: Controller = Controller(
        context,
        builder,
        this,
        builder.headerSelectionListener,
        builder.headerRangeSelectionListener,
        builder.dtChangeListener,
        builder.dtRangeChangeListener
    )
    private val header: Header = Header(context, controller)

    val currentItem: Int
        get() = controller.currentItem
    val currentRangePart: Int
        get() = controller.currentRangePart

//--------------------------------------------------------------------------------------------------
//  Constructors, init
//--------------------------------------------------------------------------------------------------

    init {
        if (id == NO_ID) {
            id = R.id.picker_view
        }

        val view = LayoutInflater.from(context)
            .inflate(R.layout.picker, this, true)

        rootContainer = view.findViewById(R.id.root_container)
        headerContainer = view.findViewById(R.id.header_container)
        pickerContainer = view.findViewById(R.id.picker_container)

        setupHeaderBg()
        setupPickerBg()

        headerContainer.addView(header)

        initPickers()
        setAnimatorAnimation()
        setupCurrentPicker()

        isSaveEnabled = true
    }

    private fun initPickers() {
        if (controller.isYearAvailable) {
            yearPicker = YearPicker(context, controller)
        }

        if (controller.isMonthAvailable) {
            monthPicker = MonthPicker(context, controller)
        }

        if (controller.isDayAvailable) {
            dayPicker = DayPicker(context, controller)
        }

        if (controller.isHourAvailable ||
            controller.isMinuteAvailable ||
            controller.isSecondAvailable
        ) {
            timePicker = TimePicker(context, controller)
        }
    }

    private fun setAnimatorAnimation() {
        val animationIn = AlphaAnimation(0.0F, 1.0F).apply {
            duration = ANIMATION_DURATION
        }
        val animationOut = AlphaAnimation(1.0F, 0.0F).apply {
            duration = ANIMATION_DURATION
        }
        pickerContainer.apply {
            inAnimation = animationIn
            outAnimation = animationOut
        }
    }

//--------------------------------------------------------------------------------------------------
//  Lifecycle
//--------------------------------------------------------------------------------------------------

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        controller.hapticFeedbackController.start()
        header.pulseCurrentHeaderItem(true)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        controller.hapticFeedbackController.stop()
    }

//--------------------------------------------------------------------------------------------------
//  Update view methods
//--------------------------------------------------------------------------------------------------

    private fun setupCurrentPicker() {
        pickerContainer.removeAllViews()

        val viewToAdd: View? =
            when (controller.currentItem) {
                YEAR -> yearPicker

                MONTH -> monthPicker

                DAY -> dayPicker

                HOUR,
                MINUTE,
                SECOND -> timePicker

                else -> null
            }

        pickerContainer.addView(viewToAdd)

        updatePicker(controller.currentItem)
    }

    private fun updatePicker(itemIndex: Int, animate: Boolean = false) {
        when (itemIndex) {
            YEAR -> yearPicker?.refreshAdapter()

            MONTH -> monthPicker?.refreshAdapter()

            DAY -> dayPicker?.refreshAdapter()

            HOUR,
            MINUTE,
            SECOND -> {
                timePicker?.apply {
                    setCurrentItemShowing(itemIndex, animate)
                    initialize(context, controller.currentSelectedDt)
                    invalidate()
                }
            }
        }
    }

    private fun setupHeaderBg() {
        headerContainer.setBackgroundColor(controller.primaryBackgroundColor)
    }

    private fun setupPickerBg() {
        rootContainer.setBackgroundColor(controller.secondaryBackgroundColor)
        timePicker?.setBackgroundColor(controller.secondaryBackgroundColor)
    }

//--------------------------------------------------------------------------------------------------
//  SelectionChangeListener
//--------------------------------------------------------------------------------------------------

    override fun onHeaderSelectionChanged(changePicker: Boolean, updatePicker: Boolean) {
        header.run {
            updateHighlight()
            pulseCurrentHeaderItem()
        }

        if (changePicker) {
            setupCurrentPicker()
        } else if (updatePicker) {
            updatePicker(controller.currentItem)
        }
    }

    override fun onAmPmSelectionChanged(
        updatePicker: Boolean,
        amPm: Int,
        part: Int
    ) {
        header.run {
            updateLabel()
            pulseAmPmItem(part)
        }

        if (updatePicker) {
            updatePicker(controller.currentItem)
        }
    }

    override fun onDtSelectionChanged(changePicker: Boolean, updatePicker: Boolean) {
        header.run {
            updateLabel()
            updateHighlight()
        }

        if (changePicker) {
            setupCurrentPicker()
        } else if (updatePicker) {
            updatePicker(controller.currentItem, true)
        }
    }

//--------------------------------------------------------------------------------------------------
//  PickerCallback
//--------------------------------------------------------------------------------------------------

    override fun getSelectedDate(): Triple<Int, Int, Int> =
        controller.getSelectedDate()

    override fun getSelectedTime(): Triple<Int, Int, Int> =
        controller.getSelectedTime()

    override fun getSelectedDt(): DateTime =
        controller.getSelectedDt()

    override fun getSelectedStartDate(): Triple<Int, Int, Int>? =
        controller.getSelectedStartDate()

    override fun getSelectedStartTime(): Triple<Int, Int, Int>? =
        controller.getSelectedStartTime()

    override fun getSelectedStartDt(): DateTime? =
        controller.getSelectedStartDt()

    override fun getSelectedEndDate(): Triple<Int, Int, Int>? =
        controller.getSelectedEndDate()

    override fun getSelectedEndTime(): Triple<Int, Int, Int>? =
        controller.getSelectedEndTime()

    override fun getSelectedEndDt(): DateTime? =
        controller.getSelectedEndDt()

//--------------------------------------------------------------------------------------------------
//  Save/restore
//--------------------------------------------------------------------------------------------------

    override fun onSaveInstanceState(): Parcelable? {
        val superState: Parcelable = super.onSaveInstanceState() ?: AbsSavedState.EMPTY_STATE

        val st = SavedState(superState)
        st.selectedStartMillis = controller.selectedDts[START].timeInMillis
        st.selectedEndMillis = controller.selectedDts[END].timeInMillis

        st.currentItem = controller.currentItem
        st.currentRagePart = controller.currentRangePart

        return st
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        (state as? SavedState)?.let { st ->
            super.onRestoreInstanceState(st.superState)

            controller.selectedDts[START].timeInMillis = st.selectedStartMillis
            controller.selectedDts[END].timeInMillis = st.selectedEndMillis

            controller.currentItem = st.currentItem
            controller.currentRangePart = st.currentRagePart
        } ?: run {
            super.onRestoreInstanceState(AbsSavedState.EMPTY_STATE)
        }

        header.apply {
            updateLabel()
            updateHighlight()
        }

        setupCurrentPicker()
    }

    class SavedState(superState: Parcelable) : BaseSavedState(superState) {
        internal var selectedStartMillis: Long = 0
        internal var selectedEndMillis: Long = 0

        internal var currentItem: Int = NOT_SET
        internal var currentRagePart = START
    }

//--------------------------------------------------------------------------------------------------
//  Companion
//--------------------------------------------------------------------------------------------------

    companion object {
        const val ANIMATION_DURATION = 300L
    }
}