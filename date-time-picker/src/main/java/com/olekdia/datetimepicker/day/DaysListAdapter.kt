package com.olekdia.datetimepicker.day

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView.LayoutParams
import android.widget.BaseAdapter
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.interfaces.DatePickerController

class DaysListAdapter(
    private val context: Context,
    private val controller: DatePickerController
) : BaseAdapter(),
    DaysView.OnDayClickListener {

    private val currDateTime =
        DateTime(System.currentTimeMillis())

    var selectedDay: DateTime = controller.currentSelectedDt

    var viewHeight: Int = 0

    override fun getCount(): Int {
        val startMonth: Int = controller.startDt.year * MONTHS_IN_YEAR + controller.startDt.month
        val endMonth: Int = controller.endDt.year * MONTHS_IN_YEAR + controller.endDt.month
        return endMonth - startMonth + 1
    }

    override fun getItem(position: Int): Any? = null

    override fun getItemId(position: Int): Long = position.toLong()

    override fun hasStableIds(): Boolean = true

    override fun getView(
        position: Int,
        convertView: View?,
        parent: ViewGroup
    ): View {
        val view: DaysView = convertView as? DaysView ?: DaysView(context, controller)
            .apply {
                layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
            }

        view.onDayClickListener = this
        view.viewHeight = viewHeight

        val startMonth: Int = controller.startDt.month
        val showedYear = (position + startMonth) / MONTHS_IN_YEAR + controller.minYear
        val showedMonth: Int = (position + startMonth) % MONTHS_IN_YEAR

        view.setupDateParams(
            currYear = currDateTime.year,
            currMonth = currDateTime.month,
            currDay = currDateTime.day,
            currDayOfWeek = currDateTime.dayOfWeek,
            firstDayInWeek = controller.firstDayOfWeek,
            selectedYear = selectedDay.year,
            selectedMonth = selectedDay.month,
            selectedDay = selectedDay.day,
            showedYear = showedYear,
            showedMonth = showedMonth
        )

        return view
    }

    override fun onDayClick(day: DateTime) {
        controller.apply {
            tryVibrate()
            onDaySelected(day.year, day.month, day.day)
        }
        selectedDay = day
        notifyDataSetChanged()
    }

    companion object {
        const val MONTHS_IN_YEAR = 12
    }
}
