package com.olekdia.datetimepicker.day

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Paint.Align
import android.graphics.Paint.Style
import android.graphics.Rect
import android.graphics.Typeface
import android.view.MotionEvent
import android.view.View
import com.olekdia.androidcommon.extensions.*
import com.olekdia.common.INVALID
import com.olekdia.common.extensions.toLocalNumerals
import com.olekdia.datetimepicker.R
import com.olekdia.datetimepicker.common.DAYS_IN_WEEK
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.common.PickerIndex.Companion.DAY
import com.olekdia.datetimepicker.interfaces.DatePickerController
import java.util.*
import kotlin.math.ceil
import kotlin.math.min

@SuppressLint("ViewConstructor")
class DaysView(
    context: Context,
    private var controller: DatePickerController
) : View(context) {

    private val isRtl: Boolean = context.resources.isLayoutRtl()

    private val defNormalTypeface: Typeface = Typeface.DEFAULT
    private val defBoldTypeface: Typeface = Typeface.DEFAULT_BOLD

    private val fontMeasureRect: Rect = Rect()
    private var headerTextHeight: Float = 0F
    private var dayTextSize: Float = 0F
    private val fontSizeMultiplier = context.getFloat(R.dimen.dtp_day_month_text_size_multiplier_normal)

    var viewHeight: Int = 0
    private var headerSize: Float = 0F
    private var headerVertPadding: Float = 0F

    private val daySeparatorWidth = 1
    private var daySelectedCircleSize: Float = 0F
    private var daySelectedCircleOffsetY: Float = 0F
    private var edgePadding = 0

    private var rowHeight: Int = 0
    var numRows: Int = 0
        private set
    private var numCells = DAYS_IN_WEEK

    private val monthYearPaint: Paint = Paint()
    private val dayOfWeekPaint: Paint = Paint()
    private val dayPaint: Paint = Paint()
    private val selectedCirclePaint: Paint = Paint()

    private val calendar: Calendar = Calendar.getInstance()

    private var currDayOfWeek: Int = INVALID
    private var currDay: Int = INVALID
    private var currMonth: Int = INVALID
    private var currYear: Int = INVALID
    private var selectedDay: Int = INVALID
    private var selectedMonth: Int = INVALID
    private var selectedYear: Int = INVALID
    private var showedMonth: Int = INVALID
    private var showedYear: Int = INVALID

    private var firstDayInWeek = Calendar.SUNDAY
    private var firstDayOfMonthDayOfWeek = 0
    private var hasCurrDay = false

    private val monthAndYearTitle: String
        get() = controller.monthsOfYearLong[showedMonth]
            .let { monthTitle ->
                "$monthTitle ${showedYear.toLocalNumerals(controller.numeralSystem)}"
            }

    val currDayOfWeekIndex: Int
        get() = when {
            !hasCurrDay -> INVALID
            firstDayInWeek > currDayOfWeek -> DAYS_IN_WEEK - firstDayInWeek + currDayOfWeek
            else -> currDayOfWeek - firstDayInWeek
        }

    var onDayClickListener: OnDayClickListener? = null

    interface OnDayClickListener {
        fun onDayClick(day: DateTime)
    }

    init {
        initView()
    }

    private fun initView() {
        val boldFont = context.getTypeface(context.getString(R.string.dtp_sans_serif), Typeface.BOLD)

        monthYearPaint.apply {
            isFakeBoldText = true
            isAntiAlias = true
            typeface = boldFont
            color = controller.primaryTextColor
            textAlign = Align.CENTER
            style = Style.FILL
        }

        dayOfWeekPaint.apply {
            isAntiAlias = true
            isFakeBoldText = true
            typeface = boldFont
            style = Style.FILL
            textAlign = Align.CENTER
        }

        dayPaint.apply {
            isAntiAlias = true
            style = Style.FILL
            textAlign = Align.CENTER
            isFakeBoldText = false
        }

        selectedCirclePaint.apply {
            isFakeBoldText = true
            isAntiAlias = true
            color = controller.accentColor
            textAlign = Align.CENTER
            style = Style.FILL
            alpha = SELECTED_CIRCLE_ALPHA
        }
    }

    fun setupDateParams(
        currYear: Int,
        currMonth: Int,
        currDay: Int,
        currDayOfWeek: Int,
        firstDayInWeek: Int,
        selectedYear: Int = currYear,
        selectedMonth: Int = currMonth,
        selectedDay: Int = currDay,
        showedYear: Int = selectedYear,
        showedMonth: Int = selectedMonth
    ) {
        this.selectedYear = selectedYear
        this.selectedMonth = selectedMonth
        this.selectedDay = selectedDay

        this.currYear = currYear
        this.currMonth = currMonth
        this.currDay = currDay
        this.currDayOfWeek = currDayOfWeek

        this.showedYear = showedYear
        this.showedMonth = showedMonth

        this.firstDayInWeek = firstDayInWeek

        calendar.set(Calendar.YEAR, showedYear)
        calendar.set(Calendar.MONTH, showedMonth)
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        firstDayOfMonthDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)

        hasCurrDay = showedYear == currYear && showedMonth == currMonth

        numCells = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)

        numRows = calculateNumRows()
    }

    private fun calculateNumRows(): Int {
        val offset: Int = findDayOffset()
        return ceil((offset + numCells).toFloat() / DAYS_IN_WEEK).toInt()
    }

    private fun findDayOffset(): Int {
        val offset: Int = firstDayOfMonthDayOfWeek - firstDayInWeek
        return if (offset >= 0) offset else offset + DAYS_IN_WEEK
    }

    private fun indexDayToShiftedWeekIndexDay(
        indexOfCalDay: Int,
        weekStart: Int
    ): Int {
        return ((indexOfCalDay + weekStart) % DAYS_IN_WEEK + 5) % DAYS_IN_WEEK
    }

//--------------------------------------------------------------------------------------------------
//  Draw methods
//--------------------------------------------------------------------------------------------------

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val h = viewHeight
        headerSize = h * 0.2F // 20% of all view should be occupied by header
        setMeasuredDimension(
            MeasureSpec.getSize(widthMeasureSpec),
            (h - headerSize * 0.59F).toInt() // Half of the header should be visible from next month
        )
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        val w = right - left
        val h = bottom - top
        rowHeight = ((h - headerSize) / numRows).toInt()
        dayTextSize = min(
            min(w, h) * fontSizeMultiplier,
            context.getDimensionPixelSize(R.dimen.dtp_normal_font_size_limit).toFloat()
        )
        val dayOfWeekTextSize = min(
            headerSize * 0.2F,
            context.getDimensionPixelSize(R.dimen.dtp_large_font_size_limit).toFloat()
        )
        val monthAndYearTextSize = dayOfWeekTextSize * 1.1F

        dayPaint.textSize = dayTextSize
        daySelectedCircleSize = dayTextSize * 1.17F
        dayPaint.getTextBounds("0", 0, 1, fontMeasureRect)
        daySelectedCircleOffsetY = fontMeasureRect.height() * 0.5F

        dayOfWeekPaint.textSize = dayOfWeekTextSize
        monthYearPaint.textSize = monthAndYearTextSize

        monthYearPaint.getTextBounds("0", 0, 1, fontMeasureRect)
        headerTextHeight = fontMeasureRect.height().toFloat() * 1.1F

        headerVertPadding = (headerSize - 2 * headerTextHeight) / 3F
    }

    override fun onDraw(canvas: Canvas) {
        drawMonthTitle(canvas)
        drawDayOfWeek(canvas)
        drawDays(canvas)
    }

    private fun drawMonthTitle(canvas: Canvas) {
        val x: Float = (width + 2 * edgePadding) / 2F
        val y: Float = headerTextHeight + headerVertPadding
        canvas.drawText(monthAndYearTitle, x, y, monthYearPaint)
    }

    private fun drawDayOfWeek(canvas: Canvas) {
        var x: Float
        val y: Float = 2 * headerTextHeight + 2 * headerVertPadding

        val dayWidthHalf: Int = (width - edgePadding * 2) / (DAYS_IN_WEEK * 2)

        var dayInWeekIndex: Int
        val currDayOfWeekIndex: Int = this.currDayOfWeekIndex

        for (i in 0 until DAYS_IN_WEEK) {
            x = ((2 * i + 1) * dayWidthHalf + edgePadding).toFloat()
            if (isRtl) {
                x = width - x
            }

            dayInWeekIndex = indexDayToShiftedWeekIndexDay(i, firstDayInWeek)

            val isCurrent: Boolean = i == currDayOfWeekIndex
            dayOfWeekPaint.color = if (isCurrent) {
                controller.primaryTextColor
            } else {
                controller.secondaryTextColor
            }

            canvas.drawText(
                controller.daysOfWeek[dayInWeekIndex],
                x,
                y,
                dayOfWeekPaint
            )
        }
    }

    private fun drawDays(canvas: Canvas) {
        var x: Float
        var y: Float = (rowHeight + dayTextSize) / 2F - daySeparatorWidth + headerSize

        val dayWidthHalf: Int = (width - edgePadding * 2) / (DAYS_IN_WEEK * 2)

        var i: Int = findDayOffset()

        for (dayNumber in 1..numCells) {
            x = (((2 * i + 1) * dayWidthHalf + edgePadding).toFloat())
            if (isRtl) {
                x = width - x
            }

            drawMonthDay(
                canvas,
                showedYear, showedMonth, dayNumber,
                x, y
            )

            i++
            if (i == DAYS_IN_WEEK) {
                i = 0
                y += rowHeight
            }
        }
    }

    private fun drawMonthDay(
        canvas: Canvas,
        year: Int, month: Int, day: Int,
        x: Float, y: Float
    ) {
        val isDisable = controller.isOutOfRange(DAY, year, month, day)
        val isSelected: Boolean =
            selectedYear == year && selectedMonth == month && selectedDay == day
        val isCurrent: Boolean = hasCurrDay && currDay == day

        if (isSelected) {
            canvas.drawCircle(
                x,
                y - daySelectedCircleOffsetY,
                daySelectedCircleSize,
                selectedCirclePaint
            )
        }

        when {
            isDisable -> dayPaint.apply {
                color = controller.disabledTextColor
                typeface = defNormalTypeface
                isFakeBoldText = false
            }
            isSelected -> dayPaint.apply {
                color = controller.highlightedTextColor
                typeface = defBoldTypeface
                isFakeBoldText = true
            }
            isCurrent -> dayPaint.apply {
                color = controller.primaryTextColor
                typeface = defBoldTypeface
                isFakeBoldText = true
            }
            else -> {
                dayPaint.apply {
                    color = controller.secondaryTextColor
                    typeface = defNormalTypeface
                    isFakeBoldText = false
                }
            }
        }

        canvas.drawText(
            day.toLocalNumerals(controller.numeralSystem),
            x,
            y,
            dayPaint
        )
    }

//--------------------------------------------------------------------------------------------------
//  Handler for event
//--------------------------------------------------------------------------------------------------

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_UP -> {
                val day: Int = getDayFromLocation(event.x, event.y)

                if (day >= 0 &&
                    !controller.isOutOfRange(DAY, showedYear, showedMonth, day)
                ) {
                    onDayClickListener?.onDayClick(
                        DateTime(System.currentTimeMillis())
                            .apply {
                                year = showedYear
                                month = showedMonth
                                this.day = day
                            }
                    )
                }
            }
        }

        return true
    }

    private fun getDayFromLocation(x: Float, y: Float): Int {
        if (x < edgePadding || x > width - edgePadding) {
            return INVALID
        }

        val columnWidth: Int = (width - 2 * edgePadding) / DAYS_IN_WEEK

        val row: Int = (y - headerSize).toInt() / rowHeight
        var column: Int = (x - edgePadding).toInt() / columnWidth

        if (isRtl) {
            column = DAYS_IN_WEEK - 1 - column
        }

        val day: Int = row * DAYS_IN_WEEK + column - findDayOffset() + 1

        return if (day < 1 || day > numCells) INVALID else day
    }

    companion object {
        const val SELECTED_CIRCLE_ALPHA = 255
    }
}
