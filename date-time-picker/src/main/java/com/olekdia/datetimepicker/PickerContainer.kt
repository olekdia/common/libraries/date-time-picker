package com.olekdia.datetimepicker

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.olekdia.androidcommon.extensions.getDimensionPixelSize
import kotlin.math.min

class PickerContainer(
    context: Context,
    attrs: AttributeSet
) : LinearLayout(context, attrs) {

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width: Int = MeasureSpec.getSize(widthMeasureSpec)
        val height: Int = min(
            MeasureSpec.getSize(heightMeasureSpec),
            context.getDimensionPixelSize(R.dimen.dtp_max_container_height)
        )

        if (orientation == VERTICAL) {
            val maxHeaderHeight: Int = context.getDimensionPixelSize(R.dimen.dtp_max_header_height)
            val headerHeight: Int = min((height * HEADER_PART_PORT).toInt(), maxHeaderHeight)
            val pickerHeight: Int = height - headerHeight

            getHeader().apply {
                this.measure(
                    MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(headerHeight, MeasureSpec.EXACTLY)
                )
            }
            getPicker().apply {
                this.measure(
                    MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(pickerHeight, MeasureSpec.EXACTLY)
                )
            }
        } else {
            val maxHeaderWidth: Int = context.getDimensionPixelSize(R.dimen.dtp_max_header_width)
            val headerWidth: Int = min((width * HEADER_PART_LAND).toInt(), maxHeaderWidth)
            val pickerWidth: Int = width - headerWidth

            getHeader().apply {
                this.measure(
                    MeasureSpec.makeMeasureSpec(headerWidth, MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
                )
            }
            getPicker().apply {
                this.measure(
                    MeasureSpec.makeMeasureSpec(pickerWidth, MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
                )
            }
        }

        setMeasuredDimension(width, height)
    }

    private fun getHeader(): View = getChildAt(if (orientation == VERTICAL) 0 else 1)
    private fun getPicker(): View = getChildAt(if (orientation == VERTICAL) 1 else 0)

    companion object {
        private const val HEADER_PART_PORT = 0.25F
        private const val HEADER_PART_LAND = 0.33F
    }
}