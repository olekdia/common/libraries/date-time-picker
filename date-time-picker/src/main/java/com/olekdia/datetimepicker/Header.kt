package com.olekdia.datetimepicker

import android.animation.Keyframe
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.annotation.SuppressLint
import android.content.Context
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.AnyRes
import com.olekdia.androidcommon.extensions.getDimensionPixelSize
import com.olekdia.androidcommon.extensions.pxToDp
import com.olekdia.common.extensions.adjustAlpha
import com.olekdia.common.extensions.toLocal2DigNumerals
import com.olekdia.common.extensions.toLocalNumerals
import com.olekdia.datetimepicker.common.MonthLabelMode
import com.olekdia.datetimepicker.common.PickerIndex.Companion.DAY
import com.olekdia.datetimepicker.common.PickerIndex.Companion.HOUR
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MINUTE
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MONTH
import com.olekdia.datetimepicker.common.PickerIndex.Companion.SECOND
import com.olekdia.datetimepicker.common.PickerIndex.Companion.YEAR
import com.olekdia.datetimepicker.common.RangePart.Companion.END
import com.olekdia.datetimepicker.common.RangePart.Companion.START
import com.olekdia.datetimepicker.interfaces.HeaderController
import java.util.*
import kotlin.math.min

@SuppressLint("ViewConstructor")
class Header constructor(
    context: Context,
    private val controller: HeaderController
) : LinearLayout(context),
    View.OnClickListener {

    private var startDtItemsViews: Array<TextView>
    private var startAmPmView: TextView
    private var startSeparatorMinutes: TextView
    private var startSeparatorSeconds: TextView

    private var endDtItemsViews: Array<TextView>? = null
    private var endAmPmView: TextView? = null
    private var endSeparatorMinutes: TextView? = null
    private var endSeparatorSeconds: TextView? = null
    private var monthLabelMode = MonthLabelMode.LONG

//--------------------------------------------------------------------------------------------------
//  Constructors, init
//--------------------------------------------------------------------------------------------------

    init {
        val inflater: LayoutInflater = LayoutInflater.from(context)

        val view: View = inflater.inflate(R.layout.header, this, true)

        startDtItemsViews = arrayOf(
            view.findViewById(R.id.start_year),
            view.findViewById(R.id.start_month),
            view.findViewById(R.id.start_day),
            view.findViewById(R.id.start_hours),
            view.findViewById(R.id.start_minutes),
            view.findViewById(R.id.start_seconds)
        )
        startAmPmView = view.findViewById(R.id.start_am_pm_label)
        startSeparatorMinutes = view.findViewById(R.id.start_separator_minutes)
        startSeparatorSeconds = view.findViewById(R.id.start_separator_seconds)

        if (controller.isRangeAvailable) {
            val endContainer: View =
                inflater.inflate(R.layout.header_end_container, this, false)

            val header: LinearLayout = view.findViewById(R.id.header)
            header.addView(endContainer)

            endDtItemsViews = arrayOf(
                view.findViewById(R.id.end_year),
                view.findViewById(R.id.end_month),
                view.findViewById(R.id.end_day),
                view.findViewById(R.id.end_hours),
                view.findViewById(R.id.end_minutes),
                view.findViewById(R.id.end_seconds)
            )
            endAmPmView = view.findViewById(R.id.end_am_pm_label)
            endSeparatorMinutes = view.findViewById(R.id.end_separator_minutes)
            endSeparatorSeconds = view.findViewById(R.id.end_separator_seconds)
        }

        updateItemsVisibility()
        updateLabel()
        updateHighlight()

        bind()
    }

    private fun bind() {
        startDtItemsViews.let {
            for (item in it) {
                item.setOnClickListener(this)
            }
        }

        endDtItemsViews?.let {
            for (item in it) {
                item.setOnClickListener(this)
            }
        }

        startAmPmView.setOnClickListener(this)
        endAmPmView?.setOnClickListener(this)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val w = MeasureSpec.getSize(widthMeasureSpec)
        val h = MeasureSpec.getSize(heightMeasureSpec)

        if (w > 0 && h > 0) {
            val textSize: Float = min(
                findBestFontSize(w, h),
                context.getDimensionPixelSize(R.dimen.dtp_extra_extra_extra_large_font_size_limit).toFloat()
            )
            val monthLabelMode: Int = findMonthLabelMode(w, h)
            val prevTextSize: Float = startSeparatorSeconds.textSize

            if (textSize != prevTextSize) {
                val amPmTextSize = textSize * 0.65F

                startDtItemsViews.let {
                    for (item in it) {
                        item.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)
                    }
                }
                startAmPmView.setTextSize(TypedValue.COMPLEX_UNIT_PX, amPmTextSize)
                startSeparatorMinutes.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)
                startSeparatorSeconds.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)

                endDtItemsViews?.let {
                    for (item in it) {
                        item.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)
                    }
                }

                endAmPmView?.setTextSize(TypedValue.COMPLEX_UNIT_PX, amPmTextSize)
                endSeparatorMinutes?.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)
                endSeparatorSeconds?.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)
            }

            if (monthLabelMode != this.monthLabelMode) {
                this.monthLabelMode = monthLabelMode
                updateMonth(START)
                updateMonth(END)
            }
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    @MonthLabelMode
    private fun findMonthLabelMode(width: Int, height: Int): Int =
        context.resources.pxToDp(width).let { widthDp ->
            if (width <= height) { // land
                when {
                    widthDp > 280 -> MonthLabelMode.MIDDLE
                    else -> MonthLabelMode.SHORT
                }
            } else { // port
                if (controller.isDateAvailable
                    && controller.isTimeAvailable
                ) {
                    when {
                        widthDp > 340 -> MonthLabelMode.MIDDLE
                        else -> MonthLabelMode.SHORT
                    }
                } else {
                    when {
                        widthDp > 300 -> MonthLabelMode.MIDDLE
                        else -> MonthLabelMode.SHORT
                    }
                }
            }
        }

    private fun findBestFontSize(width: Int, height: Int): Float =
        if (width <= height) { // land
            if (!controller.isDateAvailable
                && controller.is24HourMode
            ) { // time only
                if (controller.isSecondAvailable) {
                    width * 0.2F
                } else {
                    width * 0.23F
                }
            } else {
                width * 0.16F
            }
        } else { // port
            if (controller.isDateAvailable
                && controller.isTimeAvailable
            ) {
                height * 0.2F
            } else if (controller.isDateAvailable) {
                height * 0.235F
            } else { // time only
                if (controller.isRangeAvailable) {
                    height * 0.25F
                } else {
                    if (controller.isSecondAvailable) {
                        height * 0.33F
                    } else {
                        height * 0.35F
                    }
                }
            }
        }

//--------------------------------------------------------------------------------------------------
//  Update view methods
//--------------------------------------------------------------------------------------------------

    private fun updateItemsVisibility() {
        var isItemVisible: Boolean

        startDtItemsViews.let {
            for (i in it.indices) {
                isItemVisible = controller.startDtItemsAvailability[i]
                it[i].visibility = if (isItemVisible) View.VISIBLE else View.GONE
            }
        }

        updateSeparatorDtVisibility(R.id.start_separator_dt, controller.startDtItemsAvailability)

        startSeparatorMinutes.visibility =
            if (controller.startDtItemsAvailability[HOUR]
                && (controller.startDtItemsAvailability[MINUTE]
                    || controller.startDtItemsAvailability[SECOND])
            ) {
                View.VISIBLE
            } else {
                View.GONE
            }

        startSeparatorSeconds.visibility =
            if (controller.startDtItemsAvailability[MINUTE]
                && controller.startDtItemsAvailability[SECOND]
            ) {
                View.VISIBLE
            } else {
                View.GONE
            }

        startAmPmView.visibility =
            if (!controller.is24HourMode
                && controller.startDtItemsAvailability[HOUR]
            ) {
                View.VISIBLE
            } else {
                View.GONE
            }

        if (controller.isRangeAvailable) {
            endDtItemsViews?.let {
                for (i in it.indices) {
                    isItemVisible = controller.endDtItemsAvailability[i]
                    it[i].visibility = if (isItemVisible) View.VISIBLE else View.GONE
                }
            }

            updateSeparatorDtVisibility(R.id.end_separator_dt, controller.endDtItemsAvailability)

            endSeparatorMinutes?.visibility =
                if (controller.endDtItemsAvailability[HOUR]
                    && (controller.endDtItemsAvailability[MINUTE]
                        || controller.endDtItemsAvailability[SECOND])
                ) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
            endSeparatorSeconds?.visibility =
                if (controller.endDtItemsAvailability[MINUTE]
                    && controller.endDtItemsAvailability[SECOND]
                ) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
            endAmPmView?.visibility =
                if (!controller.is24HourMode
                    && controller.endDtItemsAvailability[HOUR]
                ) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
        }
    }

    private fun updateSeparatorDtVisibility(@AnyRes separatorId: Int, dtItemsAvailability: BooleanArray) {
        findViewById<View>(separatorId)?.let { separator ->
            separator.visibility =
                if ((dtItemsAvailability[HOUR]
                        || dtItemsAvailability[MINUTE]
                        || dtItemsAvailability[SECOND]) !=
                    (dtItemsAvailability[YEAR]
                        || dtItemsAvailability[MONTH]
                        || dtItemsAvailability[DAY])
                ) {
                    View.GONE
                } else {
                    View.VISIBLE
                }
        }
    }


    fun updateLabel() {
        updateYear(START)
        updateMonth(START)
        updateDay(START)
        updateHour(START)
        updateMinute(START)
        updateSecond(START)
        updateAmPm(START)


        if (controller.isRangeAvailable) {
            updateYear(END)
            updateMonth(END)
            updateDay(END)
            updateHour(END)
            updateMinute(END)
            updateSecond(END)
            updateAmPm(END)
        }
    }

    private fun updateYear(rangePart: Int) {
        val textView: TextView =
            if (rangePart == START) {
                startDtItemsViews[YEAR]
            } else {
                endDtItemsViews?.get(YEAR)
            } ?: return

        val year: Int = controller.selectedDts[rangePart].year
        val text: String = year.toLocalNumerals(controller.numeralSystem)
        textView.text = text
    }

    private fun updateMonth(rangePart: Int) {
        val textView: TextView =
            if (rangePart == START) {
                startDtItemsViews[MONTH]
            } else {
                endDtItemsViews?.get(MONTH)
            } ?: return

        val month: Int = controller.selectedDts[rangePart].month
        textView.text = when (monthLabelMode) {
            MonthLabelMode.LONG -> controller.monthsOfYearLong[month]
            MonthLabelMode.MIDDLE -> controller.monthsOfYearShort[month]
            else -> (month + 1).toLocal2DigNumerals(controller.numeralSystem)
        }
    }

    private fun updateDay(rangePart: Int) {
        val textView: TextView =
            if (rangePart == START) {
                startDtItemsViews[DAY]
            } else {
                endDtItemsViews?.get(DAY)
            } ?: return

        val day: Int = controller.selectedDts[rangePart].day
        val text: String = day.toLocal2DigNumerals(controller.numeralSystem)
        textView.text = text
    }

    private fun updateHour(rangePart: Int) {
        val textView: TextView =
            if (rangePart == START) {
                startDtItemsViews[HOUR]
            } else {
                endDtItemsViews?.get(HOUR)
            } ?: return

        var hour: Int = controller.selectedDts[rangePart].hour24
        val text: String

        if (controller.is24HourMode) {
            text = hour.toLocal2DigNumerals(controller.numeralSystem)
        } else {
            hour %= 12

            if (hour == 0) {
                hour = 12
            }

            text = hour.toLocal2DigNumerals(controller.numeralSystem)
        }

        textView.text = text
    }

    private fun updateMinute(rangePart: Int) {
        val textView: TextView =
            if (rangePart == START) {
                startDtItemsViews[MINUTE]
            } else {
                endDtItemsViews?.get(MINUTE)
            } ?: return

        var minute = controller.selectedDts[rangePart].minute
        if (minute == 60) {
            minute = 0
        }
        val text: String = minute.toLocal2DigNumerals(controller.numeralSystem)

        textView.text = text
    }

    private fun updateSecond(rangePart: Int) {
        val textView: TextView =
            if (rangePart == START) {
                startDtItemsViews[SECOND]
            } else {
                endDtItemsViews?.get(SECOND)
            } ?: return

        var second = controller.selectedDts[rangePart].second
        if (second == 60) {
            second = 0
        }
        val text: String = second.toLocal2DigNumerals(controller.numeralSystem)

        textView.text = text
    }

    private fun updateAmPm(rangePart: Int) {
        if (!controller.is24HourMode) {
            val textView: TextView =
                if (rangePart == START) startAmPmView else endAmPmView ?: return

            textView.text =
                if (controller.selectedDts[rangePart].amPm == Calendar.AM) {
                    controller.amText
                } else {
                    controller.pmText
                }
        }
    }

    fun updateHighlight() {
        val unselectedColor: Int = controller.secondaryTextColor.adjustAlpha(0.77F)

        startDtItemsViews.let {
            for (item in it) {
                item.setTextColor(unselectedColor)
            }
        }
        endDtItemsViews?.let {
            for (item in it) {
                item.setTextColor(unselectedColor)
            }
        }

        startAmPmView.setTextColor(unselectedColor)
        endAmPmView?.setTextColor(unselectedColor)

        startSeparatorMinutes.setTextColor(unselectedColor)
        startSeparatorSeconds.setTextColor(unselectedColor)

        endSeparatorMinutes?.setTextColor(unselectedColor)
        endSeparatorSeconds?.setTextColor(unselectedColor)

        when (controller.currentRangePart) {
            START ->
                startDtItemsViews[controller.currentItem]
                    .setTextColor(controller.primaryTextColor)
            END ->
                endDtItemsViews
                    ?.get(controller.currentItem)
                    ?.setTextColor(controller.primaryTextColor)
        }
    }

    fun pulseCurrentHeaderItem(delayLabelAnimate: Boolean = false) {
        val labelToAnimate: TextView =
            when (controller.currentRangePart) {
                START ->
                    startDtItemsViews[controller.currentItem]
                else ->
                    endDtItemsViews?.get(controller.currentItem) ?: return
            }

        pulse(labelToAnimate, delayLabelAnimate)
    }

    fun pulseAmPmItem(part: Int) {
        val labelToAnimate: TextView =
            when (part) {
                START ->
                    startAmPmView
                else ->
                    endAmPmView ?: return
            }

        pulse(labelToAnimate)
    }

    private fun pulse(
        labelToAnimate: View,
        delayLabelAnimate: Boolean = false
    ) {
        val pulseAnimator = getPulseAnimator(
            labelToAnimate,
            0.85F,
            1.1F
        )
        if (delayLabelAnimate) {
            pulseAnimator.startDelay = PULSE_ANIMATOR_DELAY
        }
        pulseAnimator.start()
    }

    private fun getPulseAnimator(
        labelToAnimate: View?,
        decreaseRatio: Float,
        increaseRatio: Float
    ): ObjectAnimator {

        val k0 = Keyframe.ofFloat(0F, 1F)
        val k1 = Keyframe.ofFloat(0.275F, decreaseRatio)
        val k2 = Keyframe.ofFloat(0.69F, increaseRatio)
        val k3 = Keyframe.ofFloat(1F, 1F)

        val scaleX = PropertyValuesHolder.ofKeyframe("scaleX", k0, k1, k2, k3)
        val scaleY = PropertyValuesHolder.ofKeyframe("scaleY", k0, k1, k2, k3)

        return ObjectAnimator.ofPropertyValuesHolder(labelToAnimate, scaleX, scaleY)
            .apply {
                duration = PULSE_ANIMATOR_DURATION
            }
    }

//--------------------------------------------------------------------------------------------------
//  Event handlers
//--------------------------------------------------------------------------------------------------

    override fun onClick(v: View?) {
        when (v?.id) {
            startDtItemsViews[YEAR].id ->
                controller.onPickHeaderItem(YEAR, START)

            startDtItemsViews[MONTH].id ->
                controller.onPickHeaderItem(MONTH, START)

            startDtItemsViews[DAY].id ->
                controller.onPickHeaderItem(DAY, START)

            startDtItemsViews[HOUR].id ->
                controller.onPickHeaderItem(HOUR, START)

            startDtItemsViews[MINUTE].id ->
                controller.onPickHeaderItem(MINUTE, START)

            startDtItemsViews[SECOND].id ->
                controller.onPickHeaderItem(SECOND, START)


            endDtItemsViews?.get(YEAR)?.id ->
                controller.onPickHeaderItem(YEAR, END)

            endDtItemsViews?.get(MONTH)?.id ->
                controller.onPickHeaderItem(MONTH, END)

            endDtItemsViews?.get(DAY)?.id ->
                controller.onPickHeaderItem(DAY, END)

            endDtItemsViews?.get(HOUR)?.id ->
                controller.onPickHeaderItem(HOUR, END)

            endDtItemsViews?.get(MINUTE)?.id ->
                controller.onPickHeaderItem(MINUTE, END)

            endDtItemsViews?.get(SECOND)?.id ->
                controller.onPickHeaderItem(SECOND, END)


            startAmPmView.id ->
                controller.onToggleAmPm(START)
            endAmPmView?.id ->
                controller.onToggleAmPm(END)
        }
    }

//--------------------------------------------------------------------------------------------------
//  Companion
//--------------------------------------------------------------------------------------------------

    companion object {
        const val PULSE_ANIMATOR_DELAY = 300L
        const val PULSE_ANIMATOR_DURATION = 544L
    }
}