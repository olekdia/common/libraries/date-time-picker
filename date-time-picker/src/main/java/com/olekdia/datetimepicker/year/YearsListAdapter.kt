package com.olekdia.datetimepicker.year

import android.content.Context
import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.olekdia.common.extensions.toLocalNumerals
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.common.PickerIndex.Companion.YEAR
import com.olekdia.datetimepicker.interfaces.DatePickerController

class YearsListAdapter(
    private val context: Context,
    private val controller: DatePickerController
) : BaseAdapter() {

    private val defNormalTypeface: Typeface = Typeface.DEFAULT
    private val defBoldTypeface: Typeface = Typeface.DEFAULT_BOLD

    private var yearsProvider: IntArray = IntArray(0)

    private val currDateTime =
        DateTime(System.currentTimeMillis())

    fun fillYearsProvider() {
        val fromYear: Int = controller.minYear
        val toYear: Int = controller.maxYear
        yearsProvider = IntArray(toYear - fromYear + 1)
        for (year in fromYear..toYear) {
            yearsProvider[year - fromYear] = year
        }
    }

    override fun getCount(): Int = yearsProvider.size

    override fun getItem(position: Int): Int = yearsProvider[position]

    override fun getItemId(position: Int): Long = yearsProvider[position].toLong()

    override fun getView(
        position: Int,
        convertView: View?,
        parent: ViewGroup
    ): View? {
        val view: YearView =
            convertView as? YearView ?: YearView(context, controller)

        val year: Int = getItem(position)
        val isDisable: Boolean = controller.isOutOfRange(YEAR, year)
        val isSelected: Boolean = controller.currentSelectedDt.year == year
        val isCurrent: Boolean = currDateTime.year == year

        view.apply {
            setColor(isDisable, isCurrent)
            typeface = if (isCurrent || isSelected) defBoldTypeface else defNormalTypeface

            drawCircle = isSelected

            text = year.toLocalNumerals(controller.numeralSystem)
            tag = year
        }

        return view
    }
}