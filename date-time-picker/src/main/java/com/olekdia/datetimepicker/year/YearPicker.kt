package com.olekdia.datetimepicker.year

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.StateListDrawable
import android.os.Build
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.ListView
import android.widget.TextView
import com.olekdia.common.INVALID
import com.olekdia.datetimepicker.R
import com.olekdia.datetimepicker.common.PickerIndex.Companion.YEAR
import com.olekdia.datetimepicker.interfaces.DatePickerController

@SuppressLint("ViewConstructor")
class YearPicker(
    context: Context,
    private val controller: DatePickerController
) : ListView(context),
    OnItemClickListener {

    private val adapter: YearsListAdapter = YearsListAdapter(context, controller)

    private var viewHeight: Int = 0
    private val childSize: Int =
        context.resources.getDimensionPixelOffset(R.dimen.dtp_year_label_height)

    private var selectedView: YearView? = null

    init {
        isFastScrollEnabled = false
        isVerticalScrollBarEnabled = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            isNestedScrollingEnabled = true
        }
        isVerticalFadingEdgeEnabled = true
        setFadingEdgeLength(childSize / 3)

        onItemClickListener = this
        selector = StateListDrawable()
        dividerHeight = 0
    }

    fun refreshAdapter() {
        if (viewHeight > 0) {
            adapter.fillYearsProvider()
            setAdapter(adapter)
            goTo(controller.currentSelectedDt.year)
        }
    }

    private fun goTo(year: Int) {
        val position = year - controller.minYear
        val offset = (viewHeight - childSize) / 2

        postSetSelection(position, offset)
    }

    private fun postSetSelection(position: Int, offset: Int) {
        post {
            setSelectionFromTop(position, offset)
            requestLayout()
        }
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)

        if (viewHeight != height) {
            viewHeight = height

            refreshAdapter()
        }
    }

    private fun getYearFromTextView(view: TextView): Int = view.tag as? Int ?: INVALID

    override fun onItemClick(
        parent: AdapterView<*>,
        view: View,
        position: Int,
        id: Long
    ) {
        (view as? YearView)?.let {
            val year: Int = getYearFromTextView(it)
            if (controller.isOutOfRange(YEAR, year)) {
                return
            }

            controller.tryVibrate()

            if (it !== selectedView) {
                selectedView?.apply {
                    drawCircle = false
                    invalidate()
                }

                it.apply {
                    drawCircle = true
                    invalidate()
                }

                selectedView = it
            }

            controller.onYearSelected(year)
            adapter.notifyDataSetChanged()
        }
    }
}