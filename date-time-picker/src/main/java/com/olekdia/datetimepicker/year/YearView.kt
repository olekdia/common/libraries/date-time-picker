package com.olekdia.datetimepicker.year

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Paint.Align
import android.graphics.Paint.Style
import android.util.TypedValue
import android.view.Gravity
import android.widget.AbsListView.LayoutParams
import android.widget.TextView
import com.olekdia.androidcommon.extensions.getDimensionPixelSize
import com.olekdia.datetimepicker.R
import com.olekdia.datetimepicker.interfaces.DatePickerController
import kotlin.math.min

@SuppressLint("ViewConstructor")
class YearView(
    context: Context,
    private val controller: DatePickerController
) : TextView(context) {

    private val circlePaint = Paint()

    var drawCircle: Boolean = false

    init {
        layoutParams = LayoutParams(
            LayoutParams.MATCH_PARENT,
            context.getDimensionPixelSize(R.dimen.dtp_year_label_height)
        )
        gravity = Gravity.CENTER
        setTextSize(
            TypedValue.COMPLEX_UNIT_PX,
            context.getDimensionPixelSize(R.dimen.dtp_year_label_text_size).toFloat()
        )

        circlePaint.apply {
            isFakeBoldText = true
            isAntiAlias = true
            textAlign = Align.CENTER
            style = Style.FILL
            alpha = SELECTED_CIRCLE_ALPHA
        }
    }

    fun setColor(isDisable: Boolean, isCurrent: Boolean) {
        circlePaint.color = controller.accentColor
        setTextColor(createTextColor(isDisable, isCurrent))
    }

    private fun createTextColor(isDisable: Boolean, isCurrent: Boolean): ColorStateList {
        val states: Array<IntArray> = arrayOf(
            intArrayOf(android.R.attr.state_pressed),
            intArrayOf(android.R.attr.state_selected),
            intArrayOf()
        )

        val pressedColor: Int =
            if (isDisable) {
                controller.disabledTextColor
            } else {
                if (drawCircle) controller.highlightedTextColor else controller.primaryTextColor
            }
        val selectedColor: Int = controller.highlightedTextColor
        val defaultColor: Int =
            when {
                isDisable -> controller.disabledTextColor
                isCurrent -> controller.primaryTextColor
                else -> controller.secondaryTextColor
            }

        val colors: IntArray = intArrayOf(
            pressedColor,
            selectedColor,
            defaultColor
        )
        return ColorStateList(states, colors)
    }

    public override fun onDraw(canvas: Canvas) {
        if (drawCircle) {
            val radius: Float = min(width, height) / 2F
            canvas.drawCircle(
                width / 2F, height / 2F,
                radius,
                circlePaint
            )
        }
        isSelected = drawCircle
        super.onDraw(canvas)
    }

    companion object {
        private const val SELECTED_CIRCLE_ALPHA = 255
    }
}
