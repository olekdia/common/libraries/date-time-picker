@file:JvmName("Constants")
package com.olekdia.datetimepicker.common

import androidx.annotation.IntDef

const val NOT_SET = -1

const val DAYS_IN_WEEK = 7
const val MONTHS_IN_YEAR = 12

@IntDef(
    NOT_SET,
    PickerIndex.YEAR,
    PickerIndex.MONTH,
    PickerIndex.DAY,
    PickerIndex.HOUR,
    PickerIndex.MINUTE,
    PickerIndex.SECOND
)
@Retention(AnnotationRetention.SOURCE)
annotation class PickerIndex {
    companion object {
        const val YEAR = 0
        const val MONTH = 1
        const val DAY = 2
        const val HOUR = 3
        const val MINUTE = 4
        const val SECOND = 5
    }
}

@IntDef(RangePart.START, RangePart.END)
@Retention(AnnotationRetention.SOURCE)
annotation class RangePart {
    companion object {
        const val START = 0
        const val END = 1
    }
}

@IntDef(
    MonthLabelMode.SHORT,
    MonthLabelMode.MIDDLE,
    MonthLabelMode.LONG
)
@Retention(AnnotationRetention.SOURCE)
annotation class MonthLabelMode {
    companion object {
        const val SHORT = 0
        const val MIDDLE = 1
        const val LONG = 2
    }
}
