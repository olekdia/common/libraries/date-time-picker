@file:JvmName("DtExt")
package com.olekdia.datetimepicker.common

import com.olekdia.common.INVALID
import com.olekdia.datetimepicker.common.PickerIndex.Companion.DAY
import com.olekdia.datetimepicker.common.PickerIndex.Companion.HOUR
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MINUTE
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MONTH
import com.olekdia.datetimepicker.common.PickerIndex.Companion.SECOND
import com.olekdia.datetimepicker.common.PickerIndex.Companion.YEAR
import java.util.*
import kotlin.math.abs

//tested
fun DateTime.isOutOfRange(
    index: Int,
    selectable: Array<DateTime>?,
    min: DateTime?, max: DateTime?
): Boolean {
    selectable?.let {
        return !this.isSelectable(it, index)
    }

    return this.isBeforeMin(min, index) || this.isAfterMax(max, index)
}

fun isOutOfRange(
    year: Int, month: Int, day: Int,
    index: Int,
    selectable: Array<DateTime>?,
    min: DateTime?, max: DateTime?
): Boolean {
    val calendar: Calendar = Calendar.getInstance()
    val newDateTime = DateTime(calendar.timeInMillis)
    newDateTime.apply {
        this.year = year
        this.month = month
        this.day = day
    }

    return newDateTime.isOutOfRange(index, selectable, min, max)
}

//tested
fun DateTime.isSelectable(
    selectable: Array<DateTime>,
    indexToCheck: Int
): Boolean {
    val roundedDateTime = this.createRoundedCopy(indexToCheck)
    var roundedTempDateTime: DateTime

    for (dt in selectable) {
        roundedTempDateTime = dt.createRoundedCopy(indexToCheck)
        if (roundedTempDateTime == roundedDateTime) {
            return true
        }
    }

    return false
}

//tested
fun DateTime.isBeforeMin(
    min: DateTime?,
    indexToCheck: Int
): Boolean {
    min?.let {
        val roundedDateTime = this.createRoundedCopy(indexToCheck)
        val roundedMin = it.createRoundedCopy(indexToCheck)
        if (roundedMin > roundedDateTime) {
            return true
        }
    }

    return false
}

//tested
fun DateTime.isAfterMax(
    max: DateTime?,
    indexToCheck: Int
): Boolean {
    max?.let {
        val roundedDateTime = this.createRoundedCopy(indexToCheck)
        val roundedMax = it.createRoundedCopy(indexToCheck)
        if (roundedMax < roundedDateTime) {
            return true
        }
    }

    return false
}

//tested
fun DateTime.createRoundedCopy(
    indexToRound: Int
): DateTime {
    val copy = DateTime(this.timeInMillis)

    copy.apply {
        when (indexToRound) {
            YEAR -> {
                month = Calendar.JANUARY
                day = 1
                hour24 = 0
                minute = 0
                second = 0
            }

            MONTH -> {
                day = 1
                hour24 = 0
                minute = 0
                second = 0
            }

            DAY -> {
                hour24 = 0
                minute = 0
                second = 0
            }

            HOUR -> {
                minute = 0
                second = 0
            }

            MINUTE -> {
                second = 0
            }
        }
    }
    return copy
}

//tested
fun DateTime.roundToNearest(
    index: Int,
    selectable: Array<DateTime>?,
    min: DateTime?, max: DateTime?,
    checkPartOfDay: Boolean = false
) {
    selectable?.let {
        var currentDistance: Long = Long.MAX_VALUE
        var rounderToNearestMillis: Long = this.timeInMillis

        for (t in it) {
            if (index == MONTH && !t.isSameYear(this)) continue
            if (index == DAY && !t.isSameMonth(this)) continue
            if (index == HOUR && !t.isSameDay(this)) continue
            if (index == HOUR && checkPartOfDay && !t.isSamePartOfDay(this)) continue
            if (index == MINUTE && !t.isSameHour(this)) continue
            if (index == SECOND && !t.isSameMinute(this)) continue

            val newDistance: Long = abs(t.distance(this))
            if (newDistance < currentDistance) {
                currentDistance = newDistance
                rounderToNearestMillis = t.timeInMillis
            }
        }

        this.timeInMillis = rounderToNearestMillis
        return
    }

    min?.let {
        val skip =
            (index == MONTH && !it.isSameYear(this)) ||
                    (index == DAY && !it.isSameMonth(this)) ||
                    (index == HOUR && !it.isSameDay(this)) ||
                    (index == HOUR && checkPartOfDay && !it.isSamePartOfDay(this)) ||
                    (index == MINUTE && !it.isSameHour(this)) ||
                    (index == SECOND && !it.isSameMinute(this))

        if (!skip && it > this) {
            this.timeInMillis = it.timeInMillis
        }
    }

    max?.let {
        val skip =
            (index == MONTH && !it.isSameYear(this)) ||
                    (index == DAY && !it.isSameMonth(this)) ||
                    (index == HOUR && !it.isSameDay(this)) ||
                    (index == HOUR && checkPartOfDay && !it.isSamePartOfDay(this)) ||
                    (index == MINUTE && !it.isSameHour(this)) ||
                    (index == SECOND && !it.isSameMinute(this))

        if (!skip && it < this) {
            this.timeInMillis = it.timeInMillis
        }
    }
}

//tested
fun DateTime.isAmPmDisable(
    valueToCheck: Int,
    selectable: Array<DateTime>?,
    min: DateTime?, max: DateTime?
): Boolean {
    selectable?.let {
        for (t in it) {
            if (t.isSameDay(this) &&
                t.amPm == valueToCheck
            ) {
                return false
            }
        }
        return true
    }

    min?.let {
        if (it.isSameDay(this) &&
            valueToCheck == Calendar.AM &&
            it.amPm == Calendar.PM
        ) {
            return true
        }
    }

    max?.let {
        if (it.isSameDay(this) &&
            valueToCheck == Calendar.PM &&
            it.amPm == Calendar.AM
        ) {
            return true
        }
    }

    return false
}

//tested
fun findStartDate(
    selectable: Array<DateTime>?,
    min: DateTime?,
    minYear: Int
): DateTime {
    selectable?.let {
        return it[0]
    }

    min?.let {
        return it
    }

    return DateTime(System.currentTimeMillis())
        .apply {
            year = minYear
            month = Calendar.JANUARY
            day = 1
        }
}

//tested
fun findEndDate(
    selectable: Array<DateTime>?,
    max: DateTime?,
    maxYear: Int
): DateTime {
    selectable?.let {
        return it[it.size - 1]
    }

    max?.let {
        return it
    }

    return DateTime(System.currentTimeMillis())
        .apply {
            year = maxYear
            month = Calendar.DECEMBER
            day = 31
        }
}

//tested
fun findMinYear(
    selectable: Array<DateTime>?,
    min: DateTime?,
    minValue: Int
): Int {
    selectable?.let {
        return it[0].year
    }

    min?.let {
        if (it.year > minValue) {
            return it.year
        }
    }

    return INVALID
}

//tested
fun findMaxYear(
    selectable: Array<DateTime>?,
    max: DateTime?,
    maxValue: Int
): Int {
    selectable?.let {
        return it[it.size - 1].year
    }

    max?.let {
        if (it.year < maxValue) {
            return it.year
        }
    }

    return INVALID
}

fun invalidateDayOfMonth(
    year: Int,
    month: Int,
    day: Int
): Int {
    val checkCalendar: Calendar = Calendar.getInstance()
    checkCalendar.apply {
        set(Calendar.DAY_OF_MONTH, 1)
        set(Calendar.MONTH, month)
        set(Calendar.YEAR, year)
    }

    val daysInMonth: Int = checkCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)

    return if (day > daysInMonth) daysInMonth else day
}



