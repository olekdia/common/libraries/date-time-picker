package com.olekdia.datetimepicker.common

import com.olekdia.common.INVALID
import com.olekdia.datetimepicker.common.PickerIndex.Companion.DAY
import com.olekdia.datetimepicker.common.PickerIndex.Companion.HOUR
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MINUTE
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MONTH
import com.olekdia.datetimepicker.common.PickerIndex.Companion.SECOND
import com.olekdia.datetimepicker.common.PickerIndex.Companion.YEAR
import java.util.*

class DateTime(
    private val initMillis: Long
) : Comparable<DateTime> {

    private val calendar: Calendar = GregorianCalendar(1970, Calendar.JANUARY, 1, 0, 0, 0)

    init {
        calendar.apply {
            if (initMillis != 0L) {
                timeInMillis = initMillis
            }

            set(Calendar.MILLISECOND, 0)
        }
    }

    var timeInMillis: Long
        get() = calendar.timeInMillis
        set(value) {
            calendar.timeInMillis = value
        }

    var year: Int
        get() = calendar.get(Calendar.YEAR)
        set(value) {
            calendar.set(Calendar.YEAR, value)
        }

    var month: Int
        get() = calendar.get(Calendar.MONTH)
        set(value) {
            calendar.set(Calendar.MONTH, value)
        }

    var day: Int
        get() = calendar.get(Calendar.DAY_OF_MONTH)
        set(value) {
            calendar.set(Calendar.DAY_OF_MONTH, value)
        }

    var dayOfWeek: Int
        get() = calendar.get(Calendar.DAY_OF_WEEK)
        set(value) {
            calendar.set(Calendar.DAY_OF_WEEK, value)
        }

    var hour24: Int
        get() = calendar.get(Calendar.HOUR_OF_DAY)
        set(value) {
            calendar.set(Calendar.HOUR_OF_DAY, value)
        }

    var hour12: Int
        get() = calendar.get(Calendar.HOUR)
        set(value) {
            calendar.set(Calendar.HOUR, value)
        }

    var minute: Int
        get() = calendar.get(Calendar.MINUTE)
        set(value) {
            calendar.set(Calendar.MINUTE, value)
        }

    var second: Int
        get() = calendar.get(Calendar.SECOND)
        set(value) {
            calendar.set(Calendar.SECOND, value)
        }

    var millis: Int
        get() = calendar.get(Calendar.MILLISECOND)
        set(value) {
            calendar.set(Calendar.MILLISECOND, value)
        }

    var amPm: Int
        get() = calendar.get(Calendar.AM_PM)
        set(value) {
            calendar.set(Calendar.AM_PM, value)
        }

    fun get(index: Int): Int {
        return when (index) {
            YEAR -> year
            MONTH -> month
            DAY -> day

            HOUR -> hour24
            MINUTE -> minute
            SECOND -> second

            else -> INVALID
        }
    }

    fun set(index: Int, value: Int) {
        when (index) {
            YEAR -> year = value
            MONTH -> month = value
            DAY -> day = value

            HOUR -> hour24 = value
            MINUTE -> minute = value
            SECOND -> second = value
        }
    }

    fun isSameYear(other: DateTime) =
        year == other.year

    fun isSameMonth(other: DateTime) =
        isSameYear(other) && month == other.month

    fun isSameDay(other: DateTime) =
        isSameMonth(other) && day == other.day

    fun isSamePartOfDay(other: DateTime) =
        isSameDay(other) && amPm == other.amPm

    fun isSameHour(other: DateTime) =
        isSamePartOfDay(other) && hour24 == other.hour24

    fun isSameMinute(other: DateTime) =
        isSameHour(other) && minute == other.minute

    fun distance(other: DateTime): Long =
        timeInMillis - other.timeInMillis

    override fun compareTo(other: DateTime): Int {
        val distance: Long = distance(other)
        return if (distance > 0) 1 else if (distance < 0) -1 else 0
    }

    override fun equals(other: Any?): Boolean =
        timeInMillis == (other as? DateTime)?.timeInMillis

    override fun hashCode(): Int {
        var result = initMillis.hashCode()
        result = 31 * result + calendar.hashCode()
        return result
    }
}