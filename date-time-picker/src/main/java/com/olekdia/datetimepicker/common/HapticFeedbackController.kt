package com.olekdia.datetimepicker.common

import android.Manifest
import android.app.Service
import android.content.Context
import android.content.pm.PackageManager
import android.database.ContentObserver
import android.net.Uri
import android.os.SystemClock
import android.os.Vibrator
import android.provider.Settings

class HapticFeedbackController(
    private val context: Context
) {

    private val contentObserver: ContentObserver
    private var vibrator: Vibrator? = null
    private var isGloballyEnabled = false
    private var lastVibrate: Long = 0

    init {
        contentObserver = object : ContentObserver(null) {
            override fun onChange(selfChange: Boolean) {
                isGloballyEnabled = checkGlobalSetting(context)
            }
        }
    }

    fun start() {
        if (hasVibratePermission(context)) {
            vibrator = context.getSystemService(Service.VIBRATOR_SERVICE) as? Vibrator
        }

        isGloballyEnabled = checkGlobalSetting(context)

        val uri: Uri = Settings.System.getUriFor(Settings.System.HAPTIC_FEEDBACK_ENABLED)
        context.contentResolver.registerContentObserver(
            uri,
            false,
            contentObserver
        )
    }

    fun stop() {
        vibrator = null
        context.contentResolver.unregisterContentObserver(contentObserver)
    }

    private fun checkGlobalSetting(context: Context): Boolean =
        Settings.System.getInt(
            context.contentResolver,
            Settings.System.HAPTIC_FEEDBACK_ENABLED,
            0
        ) == 1

    private fun hasVibratePermission(context: Context): Boolean {
        val pm: PackageManager = context.packageManager
        val hasPerm: Int = pm.checkPermission(Manifest.permission.VIBRATE, context.packageName)
        return hasPerm == PackageManager.PERMISSION_GRANTED
    }

    fun tryVibrate() {
        if (vibrator != null && isGloballyEnabled) {
            val now: Long = SystemClock.uptimeMillis()
            if (now - lastVibrate >= VIBRATE_DELAY_MS) {
                vibrator?.vibrate(VIBRATE_LENGTH_MS)
                lastVibrate = now
            }
        }
    }

    companion object {
        private const val VIBRATE_DELAY_MS = 125
        private const val VIBRATE_LENGTH_MS = 50L
    }
}