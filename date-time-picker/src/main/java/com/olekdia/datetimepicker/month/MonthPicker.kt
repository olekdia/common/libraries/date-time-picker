package com.olekdia.datetimepicker.month

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.ViewConfiguration
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.AbsListView.OnScrollListener
import android.widget.ListView
import com.olekdia.androidcommon.extensions.dpToPx
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.interfaces.DatePickerController

@SuppressLint("ViewConstructor")
class MonthPicker(
    context: Context,
    private val controller: DatePickerController
) : ListView(context),
    OnScrollListener {

    private var scrollFriction = 1.0F

    private var scrollHandler: Handler = Handler(Looper.getMainLooper())
    private var scrollStateChangedRunnable: ScrollStateRunnable = ScrollStateRunnable()

    private var adapter: MonthsListAdapter = MonthsListAdapter(context, controller)

    private var previousScrollState = OnScrollListener.SCROLL_STATE_IDLE
    private var currentScrollState = OnScrollListener.SCROLL_STATE_IDLE

    var viewHeight: Int = 0

    init {
        layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        isDrawSelectorOnTop = false
        cacheColorHint = 0
        divider = null
        itemsCanFocus = true
        isFastScrollEnabled = false
        isVerticalScrollBarEnabled = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            isNestedScrollingEnabled = true
        }

        setOnScrollListener(this)
        isVerticalFadingEdgeEnabled = true
        setFadingEdgeLength(context.resources.dpToPx(6))
        setFriction(ViewConfiguration.getScrollFriction() * scrollFriction)
    }

    fun refreshAdapter() {
        /**
         * This is important do not populate items before ListView layout,
         * because it will create insane amount of views
         */
        if (viewHeight > 0) {
            adapter.selectedMonth = controller.currentSelectedDt
            setAdapter(adapter)
            goTo(
                controller.currentSelectedDt,
                animate = false,
                forceScroll = true
            )
        }
    }

    private fun goTo(
        month: DateTime,
        animate: Boolean,
        forceScroll: Boolean
    ): Boolean {
        val position = month.year - controller.minYear

        var child: View?
        var i = 0
        var top = 0
        do {
            child = getChildAt(i++)
            child?.let {
                top = it.top
            }
        } while (top < 0)

        val selectedPosition: Int =
            if (child != null) getPositionForView(child) else 0

        if (position != selectedPosition || forceScroll) {
            previousScrollState = OnScrollListener.SCROLL_STATE_FLING

            if (animate) {
                smoothScrollToPositionFromTop(
                    position, LIST_TOP_OFFSET, GOTO_SCROLL_DURATION
                )
                return true
            } else {
                postSetSelection(position)
            }

        }
        return false
    }

    private fun postSetSelection(position: Int) {
        clearFocus()
        post { setSelection(position) }
        onScrollStateChanged(this, OnScrollListener.SCROLL_STATE_IDLE)
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)

        if (viewHeight != height) {
            viewHeight = height

            adapter.viewHeight = viewHeight
            refreshAdapter()
        }
    }

    override fun onScroll(
        view: AbsListView,
        firstVisibleItem: Int,
        visibleItemCount: Int,
        totalItemCount: Int
    ) {
        previousScrollState = currentScrollState
    }

    override fun onScrollStateChanged(view: AbsListView, scrollState: Int) {
        scrollStateChangedRunnable.doScrollStateChange(scrollState)
    }

    inner class ScrollStateRunnable : Runnable {
        private var newState: Int = 0

        fun doScrollStateChange(scrollState: Int) {
            scrollHandler.removeCallbacks(this)
            newState = scrollState
            scrollHandler.postDelayed(this, SCROLL_CHANGE_DELAY)
        }

        override fun run() {
            currentScrollState = newState

            if (newState == OnScrollListener.SCROLL_STATE_IDLE
                && previousScrollState != OnScrollListener.SCROLL_STATE_IDLE
                && previousScrollState != OnScrollListener.SCROLL_STATE_TOUCH_SCROLL
            ) {
                previousScrollState = newState
                var i = 0
                var child: View? = getChildAt(i)
                while (child != null && child.bottom <= 0) {
                    child = getChildAt(++i)
                }

                if (child == null) {
                    return
                }

                val firstPosition: Int = firstVisiblePosition
                val lastPosition: Int = lastVisiblePosition

                val scroll: Boolean = firstPosition != 0 && lastPosition != count - 1
                val top: Int = child.top
                val bottom: Int = child.bottom
                val midpoint: Int = height / 2

                if (scroll && top < LIST_TOP_OFFSET) {
                    if (bottom > midpoint) {
                        smoothScrollBy(top, GOTO_SCROLL_DURATION)
                    } else {
                        smoothScrollBy(bottom, GOTO_SCROLL_DURATION)
                    }
                }
            } else {
                previousScrollState = newState
            }
        }
    }

    companion object {
        const val GOTO_SCROLL_DURATION = 250
        const val SCROLL_CHANGE_DELAY = 40L
        var LIST_TOP_OFFSET = -1
    }
}
