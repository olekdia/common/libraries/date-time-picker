package com.olekdia.datetimepicker.month

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.Paint.Align
import android.graphics.Paint.Style
import android.view.MotionEvent
import android.view.View
import com.olekdia.androidcommon.extensions.*
import com.olekdia.common.INVALID
import com.olekdia.common.extensions.toLocal2DigNumerals
import com.olekdia.common.extensions.toLocalNumerals
import com.olekdia.datetimepicker.R
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.common.MONTHS_IN_YEAR
import com.olekdia.datetimepicker.common.MonthLabelMode
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MONTH
import com.olekdia.datetimepicker.interfaces.DatePickerController
import kotlin.math.floor
import kotlin.math.min

@SuppressLint("ViewConstructor")
open class MonthsView constructor(
    context: Context,
    private val controller: DatePickerController
) : View(context) {

    private val isRtl: Boolean = context.resources.isLayoutRtl()

    private val yearTitleTypeface: String = context.getString(R.string.dtp_sans_serif)
    private val defNormalTypeface: Typeface = Typeface.DEFAULT
    private val defBoldTypeface: Typeface = Typeface.DEFAULT_BOLD
    private val fontSizeMultiplier = context.getFloat(R.dimen.dtp_day_month_text_size_multiplier_normal)

    var viewHeight: Int = 0
    private var headerSize: Float = 0F
    private var headerTopPadding: Float = 0F

    private val fontMeasureRect: Rect = Rect()
    private val selectedRect: RectF = RectF()
    private var selectedRectHeight: Float = 0F

    private var rowHeight: Int = 0

    private var monthLabelMode: Int = MonthLabelMode.LONG

    private val yearPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        isFakeBoldText = true
        typeface = context.getTypeface(yearTitleTypeface, Typeface.BOLD)
        color = controller.primaryTextColor
        textAlign = Align.CENTER
        style = Style.FILL
    }
    private val monthPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Style.FILL
        isFakeBoldText = true
    }
    private val selectedRectPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = controller.accentColor
        style = Style.FILL
    }

    private var currMonth: Int = INVALID
    private var currYear: Int = INVALID
    private var selectedMonth: Int = INVALID
    private var selectedYear: Int = INVALID
    private var showedYear: Int = INVALID

    var onMonthClickListener: OnMonthClickListener? = null

    interface OnMonthClickListener {
        fun onMonthClick(month: DateTime)
    }

    fun setupDateParams(
        selectedYear: Int,
        selectedMonth: Int,
        currYear: Int,
        currMonth: Int,
        showedYear: Int
    ) {
        this.selectedYear = selectedYear
        this.selectedMonth = selectedMonth
        this.currYear = currYear
        this.currMonth = currMonth
        this.showedYear = showedYear
    }

//--------------------------------------------------------------------------------------------------
//  Draw methods
//--------------------------------------------------------------------------------------------------

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val h = viewHeight
        headerSize = h * 0.15F // 15% of all view should be occupied by header
        setMeasuredDimension(
            MeasureSpec.getSize(widthMeasureSpec),
            (h - headerSize).toInt()
        )
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        val w = right - left
        val h = bottom - top

        rowHeight = ((h - headerSize) / NUM_ROWS).toInt()

        val monthTextSize = min(
            min(w, h) * fontSizeMultiplier,
            context.getDimensionPixelSize(R.dimen.dtp_normal_font_size_limit).toFloat()
        )
        val yearTextSize = monthTextSize * 1.1F

        monthPaint.textSize = monthTextSize
        yearPaint.textSize = yearTextSize

        monthPaint.getTextBounds("0", 0, 1, fontMeasureRect)
        selectedRectHeight = fontMeasureRect.height().toFloat()

        yearPaint.getTextBounds("0", 0, 1, fontMeasureRect)
        val yearTextHeight = fontMeasureRect.height().toFloat()

        headerTopPadding = (headerSize - yearTextHeight) * 0.5F + yearTextHeight

        val columnWidth: Int = w / NUM_COLUMNS
        monthLabelMode = findMonthLabelMode(columnWidth)
    }

    private fun findMonthLabelMode(columnWidth: Int): Int {
        monthPaint.apply {
            typeface = defBoldTypeface
            isFakeBoldText = true
        }

        var maxLabelWidth: Float = maxLabelWidth(controller.monthsOfYearLong)
        var widthPercent: Float = maxLabelWidth / columnWidth

        if (widthPercent < 0.9) {
            return MonthLabelMode.LONG
        }

        maxLabelWidth = maxLabelWidth(controller.monthsOfYearShort)
        widthPercent = maxLabelWidth / columnWidth

        return if (widthPercent < 0.9) MonthLabelMode.MIDDLE else MonthLabelMode.SHORT
    }

    private fun maxLabelWidth(monthsTitles: Array<String>): Float {
        var monthTitle: String
        var monthNumber: String
        var monthLabel: String

        var labelWidth: Float
        var maxLabelWidth = 0F

        for (i in 0 until MONTHS_IN_YEAR) {
            monthTitle = monthsTitles[i]
            monthNumber = (i + 1).toLocal2DigNumerals(controller.numeralSystem)
            monthLabel = "$monthNumber $monthTitle"

            labelWidth = monthPaint.measureText(monthLabel, 0, monthLabel.length)

            if (labelWidth > maxLabelWidth) {
                maxLabelWidth = labelWidth
            }
        }

        val padding: Float = selectedRectHeight

        return maxLabelWidth + padding * 2
    }

    override fun onDraw(canvas: Canvas) {
        drawYearTitle(canvas)
        drawYearMonths(canvas)
    }

    private fun drawYearTitle(canvas: Canvas) {
        canvas.drawText(
            showedYear.toLocalNumerals(controller.numeralSystem),
            width / 2F,
            headerTopPadding,
            yearPaint
        )
    }

    private fun drawYearMonths(canvas: Canvas) {
        val columnWidth: Int = width / NUM_COLUMNS

        val firstColumnX: Float = if (isRtl) {
            when (monthLabelMode) {
                MonthLabelMode.SHORT -> width - 0.5F * columnWidth
                MonthLabelMode.MIDDLE -> width - 0.26F * columnWidth
                else -> width - 0.2F * columnWidth
            }
        } else {
            when (monthLabelMode) {
                MonthLabelMode.SHORT -> 0.5F * columnWidth
                MonthLabelMode.MIDDLE -> 0.26F * columnWidth
                else -> 0.2F * columnWidth
            }
        }

        if (monthLabelMode == MonthLabelMode.SHORT) {
            monthPaint.textAlign = Align.CENTER
        } else {
            monthPaint.textAlign = if (isRtl) Align.RIGHT else Align.LEFT
        }

        val nextColumnDelta: Int = if (isRtl) -columnWidth else columnWidth

        var x: Float = firstColumnX
        var y: Float = headerSize + rowHeight / 2F

        for (i in 0 until NUM_ROWS) {
            for (j in 0 until NUM_COLUMNS) {
                drawYearMonth(
                    canvas,
                    i * NUM_COLUMNS + j,
                    x, y,
                    monthLabelMode
                )

                x += nextColumnDelta
            }

            x = firstColumnX
            y += rowHeight
        }
    }

    private fun drawYearMonth(
        canvas: Canvas,
        month: Int,
        x: Float, y: Float,
        mode: Int
    ) {
        val isDisable = controller.isOutOfRange(MONTH, showedYear, month)
        val isSelected: Boolean = selectedYear == showedYear && selectedMonth == month
        val isCurrent: Boolean = currYear == showedYear && currMonth == month

        val monthNumber: String = (month + 1).toLocal2DigNumerals(controller.numeralSystem)
        val monthTitle: String
        val monthLabel: String

        when (mode) {
            MonthLabelMode.SHORT -> {
                monthTitle = ""
                monthLabel = monthNumber
            }

            MonthLabelMode.MIDDLE -> {
                monthTitle = controller.monthsOfYearShort[month]
                monthLabel = "$monthNumber $monthTitle"
            }

            else -> {
                monthTitle = controller.monthsOfYearLong[month]
                monthLabel = "$monthNumber $monthTitle"
            }
        }

        if (isSelected) {
            val padding: Float = selectedRectHeight

            if (mode == MonthLabelMode.SHORT) {
                monthPaint.getTextBounds(monthLabel, 0, monthLabel.length, fontMeasureRect)
                val labelHalfWidth: Float = fontMeasureRect.width() * 0.5f
                val labelHalfHeight: Float = fontMeasureRect.height() * 0.5f
                canvas.drawCircle(x, y - labelHalfHeight, labelHalfWidth + padding, selectedRectPaint)
            } else {
                val labelWidth: Float = monthPaint.measureText(monthLabel, 0, monthLabel.length)

                selectedRect.set(
                    x - padding - if (isRtl) labelWidth else 0F,
                    y - selectedRectHeight - padding,
                    x + padding + if (isRtl) 0F else labelWidth,
                    y + padding
                )

                val roundRadius: Float = selectedRectHeight
                canvas.drawRoundRect(selectedRect, roundRadius, roundRadius, selectedRectPaint)
            }
        }

        var isOrdinary = false
        when {
            isDisable -> {
                monthPaint.apply {
                    color = controller.disabledTextColor
                    typeface = defNormalTypeface
                    isFakeBoldText = false
                }
            }
            isSelected -> {
                monthPaint.apply {
                    color = controller.highlightedTextColor
                    typeface = defBoldTypeface
                    isFakeBoldText = true
                }
            }
            isCurrent -> {
                monthPaint.apply {
                    color = controller.primaryTextColor
                    typeface = defBoldTypeface
                    isFakeBoldText = true
                }
            }
            else -> {
                isOrdinary = true
                monthPaint.apply {
                    color = controller.secondaryTextColor
                    typeface = defNormalTypeface
                    isFakeBoldText = false
                }
            }
        }

        if (isOrdinary) {
            monthPaint.color = controller.primaryTextColor
        }
        canvas.drawText(monthNumber, x, y, monthPaint)

        if (monthTitle.isNotEmpty()) {
            val monthNumberWidth: Float =
                monthPaint.measureText("$monthNumber ", 0, monthNumber.length + 1)

            if (isOrdinary) {
                monthPaint.color = controller.secondaryTextColor
            }
            canvas.drawText(
                monthTitle,
                if (isRtl) x - monthNumberWidth else x + monthNumberWidth, y,
                monthPaint
            )
        }
    }

//--------------------------------------------------------------------------------------------------
//  Handler for event
//--------------------------------------------------------------------------------------------------

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_UP -> {
                val month: Int = getMonthFromLocation(event.x, event.y)

                if (month >= 0 &&
                    !controller.isOutOfRange(MONTH, showedYear, month)
                ) {
                    onMonthClickListener?.onMonthClick(
                        DateTime(System.currentTimeMillis())
                            .apply {
                                day = 1
                                this.month = month
                                year = showedYear
                            }
                    )
                }
            }
        }
        return true
    }

    private fun getMonthFromLocation(x: Float, y: Float): Int {
        val columnWidth: Int = width / NUM_COLUMNS

        val cellColumn: Int = floor((if (isRtl) (width - x) else x) / columnWidth).toInt()
        val cellRow: Int = floor((y - headerSize) / rowHeight).toInt()

        return cellRow * NUM_COLUMNS + cellColumn
    }

    companion object {
        const val NUM_ROWS = 4
        const val NUM_COLUMNS = 3
    }
}