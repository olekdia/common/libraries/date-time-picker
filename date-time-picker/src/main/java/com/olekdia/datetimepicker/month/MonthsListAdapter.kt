package com.olekdia.datetimepicker.month

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView.LayoutParams
import android.widget.BaseAdapter
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.interfaces.DatePickerController

class MonthsListAdapter(
    private val context: Context,
    private val controller: DatePickerController
) : BaseAdapter(),
    MonthsView.OnMonthClickListener {

    private val currDateTime =
        DateTime(System.currentTimeMillis())

    var selectedMonth: DateTime = controller.currentSelectedDt

    var viewHeight: Int = 0

    override fun getCount(): Int =
        controller.endDt.year - controller.startDt.year + 1

    override fun getItem(position: Int): Any? = null

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getView(
        position: Int,
        convertView: View?,
        parent: ViewGroup
    ): View {
        val view: MonthsView = convertView as? MonthsView ?: MonthsView(context, controller)
            .apply {
                layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
            }

        view.onMonthClickListener = this
        view.viewHeight = viewHeight

        view.setupDateParams(
            selectedMonth.year, selectedMonth.month,
            currDateTime.year, currDateTime.month,
            position + controller.minYear
        )

        return view
    }

    override fun onMonthClick(month: DateTime) {
        controller.apply {
            tryVibrate()
            onMonthSelected(month.year, month.month)
        }
        selectedMonth = month
        notifyDataSetChanged()
    }
}
