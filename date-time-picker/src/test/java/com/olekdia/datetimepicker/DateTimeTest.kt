package com.olekdia.datetimepicker

import android.os.Build
import com.olekdia.datetimepicker.common.DateTime
import org.junit.Assert.*
import org.junit.Test
import org.robolectric.annotation.Config
import org.robolectric.annotation.LooperMode
import java.util.*
import kotlin.math.abs

@Config(sdk = [Build.VERSION_CODES.M], manifest = Config.NONE)
@LooperMode(LooperMode.Mode.LEGACY)
class DateTimeTest {

    //creation

    @Test
    fun createDateTime_initBy_calendarTimeInMillis_equals_systemCurrentTimeMillis() {
        val dateTimeBySystemMillis =
            DateTime(System.currentTimeMillis())
        val dateTimeByCalendarMillis =
            DateTime(Calendar.getInstance().timeInMillis)

        assertEquals(dateTimeBySystemMillis, dateTimeByCalendarMillis)
        assertEquals(dateTimeBySystemMillis.timeInMillis, dateTimeByCalendarMillis.timeInMillis)
    }

    @Test
    fun createDateTime_initBy_calendar_correctDateSelected() {
        val calendar = Calendar.getInstance()
        calendar.set(2020, Calendar.DECEMBER, 20)

        DateTime(calendar.timeInMillis).let { dateTime ->
            assertEquals(2020, dateTime.year)
            assertEquals(Calendar.DECEMBER, dateTime.month)
            assertEquals(20, dateTime.day)
        }

        calendar.set(2020, Calendar.NOVEMBER, 19)
        DateTime(calendar.timeInMillis).let { dateTime ->
            assertEquals(2020, dateTime.year)
            assertEquals(Calendar.NOVEMBER, dateTime.month)
            assertEquals(19, dateTime.day)
        }

        calendar.set(2020, Calendar.OCTOBER, 18)
        DateTime(calendar.timeInMillis).let { dateTime ->
            assertEquals(2020, dateTime.year)
            assertEquals(Calendar.OCTOBER, dateTime.month)
            assertEquals(18, dateTime.day)
        }

        calendar.set(2020, Calendar.SEPTEMBER, 11)
        DateTime(calendar.timeInMillis).let { dateTime ->
            assertEquals(2020, dateTime.year)
            assertEquals(Calendar.SEPTEMBER, dateTime.month)
            assertEquals(11, dateTime.day)
        }
    }

    //equals

    @Test
    fun sameDateTime_equalsIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        assertTrue(dateTime1 == dateTime2)

        dateTime1.year = 2015
        dateTime2.year = 2015
        assertTrue(dateTime1 == dateTime2)

        dateTime1.month = Calendar.JANUARY
        dateTime2.month = Calendar.JANUARY
        assertTrue(dateTime1 == dateTime2)

        dateTime1.day = 1
        dateTime2.day = 1
        assertTrue(dateTime1 == dateTime2)

        dateTime1.hour24 = 1
        dateTime2.hour24 = 1
        assertTrue(dateTime1 == dateTime2)

        dateTime1.minute = 1
        dateTime2.minute = 1
        assertTrue(dateTime1 == dateTime2)

        dateTime1.second = 1
        dateTime2.second = 1
        assertTrue(dateTime1 == dateTime2)
    }

    @Test
    fun differentDateTime_equalsIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2014
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1 == dateTime2)

        dateTime1.apply {
            year = 2015
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1 == dateTime2)

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1 == dateTime2)

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 2
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1 == dateTime2)

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 2
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1 == dateTime2)

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 2
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1 == dateTime2)
    }

    //compare

    @Test
    fun compareDateTime_firstGreater_valueIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2016
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(1, dateTime1.compareTo(dateTime2))
        assertTrue(dateTime1 > dateTime2)

        dateTime1.apply {
            year = 2015
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(1, dateTime1.compareTo(dateTime2))
        assertTrue(dateTime1 > dateTime2)

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(1, dateTime1.compareTo(dateTime2))
        assertTrue(dateTime1 > dateTime2)

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 2
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(1, dateTime1.compareTo(dateTime2))
        assertTrue(dateTime1 > dateTime2)

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 2
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(1, dateTime1.compareTo(dateTime2))
        assertTrue(dateTime1 > dateTime2)

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 2
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(1, dateTime1.compareTo(dateTime2))
        assertTrue(dateTime1 > dateTime2)
    }

    @Test
    fun compareDateTime_equals_valueIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(0, dateTime1.compareTo(dateTime2))
        assertFalse(dateTime1 > dateTime2)
        assertFalse(dateTime1 < dateTime2)
        assertTrue(dateTime1 == dateTime2)
    }

    @Test
    fun compareDateTime_secondGreater_valueIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2016
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(-1, dateTime1.compareTo(dateTime2))
        assertTrue(dateTime1 < dateTime2)

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(-1, dateTime1.compareTo(dateTime2))
        assertTrue(dateTime1 < dateTime2)

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(-1, dateTime1.compareTo(dateTime2))
        assertTrue(dateTime1 < dateTime2)

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 2
            minute = 1
            second = 1
        }
        assertEquals(-1, dateTime1.compareTo(dateTime2))
        assertTrue(dateTime1 < dateTime2)

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 2
            second = 1
        }
        assertEquals(-1, dateTime1.compareTo(dateTime2))
        assertTrue(dateTime1 < dateTime2)

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 2
        }
        assertEquals(-1, dateTime1.compareTo(dateTime2))
        assertTrue(dateTime1 < dateTime2)
    }

    //distance

    @Test
    fun dateTime_distanceBetweenIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(0, abs(dateTime1.distance(dateTime2)))
        assertEquals(abs(dateTime1.distance(dateTime2)), abs(dateTime2.distance(dateTime1)))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 2
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(1000, abs(dateTime1.distance(dateTime2)))
        assertNotEquals(dateTime1.distance(dateTime2), dateTime2.distance(dateTime1))
        assertEquals(abs(dateTime1.distance(dateTime2)), abs(dateTime2.distance(dateTime1)))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 2
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(1000 * 60, abs(dateTime1.distance(dateTime2)))
        assertNotEquals(dateTime1.distance(dateTime2), dateTime2.distance(dateTime1))
        assertEquals(abs(dateTime1.distance(dateTime2)), abs(dateTime2.distance(dateTime1)))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 2
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(1000 * 60 * 60, abs(dateTime1.distance(dateTime2)))
        assertNotEquals(dateTime1.distance(dateTime2), dateTime2.distance(dateTime1))
        assertEquals(abs(dateTime1.distance(dateTime2)), abs(dateTime2.distance(dateTime1)))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(1000 * 60 * 60 * 24, abs(dateTime1.distance(dateTime2)))
        assertNotEquals(dateTime1.distance(dateTime2), dateTime2.distance(dateTime1))
        assertEquals(abs(dateTime1.distance(dateTime2)), abs(dateTime2.distance(dateTime1)))

        dateTime1.apply {
            year = 2015
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(1000L * 60 * 60 * 24 * 31, abs(dateTime1.distance(dateTime2)))
        assertNotEquals(dateTime1.distance(dateTime2), dateTime2.distance(dateTime1))
        assertEquals(abs(dateTime1.distance(dateTime2)), abs(dateTime2.distance(dateTime1)))

        dateTime1.apply {
            year = 2016
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertEquals(1000L * 60 * 60 * 24 * 365, abs(dateTime1.distance(dateTime2)))
        assertNotEquals(dateTime1.distance(dateTime2), dateTime2.distance(dateTime1))
        assertEquals(abs(dateTime1.distance(dateTime2)), abs(dateTime2.distance(dateTime1)))
    }

    //isSame

    @Test
    fun dateTime_sameYear_isSameYearIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertTrue(dateTime1.isSameYear(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertTrue(dateTime1.isSameYear(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 1
        }
        assertTrue(dateTime1.isSameYear(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 2
            minute = 1
            second = 1
        }
        assertTrue(dateTime1.isSameYear(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 2
            second = 1
        }
        assertTrue(dateTime1.isSameYear(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 2
        }
        assertTrue(dateTime1.isSameYear(dateTime2))
    }

    @Test
    fun dateTime_differentYear_isSameYearIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2016
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSameYear(dateTime2))
    }

    @Test
    fun dateTime_sameMonth_isSameYearIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertTrue(dateTime1.isSameMonth(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 1
        }
        assertTrue(dateTime1.isSameMonth(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 2
            minute = 1
            second = 1
        }
        assertTrue(dateTime1.isSameMonth(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 2
            second = 1
        }
        assertTrue(dateTime1.isSameMonth(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 2
        }
        assertTrue(dateTime1.isSameMonth(dateTime2))
    }

    @Test
    fun dateTime_differentMonth_isSameYearIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2016
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSameMonth(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSameMonth(dateTime2))
    }

    @Test
    fun dateTime_sameDay_isSameYearIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertTrue(dateTime1.isSameDay(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 2
            minute = 1
            second = 1
        }
        assertTrue(dateTime1.isSameDay(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 2
            second = 1
        }
        assertTrue(dateTime1.isSameDay(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 2
        }
        assertTrue(dateTime1.isSameDay(dateTime2))
    }

    @Test
    fun dateTime_differentDay_isSameYearIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2016
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSameDay(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSameDay(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSameDay(dateTime2))
    }

    @Test
    fun dateTime_samePartOfDay_isSameYearIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertTrue(dateTime1.isSamePartOfDay(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 2
            minute = 1
            second = 1
        }
        assertTrue(dateTime1.isSamePartOfDay(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 2
            second = 1
        }
        assertTrue(dateTime1.isSamePartOfDay(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 2
        }
        assertTrue(dateTime1.isSamePartOfDay(dateTime2))
    }

    @Test
    fun dateTime_differentPartOfDay_isSameYearIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2016
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSamePartOfDay(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSamePartOfDay(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSamePartOfDay(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 13
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSamePartOfDay(dateTime2))
    }

    @Test
    fun dateTime_sameHour_isSameYearIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertTrue(dateTime1.isSameHour(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 2
            second = 1
        }
        assertTrue(dateTime1.isSameHour(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 2
        }
        assertTrue(dateTime1.isSameHour(dateTime2))
    }

    @Test
    fun dateTime_differentHour_isSameYearIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2016
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSameHour(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSameHour(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSameHour(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 2
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSameHour(dateTime2))
    }

    @Test
    fun dateTime_sameMinute_isSameYearIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertTrue(dateTime1.isSameMinute(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 2
        }
        assertTrue(dateTime1.isSameMinute(dateTime2))
    }

    @Test
    fun dateTime_differentMinute_isSameYearIsValid() {
        val dateTime1 = DateTime(System.currentTimeMillis())
        val dateTime2 = DateTime(System.currentTimeMillis())

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2016
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSameMinute(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSameMinute(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSameMinute(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 2
            minute = 1
            second = 1
        }
        assertFalse(dateTime1.isSameMinute(dateTime2))

        dateTime1.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 1
            second = 1
        }
        dateTime2.apply {
            year = 2015
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 2
            second = 1
        }
        assertFalse(dateTime1.isSameMinute(dateTime2))
    }
}