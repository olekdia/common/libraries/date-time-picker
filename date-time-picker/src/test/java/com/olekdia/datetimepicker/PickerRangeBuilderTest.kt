package com.olekdia.datetimepicker

import android.content.Context
import android.os.Build
import androidx.test.core.app.ApplicationProvider
import com.olekdia.common.INVALID
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.annotation.LooperMode
import java.util.*

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.M], manifest = Config.NONE)
@LooperMode(LooperMode.Mode.LEGACY)
class PickerRangeBuilderTest {

    private lateinit var context: Context

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()
    }

//--------------------------------------------------------------------------------------------------
//  Valid/Invalid dates
//--------------------------------------------------------------------------------------------------

    @Test
    fun `create builder with valid date - no exception`() {
        var picker: Picker = PickerRangeBuilder(context)
            .selectedStartDate(2013, Calendar.FEBRUARY, 28)
            .selectedEndDate(2013, Calendar.MARCH, 1)
            .build()

        picker = PickerRangeBuilder(context)
            .selectedStartDate(2013, Calendar.DECEMBER, 31)
            .selectedEndDate(2014, Calendar.JANUARY, 1)
            .build()

        picker = PickerRangeBuilder(context)
            .selectedStartDate(2013, Calendar.APRIL, 30)
            .selectedEndDate(2013, Calendar.MAY, 1)
            .build()
    }

    @Test
    fun `create builder with valid feb 29 date on leap year - no exception`() {
        var picker: Picker = PickerRangeBuilder(context)
            .selectedStartDate(2020, Calendar.FEBRUARY, 29)
            .selectedEndDate(2020, Calendar.MARCH, 1)
            .build()
    }

    //invalid day

    @Test(expected = IllegalArgumentException::class)
    fun `create builder with invalid start feb 30 date - exception`() {
        var picker: Picker = PickerRangeBuilder(context)
            .selectedStartDate(2013, Calendar.FEBRUARY, 30)
            .selectedEndDate(2013, Calendar.MARCH, 1)
            .build()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `create builder with invalid end feb 30 date - exception`() {
        var picker: Picker = PickerRangeBuilder(context)
            .selectedStartDate(2013, Calendar.FEBRUARY, 1)
            .selectedEndDate(2013, Calendar.FEBRUARY, 30)
            .build()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `create builder with invalid start feb 29 date on not leap year - exception`() {
        var picker: Picker = PickerRangeBuilder(context)
            .selectedStartDate(2019, Calendar.FEBRUARY, 29)
            .selectedEndDate(2019, Calendar.MARCH, 1)
            .build()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `create builder with invalid end feb 29 date on not leap year - exception`() {
        var picker: Picker = PickerRangeBuilder(context)
            .selectedStartDate(2019, Calendar.FEBRUARY, 1)
            .selectedEndDate(2019, Calendar.FEBRUARY, 29)
            .build()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `create builder with invalid start april 31 date - exception`() {
        var picker: Picker = PickerRangeBuilder(context)
            .selectedStartDate(2013, Calendar.APRIL, 31)
            .selectedEndDate(2013, Calendar.MAY, 1)
            .build()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `create builder with invalid end april 31 date - exception`() {
        var picker: Picker = PickerRangeBuilder(context)
            .selectedStartDate(2013, Calendar.APRIL, 1)
            .selectedEndDate(2013, Calendar.APRIL, 31)
            .build()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `create builder with invalid start august 32 date - exception`() {
        var picker: Picker = PickerRangeBuilder(context)
            .selectedStartDate(2013, Calendar.AUGUST, 32)
            .selectedEndDate(2013, Calendar.SEPTEMBER, 1)
            .build()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `create builder with invalid end august 32 date - exception`() {
        var picker: Picker = PickerRangeBuilder(context)
            .selectedStartDate(2013, Calendar.AUGUST, 1)
            .selectedEndDate(2013, Calendar.AUGUST, 32)
            .build()
    }

//--------------------------------------------------------------------------------------------------
//  Picker with different dt values
//--------------------------------------------------------------------------------------------------

    //date

    @Test
    fun `create builder, year - year available, other values not`() {
        val picker: Picker = PickerRangeBuilder(context)
            .selectedStartDate(2020)
            .selectedEndDate(2021)
            .build()

        val startDate = picker.getSelectedStartDate()
        val startTime = picker.getSelectedStartTime()

        val endDate = picker.getSelectedEndDate()
        val endTime = picker.getSelectedEndTime()

        assertNotNull(startDate)
        assertNotNull(startTime)

        assertNotNull(endDate)
        assertNotNull(endTime)

        assertEquals(2020, startDate?.first)
        assertEquals(INVALID, startDate?.second)
        assertEquals(INVALID, startDate?.third)

        assertEquals(INVALID, startTime?.first)
        assertEquals(INVALID, startTime?.second)
        assertEquals(INVALID, startTime?.third)

        assertEquals(2021, endDate?.first)
        assertEquals(INVALID, endDate?.second)
        assertEquals(INVALID, endDate?.third)

        assertEquals(INVALID, endTime?.first)
        assertEquals(INVALID, endTime?.second)
        assertEquals(INVALID, endTime?.third)
    }

    @Test
    fun `create builder, month - month available, other values not`() {
        val picker: Picker = PickerRangeBuilder(context)
            .selectedStartDate(month = Calendar.JANUARY)
            .selectedEndDate(month = Calendar.FEBRUARY)
            .build()

        val startDate = picker.getSelectedStartDate()
        val startTime = picker.getSelectedStartTime()

        val endDate = picker.getSelectedEndDate()
        val endTime = picker.getSelectedEndTime()

        assertNotNull(startDate)
        assertNotNull(startTime)

        assertNotNull(endDate)
        assertNotNull(endTime)

        assertEquals(INVALID, startDate?.first)
        assertEquals(Calendar.JANUARY, startDate?.second)
        assertEquals(INVALID, startDate?.third)

        assertEquals(INVALID, startTime?.first)
        assertEquals(INVALID, startTime?.second)
        assertEquals(INVALID, startTime?.third)

        assertEquals(INVALID, endDate?.first)
        assertEquals(Calendar.FEBRUARY, endDate?.second)
        assertEquals(INVALID, endDate?.third)

        assertEquals(INVALID, endTime?.first)
        assertEquals(INVALID, endTime?.second)
        assertEquals(INVALID, endTime?.third)
    }

    @Test
    fun `create builder, day - day available, other values not`() {
        val picker: Picker = PickerRangeBuilder(context)
            .selectedStartDate(day = 1)
            .selectedEndDate(day = 2)
            .build()

        val startDate = picker.getSelectedStartDate()
        val startTime = picker.getSelectedStartTime()

        val endDate = picker.getSelectedEndDate()
        val endTime = picker.getSelectedEndTime()

        assertNotNull(startDate)
        assertNotNull(startTime)

        assertNotNull(endDate)
        assertNotNull(endTime)

        assertEquals(INVALID, startDate?.first)
        assertEquals(INVALID, startDate?.second)
        assertEquals(1, startDate?.third)

        assertEquals(INVALID, startTime?.first)
        assertEquals(INVALID, startTime?.second)
        assertEquals(INVALID, startTime?.third)

        assertEquals(INVALID, endDate?.first)
        assertEquals(INVALID, endDate?.second)
        assertEquals(2, endDate?.third)

        assertEquals(INVALID, endTime?.first)
        assertEquals(INVALID, endTime?.second)
        assertEquals(INVALID, endTime?.third)
    }

    @Test
    fun `create builder, date - date values available, time values not`() {
        val picker: Picker = PickerRangeBuilder(context)
            .selectedStartDate(2020, Calendar.MAY, 5)
            .selectedEndDate(2021, Calendar.MAY, 5)
            .build()

        val startDate = picker.getSelectedStartDate()
        val startTime = picker.getSelectedStartTime()

        val endDate = picker.getSelectedEndDate()
        val endTime = picker.getSelectedEndTime()

        assertNotNull(startDate)
        assertNotNull(startTime)

        assertNotNull(endDate)
        assertNotNull(endTime)

        assertEquals(2020, startDate?.first)
        assertEquals(Calendar.MAY, startDate?.second)
        assertEquals(5, startDate?.third)

        assertEquals(INVALID, startTime?.first)
        assertEquals(INVALID, startTime?.second)
        assertEquals(INVALID, startTime?.third)

        assertEquals(2021, endDate?.first)
        assertEquals(Calendar.MAY, endDate?.second)
        assertEquals(5, endDate?.third)

        assertEquals(INVALID, endTime?.first)
        assertEquals(INVALID, endTime?.second)
        assertEquals(INVALID, endTime?.third)
    }

    //time

    @Test
    fun `create builder, hour - hour available, other values not`() {
        val picker: Picker = PickerRangeBuilder(context)
            .selectedStartTime(8)
            .selectedEndTime(9)
            .build()

        val startDate = picker.getSelectedStartDate()
        val startTime = picker.getSelectedStartTime()

        val endDate = picker.getSelectedEndDate()
        val endTime = picker.getSelectedEndTime()

        assertNotNull(startDate)
        assertNotNull(startTime)

        assertNotNull(endDate)
        assertNotNull(endTime)

        assertEquals(INVALID, startDate?.first)
        assertEquals(INVALID, startDate?.second)
        assertEquals(INVALID, startDate?.third)

        assertEquals(8, startTime?.first)
        assertEquals(INVALID, startTime?.second)
        assertEquals(INVALID, startTime?.third)

        assertEquals(INVALID, endDate?.first)
        assertEquals(INVALID, endDate?.second)
        assertEquals(INVALID, endDate?.third)

        assertEquals(9, endTime?.first)
        assertEquals(INVALID, endTime?.second)
        assertEquals(INVALID, endTime?.third)
    }

    @Test
    fun `create builder, minute - minute available, other values not`() {
        val picker: Picker = PickerRangeBuilder(context)
            .selectedStartTime(minute = 8)
            .selectedEndTime(minute = 9)
            .build()

        val startDate = picker.getSelectedStartDate()
        val startTime = picker.getSelectedStartTime()

        val endDate = picker.getSelectedEndDate()
        val endTime = picker.getSelectedEndTime()

        assertNotNull(startDate)
        assertNotNull(startTime)

        assertNotNull(endDate)
        assertNotNull(endTime)

        assertEquals(INVALID, startDate?.first)
        assertEquals(INVALID, startDate?.second)
        assertEquals(INVALID, startDate?.third)

        assertEquals(INVALID, startTime?.first)
        assertEquals(8, startTime?.second)
        assertEquals(INVALID, startTime?.third)

        assertEquals(INVALID, endDate?.first)
        assertEquals(INVALID, endDate?.second)
        assertEquals(INVALID, endDate?.third)

        assertEquals(INVALID, endTime?.first)
        assertEquals(9, endTime?.second)
        assertEquals(INVALID, endTime?.third)
    }

    @Test
    fun `create builder, second - second available, other values not`() {
        val picker: Picker = PickerRangeBuilder(context)
            .selectedStartTime(second = 8)
            .selectedEndTime(second = 9)
            .build()

        val startDate = picker.getSelectedStartDate()
        val startTime = picker.getSelectedStartTime()

        val endDate = picker.getSelectedEndDate()
        val endTime = picker.getSelectedEndTime()

        assertNotNull(startDate)
        assertNotNull(startTime)

        assertNotNull(endDate)
        assertNotNull(endTime)

        assertEquals(INVALID, startDate?.first)
        assertEquals(INVALID, startDate?.second)
        assertEquals(INVALID, startDate?.third)

        assertEquals(INVALID, startTime?.first)
        assertEquals(INVALID, startTime?.second)
        assertEquals(8, startTime?.third)

        assertEquals(INVALID, endDate?.first)
        assertEquals(INVALID, endDate?.second)
        assertEquals(INVALID, endDate?.third)

        assertEquals(INVALID, endTime?.first)
        assertEquals(INVALID, endTime?.second)
        assertEquals(9, endTime?.third)
    }

    @Test
    fun `create builder, time - time value available, date values not`() {
        val picker: Picker = PickerRangeBuilder(context)
            .selectedStartTime(8, 8, 8)
            .selectedEndTime(9, 9, 9)
            .build()

        val startDate = picker.getSelectedStartDate()
        val startTime = picker.getSelectedStartTime()

        val endDate = picker.getSelectedEndDate()
        val endTime = picker.getSelectedEndTime()

        assertNotNull(startDate)
        assertNotNull(startTime)

        assertNotNull(endDate)
        assertNotNull(endTime)

        assertEquals(INVALID, startDate?.first)
        assertEquals(INVALID, startDate?.second)
        assertEquals(INVALID, startDate?.third)

        assertEquals(8, startTime?.first)
        assertEquals(8, startTime?.second)
        assertEquals(8, startTime?.third)

        assertEquals(INVALID, endDate?.first)
        assertEquals(INVALID, endDate?.second)
        assertEquals(INVALID, endDate?.third)

        assertEquals(9, endTime?.first)
        assertEquals(9, endTime?.second)
        assertEquals(9, endTime?.third)
    }
}