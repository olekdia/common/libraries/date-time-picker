package com.olekdia.datetimepicker

import android.content.Context
import android.os.Build
import androidx.test.core.app.ApplicationProvider
import com.olekdia.datetimepicker.day.DaysView
import com.olekdia.datetimepicker.interfaces.SelectionChangeListener
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.annotation.LooperMode
import java.util.*

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.M], manifest = Config.NONE)
@LooperMode(LooperMode.Mode.LEGACY)
class DaysViewTest {

    private lateinit var context: Context

    private lateinit var controller: Controller

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()
        controller = Controller(context, PickerBuilder(context), object : SelectionChangeListener {
            override fun onHeaderSelectionChanged(changePicker: Boolean, updatePicker: Boolean) {}

            override fun onAmPmSelectionChanged(updatePicker: Boolean, amPm: Int, part: Int) {}

            override fun onDtSelectionChanged(changePicker: Boolean, updatePicker: Boolean) {}
        })
    }

//--------------------------------------------------------------------------------------------------
//  Valid/Invalid dates
//--------------------------------------------------------------------------------------------------

    @Test
    fun `setupDateParams diff values - numRows correct`() {
        val view: DaysView = DaysView(context, controller)

        view.setupDateParams(
            currYear = 2020,
            currMonth = Calendar.OCTOBER,
            currDay = 27,
            currDayOfWeek = Calendar.TUESDAY,
            firstDayInWeek = Calendar.SUNDAY
        )
        assertEquals(5, view.numRows)

        view.setupDateParams(
            currYear = 2020,
            currMonth = Calendar.SEPTEMBER,
            currDay = 27,
            currDayOfWeek = Calendar.SUNDAY,
            firstDayInWeek = Calendar.SUNDAY
        )
        assertEquals(5, view.numRows)

        view.setupDateParams(
            currYear = 2020,
            currMonth = Calendar.AUGUST,
            currDay = 27,
            currDayOfWeek = Calendar.THURSDAY,
            firstDayInWeek = Calendar.SUNDAY
        )
        assertEquals(6, view.numRows)

        view.setupDateParams(
            currYear = 2020,
            currMonth = Calendar.JULY,
            currDay = 27,
            currDayOfWeek = Calendar.MONDAY,
            firstDayInWeek = Calendar.SUNDAY
        )
        assertEquals(5, view.numRows)

        view.setupDateParams(
            currYear = 2020,
            currMonth = Calendar.JUNE,
            currDay = 27,
            currDayOfWeek = Calendar.SATURDAY,
            firstDayInWeek = Calendar.SUNDAY
        )
        assertEquals(5, view.numRows)

        view.setupDateParams(
            currYear = 2020,
            currMonth = Calendar.MAY,
            currDay = 27,
            currDayOfWeek = Calendar.WEDNESDAY,
            firstDayInWeek = Calendar.SUNDAY
        )
        assertEquals(6, view.numRows)


        view.setupDateParams(
            currYear = 2020,
            currMonth = Calendar.MARCH,
            currDay = 27,
            currDayOfWeek = Calendar.FRIDAY,
            firstDayInWeek = Calendar.SUNDAY
        )
        assertEquals(5, view.numRows)

        view.setupDateParams(
            currYear = 2020,
            currMonth = Calendar.FEBRUARY,
            currDay = 27,
            currDayOfWeek = Calendar.THURSDAY,
            firstDayInWeek = Calendar.SUNDAY
        )
        assertEquals(5, view.numRows)

        view.setupDateParams(
            currYear = 2009,
            currMonth = Calendar.FEBRUARY,
            currDay = 27,
            currDayOfWeek = Calendar.FRIDAY,
            firstDayInWeek = Calendar.SUNDAY
        )
        assertEquals(4, view.numRows)
    }

    @Test
    fun `setupDateParams diff values - currDayOfWeekIndex correct`() {
        val view: DaysView = DaysView(context, controller)

        view.setupDateParams(
            currYear = 2020,
            currMonth = Calendar.OCTOBER,
            currDay = 27,
            currDayOfWeek = Calendar.TUESDAY,
            firstDayInWeek = Calendar.SUNDAY
        )
        assertEquals(2, view.currDayOfWeekIndex)

        view.setupDateParams(
            currYear = 2020,
            currMonth = Calendar.OCTOBER,
            currDay = 27,
            currDayOfWeek = Calendar.TUESDAY,
            firstDayInWeek = Calendar.MONDAY
        )
        assertEquals(1, view.currDayOfWeekIndex)

        view.setupDateParams(
            currYear = 2020,
            currMonth = Calendar.OCTOBER,
            currDay = 27,
            currDayOfWeek = Calendar.TUESDAY,
            firstDayInWeek = Calendar.TUESDAY
        )
        assertEquals(0, view.currDayOfWeekIndex)

        view.setupDateParams(
            currYear = 2020,
            currMonth = Calendar.OCTOBER,
            currDay = 27,
            currDayOfWeek = Calendar.TUESDAY,
            firstDayInWeek = Calendar.WEDNESDAY
        )
        assertEquals(6, view.currDayOfWeekIndex)

        view.setupDateParams(
            currYear = 2020,
            currMonth = Calendar.OCTOBER,
            currDay = 27,
            currDayOfWeek = Calendar.TUESDAY,
            firstDayInWeek = Calendar.THURSDAY
        )
        assertEquals(5, view.currDayOfWeekIndex)

        view.setupDateParams(
            currYear = 2020,
            currMonth = Calendar.OCTOBER,
            currDay = 27,
            currDayOfWeek = Calendar.TUESDAY,
            firstDayInWeek = Calendar.FRIDAY
        )
        assertEquals(4, view.currDayOfWeekIndex)

        view.setupDateParams(
            currYear = 2020,
            currMonth = Calendar.OCTOBER,
            currDay = 27,
            currDayOfWeek = Calendar.TUESDAY,
            firstDayInWeek = Calendar.SATURDAY
        )
        assertEquals(3, view.currDayOfWeekIndex)


        view.setupDateParams(
            currYear = 2009,
            currMonth = Calendar.FEBRUARY,
            currDay = 27,
            currDayOfWeek = Calendar.FRIDAY,
            firstDayInWeek = Calendar.SUNDAY
        )
        assertEquals(5, view.currDayOfWeekIndex)


        view.setupDateParams(
            currYear = 2009,
            currMonth = Calendar.FEBRUARY,
            currDay = 27,
            currDayOfWeek = Calendar.FRIDAY,
            firstDayInWeek = Calendar.MONDAY
        )
        assertEquals(4, view.currDayOfWeekIndex)


        view.setupDateParams(
            currYear = 2009,
            currMonth = Calendar.FEBRUARY,
            currDay = 27,
            currDayOfWeek = Calendar.FRIDAY,
            firstDayInWeek = Calendar.TUESDAY
        )
        assertEquals(3, view.currDayOfWeekIndex)

        view.setupDateParams(
            currYear = 2009,
            currMonth = Calendar.FEBRUARY,
            currDay = 27,
            currDayOfWeek = Calendar.FRIDAY,
            firstDayInWeek = Calendar.WEDNESDAY
        )
        assertEquals(2, view.currDayOfWeekIndex)

        view.setupDateParams(
            currYear = 2009,
            currMonth = Calendar.FEBRUARY,
            currDay = 27,
            currDayOfWeek = Calendar.FRIDAY,
            firstDayInWeek = Calendar.THURSDAY
        )
        assertEquals(1, view.currDayOfWeekIndex)

        view.setupDateParams(
            currYear = 2009,
            currMonth = Calendar.FEBRUARY,
            currDay = 27,
            currDayOfWeek = Calendar.FRIDAY,
            firstDayInWeek = Calendar.FRIDAY
        )
        assertEquals(0, view.currDayOfWeekIndex)

        view.setupDateParams(
            currYear = 2009,
            currMonth = Calendar.FEBRUARY,
            currDay = 27,
            currDayOfWeek = Calendar.FRIDAY,
            firstDayInWeek = Calendar.SATURDAY
        )
        assertEquals(6, view.currDayOfWeekIndex)
    }
}