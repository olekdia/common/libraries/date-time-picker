package com.olekdia.datetimepicker

import android.content.Context
import android.os.Build
import androidx.test.core.app.ApplicationProvider
import com.olekdia.common.INVALID
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.annotation.LooperMode
import java.util.*

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.M], manifest = Config.NONE)
@LooperMode(LooperMode.Mode.LEGACY)
class PickerBuilderTest {

    private lateinit var context: Context

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()
    }

//--------------------------------------------------------------------------------------------------
//  Valid/Invalid dates
//--------------------------------------------------------------------------------------------------

    @Test
    fun `create builder with valid date - no exception`() {
        var picker: Picker = PickerBuilder(context)
            .selectedDate(2013, Calendar.FEBRUARY, 28)
            .build()

        picker = PickerBuilder(context)
            .selectedDate(2013, Calendar.DECEMBER, 31)
            .build()

        picker = PickerBuilder(context)
            .selectedDate(2013, Calendar.APRIL, 30)
            .build()
    }

    @Test
    fun `create builder with valid feb 29 date on leap year - no exception`() {
        var picker: Picker = PickerBuilder(context)
            .selectedDate(2020, Calendar.FEBRUARY, 29)
            .build()
    }

    //invalid day

    @Test(expected = IllegalArgumentException::class)
    fun `create builder with invalid feb 30 date - exception`() {
        var picker: Picker = PickerBuilder(context)
            .selectedDate(2013, Calendar.FEBRUARY, 30)
            .build()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `create builder with invalid feb 29 date on not leap year - exception`() {
        var picker: Picker = PickerBuilder(context)
            .selectedDate(2019, Calendar.FEBRUARY, 29)
            .build()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `create builder with invalid april 31 date - exception`() {
        var picker: Picker = PickerBuilder(context)
            .selectedDate(2013, Calendar.APRIL, 31)
            .build()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `create builder with invalid august 32 date - exception`() {
        var picker: Picker = PickerBuilder(context)
            .selectedDate(2013, Calendar.AUGUST, 32)
            .build()
    }

//--------------------------------------------------------------------------------------------------
//  Picker with different dt values
//--------------------------------------------------------------------------------------------------

    //date

    @Test
    fun `create builder, year - year available, other values not`() {
        val picker: Picker = PickerBuilder(context)
            .selectedDate(2020)
            .build()

        val date = picker.getSelectedDate()
        val time = picker.getSelectedTime()

        assertEquals(2020, date.first)
        assertEquals(INVALID, date.second)
        assertEquals(INVALID, date.third)

        assertEquals(INVALID, time.first)
        assertEquals(INVALID, time.second)
        assertEquals(INVALID, time.third)
    }

    @Test
    fun `create builder, month - month available, other values not`() {
        val picker: Picker = PickerBuilder(context)
            .selectedDate(month = Calendar.JANUARY)
            .build()

        val date = picker.getSelectedDate()
        val time = picker.getSelectedTime()

        assertEquals(INVALID, date.first)
        assertEquals(Calendar.JANUARY, date.second)
        assertEquals(INVALID, date.third)

        assertEquals(INVALID, time.first)
        assertEquals(INVALID, time.second)
        assertEquals(INVALID, time.third)
    }

    @Test
    fun `create builder, day - day available, other values not`() {
        val picker: Picker = PickerBuilder(context)
            .selectedDate(day = 1)
            .build()

        val date = picker.getSelectedDate()
        val time = picker.getSelectedTime()

        assertEquals(INVALID, date.first)
        assertEquals(INVALID, date.second)
        assertEquals(1, date.third)

        assertEquals(INVALID, time.first)
        assertEquals(INVALID, time.second)
        assertEquals(INVALID, time.third)
    }

    @Test
    fun `create builder, date - date values available, time values not`() {
        val picker: Picker = PickerBuilder(context)
            .selectedDate(2020, Calendar.MAY, 5)
            .build()

        val date = picker.getSelectedDate()
        val time = picker.getSelectedTime()

        assertEquals(2020, date.first)
        assertEquals(Calendar.MAY, date.second)
        assertEquals(5, date.third)

        assertEquals(INVALID, time.first)
        assertEquals(INVALID, time.second)
        assertEquals(INVALID, time.third)
    }

    //time

    @Test
    fun `create builder, hour - hour available, other values not`() {
        val picker: Picker = PickerBuilder(context)
            .selectedTime(8)
            .build()

        val date = picker.getSelectedDate()
        val time = picker.getSelectedTime()

        assertEquals(INVALID, date.first)
        assertEquals(INVALID, date.second)
        assertEquals(INVALID, date.third)

        assertEquals(8, time.first)
        assertEquals(INVALID, time.second)
        assertEquals(INVALID, time.third)
    }

    @Test
    fun `create builder, minute - minute available, other values not`() {
        val picker: Picker = PickerBuilder(context)
            .selectedTime(minute = 8)
            .build()

        val date = picker.getSelectedDate()
        val time = picker.getSelectedTime()

        assertEquals(INVALID, date.first)
        assertEquals(INVALID, date.second)
        assertEquals(INVALID, date.third)

        assertEquals(INVALID, time.first)
        assertEquals(8, time.second)
        assertEquals(INVALID, time.third)
    }

    @Test
    fun `create builder, second - second available, other values not`() {
        val picker: Picker = PickerBuilder(context)
            .selectedTime(second = 8)
            .build()

        val date = picker.getSelectedDate()
        val time = picker.getSelectedTime()

        assertEquals(INVALID, date.first)
        assertEquals(INVALID, date.second)
        assertEquals(INVALID, date.third)

        assertEquals(INVALID, time.first)
        assertEquals(INVALID, time.second)
        assertEquals(8, time.third)
    }

    @Test
    fun `create builder, time - time value available, date values not`() {
        val picker: Picker = PickerBuilder(context)
            .selectedTime(8, 8, 8)
            .build()

        val date = picker.getSelectedDate()
        val time = picker.getSelectedTime()

        assertEquals(INVALID, date.first)
        assertEquals(INVALID, date.second)
        assertEquals(INVALID, date.third)

        assertEquals(8, time.first)
        assertEquals(8, time.second)
        assertEquals(8, time.third)
    }
}