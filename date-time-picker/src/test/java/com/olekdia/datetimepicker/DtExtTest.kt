package com.olekdia.datetimepicker

import android.os.Build
import com.olekdia.common.INVALID
import com.olekdia.datetimepicker.common.*
import com.olekdia.datetimepicker.common.PickerIndex.Companion.DAY
import com.olekdia.datetimepicker.common.PickerIndex.Companion.HOUR
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MINUTE
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MONTH
import com.olekdia.datetimepicker.common.PickerIndex.Companion.SECOND
import com.olekdia.datetimepicker.common.PickerIndex.Companion.YEAR
import org.junit.Assert.*
import org.junit.Test
import org.robolectric.annotation.Config
import org.robolectric.annotation.LooperMode
import java.util.*

@Config(sdk = [Build.VERSION_CODES.M], manifest = Config.NONE)
@LooperMode(LooperMode.Mode.LEGACY)
class DtExtTest {

    //createRoundedCopy

    @Test
    fun year_createRoundedCopy_copyIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val copy = dateTime.createRoundedCopy(YEAR)

        assertEquals(dateTime.year, copy.year)
        assertEquals(Calendar.JANUARY, copy.month)
        assertEquals(1, copy.day)

        assertEquals(0, copy.hour24)
        assertEquals(0, copy.minute)
        assertEquals(0, copy.second)

        assertEquals(0, copy.millis)
    }

    @Test
    fun month_createRoundedCopy_copyIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val copy = dateTime.createRoundedCopy(MONTH)

        assertEquals(dateTime.year, copy.year)
        assertEquals(dateTime.month, copy.month)
        assertEquals(1, copy.day)

        assertEquals(0, copy.hour24)
        assertEquals(0, copy.minute)
        assertEquals(0, copy.second)

        assertEquals(0, copy.millis)
    }

    @Test
    fun day_createRoundedCopy_copyIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val copy = dateTime.createRoundedCopy(DAY)

        assertEquals(dateTime.year, copy.year)
        assertEquals(dateTime.month, copy.month)
        assertEquals(dateTime.day, copy.day)

        assertEquals(0, copy.hour24)
        assertEquals(0, copy.minute)
        assertEquals(0, copy.second)

        assertEquals(0, copy.millis)
    }

    @Test
    fun hour_createRoundedCopy_copyIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val copy = dateTime.createRoundedCopy(HOUR)

        assertEquals(dateTime.year, copy.year)
        assertEquals(dateTime.month, copy.month)
        assertEquals(dateTime.day, copy.day)

        assertEquals(dateTime.hour24, copy.hour24)
        assertEquals(0, copy.minute)
        assertEquals(0, copy.second)

        assertEquals(0, copy.millis)
    }

    @Test
    fun minute_createRoundedCopy_copyIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val copy = dateTime.createRoundedCopy(MINUTE)

        assertEquals(dateTime.year, copy.year)
        assertEquals(dateTime.month, copy.month)
        assertEquals(dateTime.day, copy.day)

        assertEquals(dateTime.hour24, copy.hour24)
        assertEquals(dateTime.minute, copy.minute)
        assertEquals(0, copy.second)

        assertEquals(0, copy.millis)
    }

    //isOutOfRange

    //YEAR

    @Test
    fun isOutOfRange_yearIndex_withoutSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        dateTime.apply {
            year = 1995
        }

        val isOut =
            dateTime.isOutOfRange(YEAR, null, null, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_yearIndex_withoutSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1995
        }

        dateTime.apply {
            year = 1994
        }

        var isOut =
            dateTime.isOutOfRange(YEAR, null, null, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
        }

        isOut =
            dateTime.isOutOfRange(YEAR, null, null, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1996
        }

        isOut =
            dateTime.isOutOfRange(YEAR, null, null, max)
        assertTrue(isOut)
    }

    @Test
    fun isOutOfRange_yearIndex_withoutSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1995
        }

        dateTime.apply {
            year = 1994
        }

        var isOut =
            dateTime.isOutOfRange(YEAR, null, min, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1995
        }

        isOut =
            dateTime.isOutOfRange(YEAR, null, min, null)
        assertFalse(isOut)

        dateTime.apply {
            year = 1996
        }

        isOut =
            dateTime.isOutOfRange(YEAR, null, min, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_yearIndex_withoutSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1990
        }
        max.apply {
            year = 2000
        }

        dateTime.apply {
            year = 1989
        }

        var isOut =
            dateTime.isOutOfRange(YEAR, null, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
        }

        isOut =
            dateTime.isOutOfRange(YEAR, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
        }

        isOut =
            dateTime.isOutOfRange(YEAR, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 2000
        }

        isOut =
            dateTime.isOutOfRange(YEAR, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 2001
        }

        isOut =
            dateTime.isOutOfRange(YEAR, null, min, max)
        assertTrue(isOut)
    }

    @Test
    fun isOutOfRange_yearIndex_withSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }

        selectable[0].apply {
            year = 1990
        }
        selectable[1].apply {
            year = 1995
        }
        selectable[2].apply {
            year = 2000
        }

        dateTime.apply {
            year = 2020
        }

        var isOut =
            dateTime.isOutOfRange(YEAR, selectable, null, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1995
        }

        isOut =
            dateTime.isOutOfRange(YEAR, selectable, null, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_yearIndex_withSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val max = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
        }
        selectable[1].apply {
            year = 1995
        }
        selectable[2].apply {
            year = 2000
        }
        max.apply {
            year = 1994
        }

        dateTime.apply {
            year = 2020
        }

        var isOut =
            dateTime.isOutOfRange(YEAR, selectable, null, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1995
        }

        isOut =
            dateTime.isOutOfRange(YEAR, selectable, null, max)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_yearIndex_withSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
        }
        selectable[1].apply {
            year = 1995
        }
        selectable[2].apply {
            year = 2000
        }
        min.apply {
            year = 1995
        }


        dateTime.apply {
            year = 2020
        }

        var isOut =
            dateTime.isOutOfRange(YEAR, selectable, min, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
        }

        isOut =
            dateTime.isOutOfRange(YEAR, selectable, min, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_yearIndex_withSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
        }
        selectable[1].apply {
            year = 1995
        }
        selectable[2].apply {
            year = 2000
        }

        min.apply {
            year = 1995
        }
        max.apply {
            year = 2000
        }

        dateTime.apply {
            year = 2020
        }

        var isOut =
            dateTime.isOutOfRange(YEAR, selectable, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1997
        }

        isOut =
            dateTime.isOutOfRange(YEAR, selectable, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1995
        }

        isOut =
            dateTime.isOutOfRange(YEAR, selectable, min, max)
        assertFalse(isOut)
    }

    //MONTH

    @Test
    fun isOutOfRange_monthIndex_withoutSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        dateTime.apply {
            year = 1995
            month = Calendar.JANUARY
        }

        val isOut =
            dateTime.isOutOfRange(MONTH, null, null, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_monthIndex_withoutSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1995
            month = Calendar.MARCH
        }

        dateTime.apply {
            year = 1995
            month = Calendar.JANUARY
        }

        var isOut =
            dateTime.isOutOfRange(MONTH, null, null, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
        }

        isOut =
            dateTime.isOutOfRange(MONTH, null, null, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.APRIL
        }

        isOut =
            dateTime.isOutOfRange(MONTH, null, null, max)
        assertTrue(isOut)
    }

    @Test
    fun isOutOfRange_monthIndex_withoutSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1995
            month = Calendar.MARCH
        }

        dateTime.apply {
            year = 1995
            month = Calendar.JANUARY
        }

        var isOut =
            dateTime.isOutOfRange(MONTH, null, min, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
        }

        isOut =
            dateTime.isOutOfRange(MONTH, null, min, null)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.APRIL
        }

        isOut =
            dateTime.isOutOfRange(MONTH, null, min, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_monthIndex_withoutSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1990
            month = Calendar.MARCH
        }
        max.apply {
            year = 1990
            month = Calendar.SEPTEMBER
        }

        dateTime.apply {
            year = 1990
            month = Calendar.JANUARY
        }

        var isOut =
            dateTime.isOutOfRange(MONTH, null, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
        }

        isOut =
            dateTime.isOutOfRange(MONTH, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.JULY
        }

        isOut =
            dateTime.isOutOfRange(MONTH, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.SEPTEMBER
        }

        isOut =
            dateTime.isOutOfRange(MONTH, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.DECEMBER
        }

        isOut =
            dateTime.isOutOfRange(MONTH, null, min, max)
        assertTrue(isOut)
    }

    @Test
    fun isOutOfRange_monthIndex_withSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.JULY
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.OCTOBER
        }

        dateTime.apply {
            year = 1990
            month = Calendar.DECEMBER
        }

        var isOut =
            dateTime.isOutOfRange(MONTH, selectable, null, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.JULY
        }

        isOut =
            dateTime.isOutOfRange(MONTH, selectable, null, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_monthIndex_withSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val max = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.JULY
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.OCTOBER
        }
        max.apply {
            year = 1990
            month = Calendar.MAY
        }

        dateTime.apply {
            year = 1990
            month = Calendar.DECEMBER
        }

        var isOut =
            dateTime.isOutOfRange(MONTH, selectable, null, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.JULY
        }

        isOut =
            dateTime.isOutOfRange(MONTH, selectable, null, max)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_monthIndex_withSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.JULY
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.OCTOBER
        }
        min.apply {
            year = 1990
            month = Calendar.MAY
        }

        dateTime.apply {
            year = 1990
            month = Calendar.DECEMBER
        }

        var isOut =
            dateTime.isOutOfRange(MONTH, selectable, min, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
        }

        isOut =
            dateTime.isOutOfRange(MONTH, selectable, min, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_monthIndex_withSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.JULY
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.OCTOBER
        }

        min.apply {
            year = 1990
            month = Calendar.JULY
        }
        max.apply {
            year = 1990
            month = Calendar.OCTOBER
        }

        dateTime.apply {
            year = 1990
            month = Calendar.DECEMBER
        }

        var isOut =
            dateTime.isOutOfRange(MONTH, selectable, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.AUGUST
        }

        isOut =
            dateTime.isOutOfRange(MONTH, selectable, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.JULY
        }

        isOut =
            dateTime.isOutOfRange(MONTH, selectable, min, max)
        assertFalse(isOut)
    }

    //DAY

    @Test
    fun isOutOfRange_dayIndex_withoutSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        dateTime.apply {
            year = 1995
            month = Calendar.JANUARY
            day = 1
        }

        val isOut =
            dateTime.isOutOfRange(DAY, null, null, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_dayIndex_withoutSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
        }

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 1
        }

        var isOut =
            dateTime.isOutOfRange(DAY, null, null, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
        }

        isOut =
            dateTime.isOutOfRange(DAY, null, null, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 20
        }

        isOut =
            dateTime.isOutOfRange(DAY, null, null, max)
        assertTrue(isOut)
    }

    @Test
    fun isOutOfRange_dayIndex_withoutSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
        }

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 1
        }

        var isOut =
            dateTime.isOutOfRange(DAY, null, min, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
        }

        isOut =
            dateTime.isOutOfRange(DAY, null, min, null)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 20
        }

        isOut =
            dateTime.isOutOfRange(DAY, null, min, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_dayIndex_withoutSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1990
            month = Calendar.MARCH
            day = 10
        }
        max.apply {
            year = 1990
            month = Calendar.MARCH
            day = 20
        }

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 1
        }

        var isOut = dateTime.isOutOfRange(DAY, null, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 10
        }

        isOut =
            dateTime.isOutOfRange(DAY, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
        }

        isOut =
            dateTime.isOutOfRange(DAY, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 20
        }

        isOut =
            dateTime.isOutOfRange(DAY, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 25
        }

        isOut =
            dateTime.isOutOfRange(DAY, null, min, max)
        assertTrue(isOut)
    }

    @Test
    fun isOutOfRange_dayIndex_withSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 10
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 20
        }

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 25
        }

        var isOut =
            dateTime.isOutOfRange(DAY, selectable, null, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
        }

        isOut =
            dateTime.isOutOfRange(DAY, selectable, null, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_dayIndex_withSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val max = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 10
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 20
        }
        max.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 14
        }

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 25
        }

        var isOut =
            dateTime.isOutOfRange(DAY, selectable, null, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
        }

        isOut =
            dateTime.isOutOfRange(DAY, selectable, null, max)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_dayIndex_withSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 10
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 20
        }
        min.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 14
        }

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 25
        }

        var isOut =
            dateTime.isOutOfRange(DAY, selectable, min, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 10
        }

        isOut =
            dateTime.isOutOfRange(DAY, selectable, min, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_dayIndex_withSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 10
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.JULY
            day = 15
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.OCTOBER
            day = 20
        }

        min.apply {
            year = 1990
            month = Calendar.JULY
            day = 15
        }
        max.apply {
            year = 1990
            month = Calendar.OCTOBER
            day = 20
        }

        dateTime.apply {
            year = 1990
            month = Calendar.DECEMBER
            day = 25
        }

        var isOut =
            dateTime.isOutOfRange(DAY, selectable, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.AUGUST
            day = 17
        }

        isOut =
            dateTime.isOutOfRange(DAY, selectable, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.JULY
            day = 15
        }

        isOut =
            dateTime.isOutOfRange(DAY, selectable, min, max)
        assertFalse(isOut)
    }

    //HOUR

    @Test
    fun isOutOfRange_hourIndex_withoutSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        dateTime.apply {
            year = 1995
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
        }

        val isOut =
            dateTime.isOutOfRange(HOUR, null, null, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_hourIndex_withoutSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
        }

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 1
        }

        var isOut =
            dateTime.isOutOfRange(HOUR, null, null, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
        }

        isOut =
            dateTime.isOutOfRange(HOUR, null, null, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 20
        }

        isOut =
            dateTime.isOutOfRange(HOUR, null, null, max)
        assertTrue(isOut)
    }

    @Test
    fun isOutOfRange_hourIndex_withoutSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
        }

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 1
        }

        var isOut =
            dateTime.isOutOfRange(HOUR, null, min, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
        }

        isOut =
            dateTime.isOutOfRange(HOUR, null, min, null)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 20
        }

        isOut =
            dateTime.isOutOfRange(HOUR, null, min, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_hourIndex_withoutSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 10
        }
        max.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 20
        }

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 1
        }

        var isOut =
            dateTime.isOutOfRange(HOUR, null, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 10
        }

        isOut =
            dateTime.isOutOfRange(HOUR, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 15
        }

        isOut =
            dateTime.isOutOfRange(HOUR, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 20
        }

        isOut =
            dateTime.isOutOfRange(HOUR, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 25
        }

        isOut =
            dateTime.isOutOfRange(HOUR, null, min, max)
        assertTrue(isOut)
    }

    @Test
    fun isOutOfRange_hourIndex_withSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 10
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 20
        }

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 25
        }

        var isOut =
            dateTime.isOutOfRange(HOUR, selectable, null, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
        }

        isOut =
            dateTime.isOutOfRange(HOUR, selectable, null, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_hourIndex_withSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val max = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 10
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 20
        }
        max.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 14
        }

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 25
        }

        var isOut =
            dateTime.isOutOfRange(HOUR, selectable, null, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
        }

        isOut =
            dateTime.isOutOfRange(HOUR, selectable, null, max)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_hourIndex_withSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 10
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 20
        }
        min.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 14
        }

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 25
        }

        var isOut =
            dateTime.isOutOfRange(HOUR, selectable, min, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 10
        }

        isOut =
            dateTime.isOutOfRange(HOUR, selectable, min, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_hourIndex_withSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 10
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.JULY
            day = 15
            hour24 = 15
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.OCTOBER
            day = 15
            hour24 = 20
        }

        min.apply {
            year = 1990
            month = Calendar.JULY
            day = 15
            hour24 = 15
        }
        max.apply {
            year = 1990
            month = Calendar.OCTOBER
            day = 15
            hour24 = 20
        }

        dateTime.apply {
            year = 1990
            month = Calendar.DECEMBER
            day = 15
            hour24 = 25
        }

        var isOut =
            dateTime.isOutOfRange(HOUR, selectable, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.AUGUST
            day = 15
            hour24 = 17
        }

        isOut =
            dateTime.isOutOfRange(HOUR, selectable, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.JULY
            day = 15
            hour24 = 15
        }

        isOut =
            dateTime.isOutOfRange(HOUR, selectable, min, max)
        assertFalse(isOut)
    }

    //MINUTE

    @Test
    fun isOutOfRange_minuteIndex_withoutSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        dateTime.apply {
            year = 1995
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 15
        }

        val isOut =
            dateTime.isOutOfRange(MINUTE, null, null, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_minuteIndex_withoutSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
        }

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 1
        }

        var isOut =
            dateTime.isOutOfRange(MINUTE, null, null, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
        }

        isOut =
            dateTime.isOutOfRange(MINUTE, null, null, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 20
        }

        isOut =
            dateTime.isOutOfRange(MINUTE, null, null, max)
        assertTrue(isOut)
    }

    @Test
    fun isOutOfRange_minuteIndex_withoutSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
        }

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 1
        }

        var isOut =
            dateTime.isOutOfRange(MINUTE, null, min, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
        }

        isOut =
            dateTime.isOutOfRange(MINUTE, null, min, null)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 20
        }

        isOut =
            dateTime.isOutOfRange(MINUTE, null, min, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_minuteIndex_withoutSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 10
        }
        max.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 20
        }

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 1
        }

        var isOut =
            dateTime.isOutOfRange(MINUTE, null, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 10
        }

        isOut =
            dateTime.isOutOfRange(MINUTE, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
        }

        isOut =
            dateTime.isOutOfRange(MINUTE, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 20
        }

        isOut =
            dateTime.isOutOfRange(MINUTE, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 25
        }

        isOut =
            dateTime.isOutOfRange(MINUTE, null, min, max)
        assertTrue(isOut)
    }

    @Test
    fun isOutOfRange_minuteIndex_withSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 10
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 20
        }

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 25
        }

        var isOut =
            dateTime.isOutOfRange(MINUTE, selectable, null, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
        }

        isOut =
            dateTime.isOutOfRange(MINUTE, selectable, null, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_minuteIndex_withSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val max = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 10
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 20
        }
        max.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 14
        }

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 25
        }

        var isOut =
            dateTime.isOutOfRange(MINUTE, selectable, null, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
        }

        isOut =
            dateTime.isOutOfRange(MINUTE, selectable, null, max)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_minuteIndex_withSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 10
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 20
        }
        min.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 14
        }

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 25
        }

        var isOut =
            dateTime.isOutOfRange(MINUTE, selectable, min, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 10
        }

        isOut =
            dateTime.isOutOfRange(MINUTE, selectable, min, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_minuteIndex_withSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 10
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.JULY
            day = 15
            hour24 = 15
            minute = 15
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.OCTOBER
            day = 15
            hour24 = 15
            minute = 20
        }

        min.apply {
            year = 1990
            month = Calendar.JULY
            day = 15
            hour24 = 15
            minute = 15
        }
        max.apply {
            year = 1990
            month = Calendar.OCTOBER
            day = 15
            hour24 = 15
            minute = 20
        }

        dateTime.apply {
            year = 1990
            month = Calendar.DECEMBER
            day = 15
            hour24 = 15
            minute = 25
        }

        var isOut =
            dateTime.isOutOfRange(MINUTE, selectable, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.AUGUST
            day = 15
            hour24 = 15
            minute = 17
        }

        isOut =
            dateTime.isOutOfRange(MINUTE, selectable, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.JULY
            day = 15
            hour24 = 15
            minute = 15
        }

        isOut =
            dateTime.isOutOfRange(MINUTE, selectable, min, max)
        assertFalse(isOut)
    }


    //SECOND

    @Test
    fun isOutOfRange_secondIndex_withoutSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        dateTime.apply {
            year = 1995
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 15
            second = 15
        }

        val isOut =
            dateTime.isOutOfRange(SECOND, null, null, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_secondIndex_withoutSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
            second = 15
        }

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
            second = 1
        }

        var isOut =
            dateTime.isOutOfRange(SECOND, null, null, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
            second = 15
        }

        isOut =
            dateTime.isOutOfRange(SECOND, null, null, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
            second = 20
        }

        isOut =
            dateTime.isOutOfRange(SECOND, null, null, max)
        assertTrue(isOut)
    }

    @Test
    fun isOutOfRange_secondIndex_withoutSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
            second = 15
        }

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
            second = 1
        }

        var isOut =
            dateTime.isOutOfRange(SECOND, null, min, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
            second = 15
        }

        isOut =
            dateTime.isOutOfRange(SECOND, null, min, null)
        assertFalse(isOut)

        dateTime.apply {
            year = 1995
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
            second = 20
        }

        isOut =
            dateTime.isOutOfRange(SECOND, null, min, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_secondIndex_withoutSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
            second = 10
        }
        max.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
            second = 20
        }

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
            second = 1
        }

        var isOut =
            dateTime.isOutOfRange(SECOND, null, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
            second = 10
        }

        isOut =
            dateTime.isOutOfRange(SECOND, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
            second = 15
        }

        isOut =
            dateTime.isOutOfRange(SECOND, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
            second = 20
        }

        isOut =
            dateTime.isOutOfRange(SECOND, null, min, max)
        assertFalse(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.MARCH
            day = 15
            hour24 = 15
            minute = 15
            second = 25
        }

        isOut =
            dateTime.isOutOfRange(SECOND, null, min, max)
        assertTrue(isOut)
    }

    @Test
    fun isOutOfRange_secondIndex_withSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 10
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 15
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 20
        }

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 25
        }

        var isOut =
            dateTime.isOutOfRange(SECOND, selectable, null, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 15
        }

        isOut =
            dateTime.isOutOfRange(SECOND, selectable, null, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_secondIndex_withSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val max = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 10
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 15
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 20
        }
        max.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 14
        }

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 25
        }

        var isOut =
            dateTime.isOutOfRange(SECOND, selectable, null, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 15
        }

        isOut =
            dateTime.isOutOfRange(SECOND, selectable, null, max)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_secondIndex_withSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 10
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 15
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 20
        }
        min.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 14
        }

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 25
        }

        var isOut =
            dateTime.isOutOfRange(SECOND, selectable, min, null)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 10
        }

        isOut =
            dateTime.isOutOfRange(SECOND, selectable, min, null)
        assertFalse(isOut)
    }

    @Test
    fun isOutOfRange_secondIndex_withSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        selectable[0].apply {
            year = 1990
            month = Calendar.FEBRUARY
            day = 15
            hour24 = 15
            minute = 15
            second = 10
        }
        selectable[1].apply {
            year = 1990
            month = Calendar.JULY
            day = 15
            hour24 = 15
            minute = 15
            second = 15
        }
        selectable[2].apply {
            year = 1990
            month = Calendar.OCTOBER
            day = 15
            hour24 = 15
            minute = 15
            second = 20
        }

        min.apply {
            year = 1990
            month = Calendar.JULY
            day = 15
            hour24 = 15
            minute = 15
            second = 15
        }
        max.apply {
            year = 1990
            month = Calendar.OCTOBER
            day = 15
            hour24 = 15
            minute = 15
            second = 20
        }

        dateTime.apply {
            year = 1990
            month = Calendar.DECEMBER
            day = 15
            hour24 = 15
            minute = 15
            second = 25
        }

        var isOut =
            dateTime.isOutOfRange(SECOND, selectable, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.AUGUST
            day = 15
            hour24 = 15
            minute = 15
            second = 17
        }

        isOut =
            dateTime.isOutOfRange(SECOND, selectable, min, max)
        assertTrue(isOut)

        dateTime.apply {
            year = 1990
            month = Calendar.JULY
            day = 15
            hour24 = 15
            minute = 15
            second = 15
        }

        isOut =
            dateTime.isOutOfRange(SECOND, selectable, min, max)
        assertFalse(isOut)
    }

    //isSelectable

    @Test
    fun year_valueInSelectable_resultIsValid() {
        val selectable: Array<DateTime> = Array(1) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val dateTime = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isSelectable(selectable, YEAR))

        selectable[0].apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isSelectable(selectable, YEAR))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isSelectable(selectable, YEAR))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 16
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isSelectable(selectable, YEAR))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 31
            second = 30
        }
        assertTrue(dateTime.isSelectable(selectable, YEAR))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 31
        }
        assertTrue(dateTime.isSelectable(selectable, YEAR))
    }

    @Test
    fun year_valueNotInSelectable_resultIsValid() {
        val selectable: Array<DateTime> = Array(1) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val dateTime = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1997
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, YEAR))
    }

    @Test
    fun month_valueInSelectable_resultIsValid() {
        val selectable: Array<DateTime> = Array(1) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val dateTime = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isSelectable(selectable, MONTH))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isSelectable(selectable, MONTH))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 16
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isSelectable(selectable, MONTH))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 31
            second = 30
        }
        assertTrue(dateTime.isSelectable(selectable, MONTH))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 31
        }
        assertTrue(dateTime.isSelectable(selectable, MONTH))
    }

    @Test
    fun month_valueNotInSelectable_resultIsValid() {
        val selectable: Array<DateTime> = Array(1) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val dateTime = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1997
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, MONTH))

        selectable[0].apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, MONTH))
    }

    @Test
    fun day_valueInSelectable_resultIsValid() {
        val selectable: Array<DateTime> = Array(1) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val dateTime = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isSelectable(selectable, DAY))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 16
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isSelectable(selectable, DAY))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 31
            second = 30
        }
        assertTrue(dateTime.isSelectable(selectable, DAY))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 31
        }
        assertTrue(dateTime.isSelectable(selectable, DAY))
    }

    @Test
    fun day_valueNotInSelectable_resultIsValid() {
        val selectable: Array<DateTime> = Array(1) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val dateTime = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1997
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, DAY))

        selectable[0].apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, DAY))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, DAY))
    }

    @Test
    fun hour_valueInSelectable_resultIsValid() {
        val selectable: Array<DateTime> = Array(1) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val dateTime = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isSelectable(selectable, HOUR))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 31
            second = 30
        }
        assertTrue(dateTime.isSelectable(selectable, HOUR))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 31
        }
        assertTrue(dateTime.isSelectable(selectable, HOUR))
    }

    @Test
    fun hour_valueNotInSelectable_resultIsValid() {
        val selectable: Array<DateTime> = Array(1) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val dateTime = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1997
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, HOUR))

        selectable[0].apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, HOUR))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, HOUR))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 16
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, HOUR))
    }

    @Test
    fun minute_valueInSelectable_resultIsValid() {
        val selectable: Array<DateTime> = Array(1) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val dateTime = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isSelectable(selectable, MINUTE))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 31
        }
        assertTrue(dateTime.isSelectable(selectable, MINUTE))
    }

    @Test
    fun minute_valueNotInSelectable_resultIsValid() {
        val selectable: Array<DateTime> = Array(1) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val dateTime = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1997
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, MINUTE))

        selectable[0].apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, MINUTE))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, MINUTE))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 16
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, MINUTE))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 31
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, MINUTE))
    }

    @Test
    fun second_valueInSelectable_resultIsValid() {
        val selectable: Array<DateTime> = Array(1) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val dateTime = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isSelectable(selectable, SECOND))
    }

    @Test
    fun second_valueNotInSelectable_resultIsValid() {
        val selectable: Array<DateTime> = Array(1) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val dateTime = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1997
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, SECOND))

        selectable[0].apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, SECOND))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, SECOND))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 16
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, SECOND))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 31
            second = 30
        }
        assertFalse(dateTime.isSelectable(selectable, SECOND))

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 31
        }
        assertFalse(dateTime.isSelectable(selectable, SECOND))
    }

    //isBeforeMin

    @Test
    fun year_valueBeforeMin_resultIsValid() {
        val min = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1995
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, YEAR))
    }

    @Test
    fun year_valueNotBeforeMin_resultIsValid() {
        val min = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, YEAR))

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, YEAR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 4
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, YEAR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 14
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, YEAR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 29
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, YEAR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 29
        }
        assertFalse(dateTime.isBeforeMin(min, YEAR))

        dateTime.apply {
            year = 1997
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, YEAR))
    }

    @Test
    fun month_valueBeforeMin_resultIsValid() {
        val min = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1995
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, MONTH))

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, MONTH))
    }

    @Test
    fun month_valueNotBeforeMin_resultIsValid() {
        val min = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, MONTH))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 4
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, MONTH))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 14
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, MONTH))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 29
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, MONTH))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 29
        }
        assertFalse(dateTime.isBeforeMin(min, MONTH))

        dateTime.apply {
            year = 1997
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.MARCH
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, MONTH))
    }

    @Test
    fun day_valueBeforeMin_resultIsValid() {
        val min = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1995
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, DAY))

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, DAY))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 4
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, DAY))
    }

    @Test
    fun day_valueNotBeforeMin_resultIsValid() {
        val min = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, DAY))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 14
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, DAY))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 29
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, DAY))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 29
        }
        assertFalse(dateTime.isBeforeMin(min, DAY))

        dateTime.apply {
            year = 1997
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.MARCH
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, DAY))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 6
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, DAY))
    }

    @Test
    fun hour_valueBeforeMin_resultIsValid() {
        val min = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1995
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, HOUR))

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, HOUR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 4
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, HOUR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 14
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, HOUR))
    }

    @Test
    fun hour_valueNotBeforeMin_resultIsValid() {
        val min = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, HOUR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 29
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, HOUR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 29
        }
        assertFalse(dateTime.isBeforeMin(min, HOUR))

        dateTime.apply {
            year = 1997
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.MARCH
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, HOUR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 6
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, HOUR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 16
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, HOUR))
    }

    @Test
    fun minute_valueBeforeMin_resultIsValid() {
        val min = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1995
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 4
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 14
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 29
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, MINUTE))
    }

    @Test
    fun minute_valueNotBeforeMin_resultIsValid() {
        val min = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 29
        }
        assertFalse(dateTime.isBeforeMin(min, MINUTE))

        dateTime.apply {
            year = 1997
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.MARCH
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 6
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 16
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 31
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, MINUTE))
    }

    @Test
    fun second_valueBeforeMin_resultIsValid() {
        val min = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1995
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 4
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 14
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 29
            second = 30
        }
        assertTrue(dateTime.isBeforeMin(min, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 29
        }
        assertTrue(dateTime.isBeforeMin(min, SECOND))
    }

    @Test
    fun second_valueNotBeforeMin_resultIsValid() {
        val min = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, SECOND))

        dateTime.apply {
            year = 1997
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.MARCH
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 6
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 16
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 31
            second = 30
        }
        assertFalse(dateTime.isBeforeMin(min, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 31
        }
        assertFalse(dateTime.isBeforeMin(min, SECOND))
    }

    //isAfterMax

    @Test
    fun year_valueAfterMax_resultIsValid() {
        val max = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1997
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, YEAR))
    }

    @Test
    fun year_valueNotAfterMax_resultIsValid() {
        val max = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        assertFalse(dateTime.isAfterMax(max, YEAR))

        dateTime.apply {
            year = 1996
            month = Calendar.MARCH
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, YEAR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 6
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, YEAR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 16
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, YEAR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 31
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, YEAR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 31
        }
        assertFalse(dateTime.isAfterMax(max, YEAR))

        dateTime.apply {
            year = 1995
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, YEAR))
    }

    @Test
    fun month_valueAfterMax_resultIsValid() {
        val max = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1997
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, MONTH))

        dateTime.apply {
            year = 1996
            month = Calendar.MARCH
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, MONTH))
    }

    @Test
    fun month_valueNotAfterMax_resultIsValid() {
        val max = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, MONTH))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 6
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, MONTH))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 16
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, MONTH))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 31
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, MONTH))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 31
        }
        assertFalse(dateTime.isAfterMax(max, MONTH))

        dateTime.apply {
            year = 1995
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, MONTH))

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, MONTH))
    }

    @Test
    fun day_valueAfterMax_resultIsValid() {
        val max = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1997
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, DAY))

        dateTime.apply {
            year = 1996
            month = Calendar.MARCH
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, DAY))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 6
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, DAY))
    }

    @Test
    fun day_valueNotAfterMax_resultIsValid() {
        val max = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, DAY))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 16
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, DAY))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 31
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, DAY))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 31
        }
        assertFalse(dateTime.isAfterMax(max, DAY))

        dateTime.apply {
            year = 1995
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, DAY))

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, DAY))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 4
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, DAY))
    }

    @Test
    fun hour_valueAfterMax_resultIsValid() {
        val max = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1997
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, HOUR))

        dateTime.apply {
            year = 1996
            month = Calendar.MARCH
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, HOUR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 6
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, HOUR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 16
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, HOUR))
    }

    @Test
    fun hour_valueNotAfterMax_resultIsValid() {
        val max = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, HOUR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 31
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, HOUR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 31
        }
        assertFalse(dateTime.isAfterMax(max, HOUR))

        dateTime.apply {
            year = 1995
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, HOUR))

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, HOUR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 4
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, HOUR))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 14
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, HOUR))
    }

    @Test
    fun minute_valueAfterMax_resultIsValid() {
        val max = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1997
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.MARCH
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 6
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 16
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 31
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, MINUTE))
    }

    @Test
    fun minute_valueNotAfterMax_resultIsValid() {
        val max = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 31
        }
        assertFalse(dateTime.isAfterMax(max, MINUTE))

        dateTime.apply {
            year = 1995
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 4
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 14
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, MINUTE))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 29
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, MINUTE))
    }

    @Test
    fun second_valueAfterMax_resultIsValid() {
        val max = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1997
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.MARCH
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 6
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 16
            minute = 30
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 31
            second = 30
        }
        assertTrue(dateTime.isAfterMax(max, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 31
        }
        assertTrue(dateTime.isAfterMax(max, SECOND))
    }

    @Test
    fun second_valueNotAfterMax_resultIsValid() {
        val max = DateTime(System.currentTimeMillis())
        val dateTime = DateTime(System.currentTimeMillis())

        max.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, SECOND))

        dateTime.apply {
            year = 1995
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 4
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 14
            minute = 30
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 29
            second = 30
        }
        assertFalse(dateTime.isAfterMax(max, SECOND))

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 5
            hour24 = 15
            minute = 30
            second = 29
        }
        assertFalse(dateTime.isAfterMax(max, SECOND))
    }

    //isAmPmDisable

    @Test
    fun withoutSelectable_withoutMin_withoutMax_amIsAble_pmIsAble() {
        val dateTime = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        assertFalse(
            dateTime.isAmPmDisable(Calendar.AM, null, null, null)
        )
        assertFalse(
            dateTime.isAmPmDisable(Calendar.PM, null, null, null)
        )
    }

    @Test
    fun withSelectable_withoutMin_withoutMax_isAmPmDisableIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(1) {
            DateTime(
                System.currentTimeMillis()
            )
        }

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(
            dateTime.isAmPmDisable(Calendar.AM, selectable, null, null)
        )
        assertTrue(
            dateTime.isAmPmDisable(Calendar.PM, selectable, null, null)
        )

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 5
            minute = 30
            second = 30
        }
        assertFalse(
            dateTime.isAmPmDisable(Calendar.AM, selectable, null, null)
        )
        assertTrue(
            dateTime.isAmPmDisable(Calendar.PM, selectable, null, null)
        )

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(
            dateTime.isAmPmDisable(Calendar.AM, selectable, null, null)
        )
        assertFalse(
            dateTime.isAmPmDisable(Calendar.PM, selectable, null, null)
        )
    }

    @Test
    fun withoutSelectable_withMin_withoutMax_isAmPmDisableIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(
            dateTime.isAmPmDisable(Calendar.AM, null, min, null)
        )
        assertFalse(
            dateTime.isAmPmDisable(Calendar.PM, null, min, null)
        )

        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 5
            minute = 30
            second = 30
        }
        assertFalse(
            dateTime.isAmPmDisable(Calendar.AM, null, min, null)
        )
        assertFalse(
            dateTime.isAmPmDisable(Calendar.PM, null, min, null)
        )

        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(
            dateTime.isAmPmDisable(Calendar.AM, null, min, null)
        )
        assertFalse(
            dateTime.isAmPmDisable(Calendar.PM, null, min, null)
        )
    }

    @Test
    fun withoutSelectable_withoutMin_withMax_isAmPmDisableIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(
            dateTime.isAmPmDisable(Calendar.AM, null, null, max)
        )
        assertFalse(
            dateTime.isAmPmDisable(Calendar.PM, null, null, max)
        )

        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 5
            minute = 30
            second = 30
        }
        assertFalse(
            dateTime.isAmPmDisable(Calendar.AM, null, null, max)
        )
        assertTrue(
            dateTime.isAmPmDisable(Calendar.PM, null, null, max)
        )

        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(
            dateTime.isAmPmDisable(Calendar.AM, null, null, max)
        )
        assertFalse(
            dateTime.isAmPmDisable(Calendar.PM, null, null, max)
        )
    }

    @Test
    fun withoutSelectable_withMin_withMax_isAmPmDisableIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 15
            minute = 30
            second = 30
        }

        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 3
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(
            dateTime.isAmPmDisable(Calendar.AM, null, min, max)
        )
        assertFalse(
            dateTime.isAmPmDisable(Calendar.PM, null, min, max)
        )

        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 5
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 5
            minute = 30
            second = 30
        }
        assertFalse(
            dateTime.isAmPmDisable(Calendar.AM, null, min, max)
        )
        assertTrue(
            dateTime.isAmPmDisable(Calendar.PM, null, min, max)
        )

        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 5
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(
            dateTime.isAmPmDisable(Calendar.AM, null, min, max)
        )
        assertFalse(
            dateTime.isAmPmDisable(Calendar.PM, null, min, max)
        )

        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 15
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(
            dateTime.isAmPmDisable(Calendar.AM, null, min, max)
        )
        assertFalse(
            dateTime.isAmPmDisable(Calendar.PM, null, min, max)
        )
    }

    @Test
    fun withSelectable_withMin_withMax_isAmPmDisableIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(2) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val max = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 15
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 5
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertTrue(
            dateTime.isAmPmDisable(Calendar.AM, selectable, min, max)
        )
        assertTrue(
            dateTime.isAmPmDisable(Calendar.PM, selectable, min, max)
        )

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 5
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        assertFalse(
            dateTime.isAmPmDisable(Calendar.AM, selectable, min, max)
        )
        assertTrue(
            dateTime.isAmPmDisable(Calendar.PM, selectable, min, max)
        )

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 5
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 5
            minute = 30
            second = 30
        }
        assertTrue(
            dateTime.isAmPmDisable(Calendar.AM, selectable, min, max)
        )
        assertFalse(
            dateTime.isAmPmDisable(Calendar.PM, selectable, min, max)
        )

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 5
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 5
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 5
            minute = 30
            second = 30
        }
        assertFalse(
            dateTime.isAmPmDisable(Calendar.AM, selectable, min, max)
        )
        assertFalse(
            dateTime.isAmPmDisable(Calendar.PM, selectable, min, max)
        )

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 5
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 5
            minute = 30
            second = 30
        }
        assertFalse(
            dateTime.isAmPmDisable(Calendar.AM, selectable, min, max)
        )
        assertFalse(
            dateTime.isAmPmDisable(Calendar.PM, selectable, min, max)
        )
    }

    //roundToNearest

    //NOT_SET

    @Test
    fun roundToNearest_notSetIndex_withoutSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, null, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(15, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_notSetIndex_withSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        //selectable contain dateTime -> result = dateTime
        selectable[0].apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1996
            month = Calendar.OCTOBER
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 1995
            month = Calendar.DECEMBER
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, selectable, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //nearest is previous year -> result chose from all selectable
        selectable[0].apply {
            year = 1996
            month = Calendar.AUGUST
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1996
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 1995
            month = Calendar.DECEMBER
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, selectable, null, null)

        assertEquals(1995, dateTime.year)
        assertEquals(Calendar.DECEMBER, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_notSetIndex_withSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        //min is after dateTime, selectable contains dateTime -> result = dateTime
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, selectable, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //min is after dateTime -> result chose from selectable
        selectable[0].apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.AUGUST
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, selectable, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.MARCH, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_notSetIndex_withSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val max = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        //max is before dateTime, selectable contains dateTime -> result = dateTime
        selectable[0].apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, selectable, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //max is before dateTime -> result chose from selectable
        selectable[0].apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, selectable, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.MARCH, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_notSetIndex_withoutSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())

        //dateTime after min -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime = min -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime before min -> result = min
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime before min, min is next year -> result = min
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1997
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, null, min, null)

        assertEquals(1997, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_notSetIndex_withoutSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        //dateTime before max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime = max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime after max -> result = max
        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime after max, max is previous month -> result = max
        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1995
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, null, null, max)

        assertEquals(1995, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_notSetIndex_withoutSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        //dateTime = min = max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //min < dateTime < max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime < min, max -> result = min
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime > min, max -> result = max
        dateTime.apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(NOT_SET, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    //MONTH

    @Test
    fun roundToNearest_monthIndex_withoutSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, null, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(15, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_monthIndex_withSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        //selectable contain dateTime -> result = dateTime
        selectable[0].apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1996
            month = Calendar.OCTOBER
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 1995
            month = Calendar.DECEMBER
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, selectable, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //nearest is previous year -> result chose from current year
        selectable[0].apply {
            year = 1996
            month = Calendar.AUGUST
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1996
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 1995
            month = Calendar.DECEMBER
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, selectable, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.AUGUST, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_monthIndex_withSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        //min is after dateTime, selectable contains dateTime -> result = dateTime
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, selectable, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //min is after dateTime -> result chose from selectable
        selectable[0].apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.AUGUST
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, selectable, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.MARCH, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_monthIndex_withSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val max = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        //max is before dateTime, selectable contains dateTime -> result = dateTime
        selectable[0].apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, selectable, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //max is before dateTime -> result chose from selectable
        selectable[0].apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, selectable, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.MARCH, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_monthIndex_withoutSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())

        //dateTime after min -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime = min -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime before min -> result = min
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime before min, min is next year -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1997
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_monthIndex_withoutSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        //dateTime before max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime = max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime after max -> result = max
        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime after max, max is previous month -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1995
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_monthIndex_withoutSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        //dateTime = min = max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //min < dateTime < max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime < min, max -> result = min
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime > min, max -> result = max
        dateTime.apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MONTH, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    //DAY

    @Test
    fun roundToNearest_dayIndex_withoutSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, null, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(15, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_dayIndex_withSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }

        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        //selectable contain dateTime -> result = dateTime
        selectable[0].apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 16
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 31
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, selectable, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //nearest is previous month -> result chose from current month
        selectable[0].apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 25
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 16
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 31
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, selectable, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(16, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_dayIndex_withSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        //min is after dateTime, selectable contains dateTime -> result = dateTime
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 3
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, selectable, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //min is after dateTime -> result chose from selectable
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 3
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, selectable, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_dayIndex_withSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val max = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        //max is before dateTime, selectable contains dateTime -> result = dateTime
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, selectable, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //max is before dateTime -> result chose from selectable
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 16
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, selectable, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(16, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_dayIndex_withoutSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())

        //dateTime after min -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime = min -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime before min -> result = min
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime before min, min is next month -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 3
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_dayIndex_withoutSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        //dateTime before max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime = max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime after max -> result = max
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 3
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime after max, max is previous month -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.FEBRUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.FEBRUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_dayIndex_withoutSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        //dateTime = min = max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //min < dateTime < max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 3
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime < min, max -> result = min
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 3
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 4
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(3, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime > min, max -> result = max
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 5
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 3
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 4
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(DAY, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(4, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    //HOUR

    @Test
    fun roundToNearest_hourIndex_withoutSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(15, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_hourIndex_withSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        //selectable contain dateTime -> result = dateTime
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 16
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 23
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, selectable, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //nearest is previous day -> result chose from current day
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 23
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 16
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 23
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, selectable, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(16, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_hourIndex_withSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        //min is after dateTime, selectable contains dateTime -> result = dateTime
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 23
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, selectable, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //min is after dateTime -> result chose from selectable
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 22
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 23
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, selectable, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(22, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_hourIndex_withSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val max = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        //max is before dateTime, selectable contains dateTime -> result = dateTime
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 0
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, selectable, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //max is before dateTime -> result chose from selectable
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 22
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 0
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, selectable, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(22, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_hourIndex_withoutSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())

        //dateTime after min -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime = min -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime before min -> result = min
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 3
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(3, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime before min, min is next day -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 3
            hour24 = 3
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_hourIndex_withoutSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        //dateTime before max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 3
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime = max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime after max -> result = max
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime after max, max is previous day -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_hourIndex_withoutSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        //dateTime = min = max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //min < dateTime < max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 3
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime < min, max -> result = min
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 3
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 4
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(3, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime > min, max -> result = max
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 0
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun bug_roundToNearest_minIsPreviousDay_afterMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 25
            hour24 = 19
            minute = 0
            second = 0
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 23
            hour24 = 19
            minute = 0
            second = 0
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 25
            hour24 = 9
            minute = 0
            second = 0
        }

        dateTime.roundToNearest(HOUR, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(25, dateTime.day)
        assertEquals(9, dateTime.hour24)
        assertEquals(0, dateTime.minute)
        assertEquals(0, dateTime.second)
    }

    @Test
    fun roundToNearest_hourIndex_checkPartOfDay_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, null, null, true)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(15, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_hourIndex_checkPartOfDay_withSelectable_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 11
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 23
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 13
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, selectable, null, null, true)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_hourIndex_checkPartOfDay_withMin_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 3
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, min, null, true)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(3, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 13
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, min, null, true)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_hourIndex_checkPartOfDay_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 2
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, null, max, true)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 14
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(HOUR, null, null, max, true)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(14, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    //MINUTE

    @Test
    fun roundToNearest_minuteIndex_withoutSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MINUTE, null, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(15, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_minuteIndex_withSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }

        //selectable contain dateTime -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 16
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 23
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MINUTE, selectable, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //nearest is previous hour -> result chose from current hour
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 0
        }

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 45
            second = 30
        }
        selectable[2].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 0
            minute = 59
            second = 30
        }

        dateTime.roundToNearest(MINUTE, selectable, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_minuteIndex_withSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        //min is after dateTime, selectable contains dateTime -> result = dateTime
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 40
            second = 30
        }

        dateTime.roundToNearest(MINUTE, selectable, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //min is after dateTime -> result chose from selectable
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 20
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 40
            second = 30
        }

        dateTime.roundToNearest(MINUTE, selectable, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(20, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_minuteIndex_withSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val max = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        //max is before dateTime, selectable contains dateTime -> result = dateTime
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 20
            second = 30
        }

        dateTime.roundToNearest(MINUTE, selectable, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //max is before dateTime -> result chose from selectable
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 40
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 20
            second = 30
        }

        dateTime.roundToNearest(MINUTE, selectable, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(40, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_minuteIndex_withoutSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())

        //dateTime after min -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 40
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MINUTE, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(40, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime = min -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MINUTE, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime before min -> result = min
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 20
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MINUTE, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime before min, min is next hour -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 20
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 3
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MINUTE, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(20, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_minuteIndex_withoutSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        //dateTime before max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 1
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 2
            second = 30
        }

        dateTime.roundToNearest(MINUTE, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(1, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime = max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MINUTE, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime after max -> result = max
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 40
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MINUTE, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime after max, max is previous hour -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 40
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MINUTE, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(40, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_minuteIndex_withoutSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        //dateTime = min = max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(MINUTE, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //min < dateTime < max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 20
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 40
            second = 30
        }

        dateTime.roundToNearest(MINUTE, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime < min, max -> result = min
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 40
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 50
            second = 30
        }

        dateTime.roundToNearest(MINUTE, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(40, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime > min, max -> result = max
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 10
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 20
            second = 30
        }

        dateTime.roundToNearest(MINUTE, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(20, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    //SECOND

    @Test
    fun roundToNearest_secondIndex_withoutSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 15
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(SECOND, null, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(1, dateTime.day)
        assertEquals(15, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_secondIndex_withSelectable_withoutMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }

        //selectable contain dateTime -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 25
        }
        selectable[2].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 35
        }

        dateTime.roundToNearest(SECOND, selectable, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //nearest is previous minute -> result chose from current minute
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 0
        }

        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 30
        }
        selectable[1].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 59
        }
        selectable[2].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 0
            minute = 0
            second = 59
        }

        dateTime.roundToNearest(SECOND, selectable, null, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(1, dateTime.minute)
        assertEquals(30, dateTime.second)
    }

    @Test
    fun roundToNearest_secondIndex_withSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val min = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 30
        }

        //second is after dateTime, selectable contains dateTime -> result = dateTime
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 40
        }

        dateTime.roundToNearest(SECOND, selectable, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(1, dateTime.minute)
        assertEquals(30, dateTime.second)

        //second is after dateTime -> result chose from selectable
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 20
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 1
            second = 40
        }

        dateTime.roundToNearest(SECOND, selectable, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(1, dateTime.minute)
        assertEquals(20, dateTime.second)
    }

    @Test
    fun roundToNearest_secondIndex_withSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        val max = DateTime(System.currentTimeMillis())

        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        //max is before dateTime, selectable contains dateTime -> result = dateTime
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 20
        }

        dateTime.roundToNearest(SECOND, selectable, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //max is before dateTime -> result chose from selectable
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 40
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 20
        }

        dateTime.roundToNearest(MINUTE, selectable, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(40, dateTime.second)
    }

    @Test
    fun roundToNearest_secondIndex_withoutSelectable_withMin_withoutMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())

        //dateTime after min -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 40
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(SECOND, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(40, dateTime.second)

        //dateTime = min -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(SECOND, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime before min -> result = min
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 20
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(SECOND, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime before min, min is next minute -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 20
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 31
            second = 30
        }

        dateTime.roundToNearest(SECOND, null, min, null)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(20, dateTime.second)
    }

    @Test
    fun roundToNearest_secondIndex_withoutSelectable_withoutMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        //dateTime before max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 2
            second = 29
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 2
            second = 30
        }

        dateTime.roundToNearest(SECOND, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(2, dateTime.minute)
        assertEquals(29, dateTime.second)

        //dateTime = max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(SECOND, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime after max -> result = max
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 40
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(SECOND, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime after max, max is previous minute -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 40
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 29
            second = 30
        }

        dateTime.roundToNearest(SECOND, null, null, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(40, dateTime.second)
    }

    @Test
    fun roundToNearest_secondIndex_withoutSelectable_withMin_withMax_valueIsValid() {
        val dateTime = DateTime(System.currentTimeMillis())
        val min = DateTime(System.currentTimeMillis())
        val max = DateTime(System.currentTimeMillis())

        //dateTime = min = max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }

        dateTime.roundToNearest(SECOND, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //min < dateTime < max -> result = dateTime
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 20
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 1
            minute = 30
            second = 40
        }

        dateTime.roundToNearest(SECOND, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(1, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(30, dateTime.second)

        //dateTime < min, max -> result = min
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 40
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 50
        }

        dateTime.roundToNearest(SECOND, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(40, dateTime.second)

        //dateTime > min, max -> result = max
        dateTime.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 30
        }
        min.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 10
        }
        max.apply {
            year = 1996
            month = Calendar.JANUARY
            day = 2
            hour24 = 2
            minute = 30
            second = 20
        }

        dateTime.roundToNearest(SECOND, null, min, max)

        assertEquals(1996, dateTime.year)
        assertEquals(Calendar.JANUARY, dateTime.month)
        assertEquals(2, dateTime.day)
        assertEquals(2, dateTime.hour24)
        assertEquals(30, dateTime.minute)
        assertEquals(20, dateTime.second)
    }

    //findStartDate

    @Test
    fun findStartDate_withoutSelectable_withoutMin_valueIsValid() {
        val startDate = findStartDate(null, null, 1900)

        assertEquals(1900, startDate.year)
        assertEquals(Calendar.JANUARY, startDate.month)
        assertEquals(1, startDate.day)
    }

    @Test
    fun findStartDate_withoutSelectable_withMin_valueIsValid() {
        val min = DateTime(System.currentTimeMillis())
        min.apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        val startDate = findStartDate(null, min, 1990)

        assertEquals(1996, startDate.year)
        assertEquals(Calendar.MARCH, startDate.month)
        assertEquals(1, startDate.day)
    }

    @Test
    fun findStartDate_withSelectable_withoutMin_valueIsValid() {
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1997
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        Arrays.sort(selectable)

        val startDate = findStartDate(selectable, null, 1990)

        assertEquals(1996, startDate.year)
        assertEquals(Calendar.JANUARY, startDate.month)
        assertEquals(1, startDate.day)
    }

    @Test
    fun findStartDate_withSelectable_withMin_valueIsValid() {
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1997
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        Arrays.sort(selectable)

        val min = DateTime(System.currentTimeMillis())
        min.apply {
            year = 1995
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        val startDate = findStartDate(selectable, min, 1990)

        assertEquals(1996, startDate.year)
        assertEquals(Calendar.JANUARY, startDate.month)
        assertEquals(1, startDate.day)
    }

    //findEndDate

    @Test
    fun findEndDate_withoutSelectable_withoutMax_valueIsValid() {
        val endDate = findEndDate(null, null, 2100)

        assertEquals(2100, endDate.year)
        assertEquals(Calendar.DECEMBER, endDate.month)
        assertEquals(31, endDate.day)
    }

    @Test
    fun findEndDate_withoutSelectable_withMax_valueIsValid() {
        val max = DateTime(System.currentTimeMillis())
        max.apply {
            year = 2026
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        val endDate = findEndDate(null, max, 2100)

        assertEquals(2026, endDate.year)
        assertEquals(Calendar.MARCH, endDate.month)
        assertEquals(1, endDate.day)
    }

    @Test
    fun findEndDate_withSelectable_withoutMax_valueIsValid() {
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        selectable[0].apply {
            year = 2021
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 2022
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 2022
            month = Calendar.AUGUST
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        Arrays.sort(selectable)

        val endDate = findEndDate(selectable, null, 2100)

        assertEquals(2022, endDate.year)
        assertEquals(Calendar.AUGUST, endDate.month)
        assertEquals(1, endDate.day)
    }

    @Test
    fun findEndDate_withSelectable_withMax_valueIsValid() {
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        selectable[0].apply {
            year = 2021
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 2022
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 2022
            month = Calendar.AUGUST
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        Arrays.sort(selectable)

        val max = DateTime(System.currentTimeMillis())
        max.apply {
            year = 2026
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        val endDate = findEndDate(selectable, max, 2100)

        assertEquals(2022, endDate.year)
        assertEquals(Calendar.AUGUST, endDate.month)
        assertEquals(1, endDate.day)
    }

    //findMinYear

    @Test
    fun findMinYear_withoutSelectable_withoutMin_valueIsValid() {
        val minYear = findMinYear(null, null, 1900)

        assertEquals(INVALID, minYear)
    }

    @Test
    fun findMinYear_withoutSelectable_withMin_valueIsValid() {
        val min = DateTime(System.currentTimeMillis())
        min.apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        var minYear = findMinYear(null, min, 1900)

        assertEquals(1996, minYear)

        min.apply {
            year = 1890
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        minYear = findMinYear(null, min, 1900)

        assertEquals(INVALID, minYear)
    }

    @Test
    fun findMinYear_withSelectable_withoutMin_valueIsValid() {
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1997
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        Arrays.sort(selectable)

        val minYear = findMinYear(selectable, null, 1990)

        assertEquals(1996, minYear)
    }

    @Test
    fun findMinYear_withSelectable_withMin_valueIsValid() {
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1997
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        Arrays.sort(selectable)

        val min = DateTime(System.currentTimeMillis())
        min.apply {
            year = 1995
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        val minYear = findMinYear(selectable, min, 1990)

        assertEquals(1996, minYear)
    }

    //findMaxYear

    @Test
    fun findMaxYear_withoutSelectable_withoutMin_valueIsValid() {
        val maxYear = findMaxYear(null, null, 2100)

        assertEquals(INVALID, maxYear)
    }

    @Test
    fun findMaxYear_withoutSelectable_withMax_valueIsValid() {
        val max = DateTime(System.currentTimeMillis())
        max.apply {
            year = 2050
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        var maxYear = findMaxYear(null, max, 2100)

        assertEquals(2050, maxYear)

        max.apply {
            year = 2110
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        maxYear = findMaxYear(null, max, 2100)

        assertEquals(INVALID, maxYear)
    }

    @Test
    fun findMaxYear_withSelectable_withoutMax_valueIsValid() {
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1997
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        Arrays.sort(selectable)

        val maxYear = findMaxYear(selectable, null, 1990)

        assertEquals(1997, maxYear)
    }

    @Test
    fun findMaxYear_withSelectable_withMax_valueIsValid() {
        val selectable: Array<DateTime> = Array(3) {
            DateTime(
                System.currentTimeMillis()
            )
        }
        selectable[0].apply {
            year = 1996
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[1].apply {
            year = 1997
            month = Calendar.JANUARY
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        selectable[2].apply {
            year = 1996
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }
        Arrays.sort(selectable)

        val max = DateTime(System.currentTimeMillis())
        max.apply {
            year = 2020
            month = Calendar.MARCH
            day = 1
            hour24 = 1
            minute = 30
            second = 30
        }

        val maxYear = findMaxYear(selectable, null, 1990)

        assertEquals(1997, maxYear)
    }

    //invalidateDayOfMonth

    @Test
    fun invalidateDayOfMonth_valueIsValid() {
        var day: Int =
            invalidateDayOfMonth(2020, Calendar.FEBRUARY, 31)
        assertEquals(29, day)

        day = invalidateDayOfMonth(2020, Calendar.FEBRUARY, 29)
        assertEquals(29, day)

        day = invalidateDayOfMonth(2020, Calendar.FEBRUARY, 28)
        assertEquals(28, day)

        day = invalidateDayOfMonth(2021, Calendar.FEBRUARY, 29)
        assertEquals(28, day)

        day = invalidateDayOfMonth(2021, Calendar.FEBRUARY, 28)
        assertEquals(28, day)

        day = invalidateDayOfMonth(2021, Calendar.FEBRUARY, 27)
        assertEquals(27, day)
    }
}

