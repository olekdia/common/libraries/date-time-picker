package com.olekdia.datetimepicker

import android.content.Context
import android.os.Build
import androidx.test.core.app.ApplicationProvider
import com.olekdia.common.INVALID
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.common.PickerIndex.Companion.DAY
import com.olekdia.datetimepicker.common.PickerIndex.Companion.HOUR
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MINUTE
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MONTH
import com.olekdia.datetimepicker.common.PickerIndex.Companion.SECOND
import com.olekdia.datetimepicker.common.PickerIndex.Companion.YEAR
import com.olekdia.datetimepicker.common.RangePart.Companion.END
import com.olekdia.datetimepicker.common.RangePart.Companion.START
import com.olekdia.datetimepicker.interfaces.HeaderRangeSelectionListener
import com.olekdia.datetimepicker.interfaces.HeaderSelectionListener
import com.olekdia.datetimepicker.interfaces.SelectionChangeListener
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.annotation.LooperMode
import java.util.*

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.M], manifest = Config.NONE)
@LooperMode(LooperMode.Mode.LEGACY)
class ControllerTest {

    lateinit var context: Context

    lateinit var selectionChangeListener: SelectionChangeListener

    private var isHeaderSelectionListenerCalled: Boolean = false
    private var isHeaderRangeSelectionListenerCalled: Boolean = false

    private var item: Int = INVALID
    private var rangeItem: Int = INVALID
    private var rangePart: Int = INVALID

    lateinit var headerSelectionListener: HeaderSelectionListener
    lateinit var headerRangeSelectionListener: HeaderRangeSelectionListener

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()

        selectionChangeListener = object : SelectionChangeListener {
            override fun onHeaderSelectionChanged(changePicker: Boolean, updatePicker: Boolean) {}
            override fun onAmPmSelectionChanged(updatePicker: Boolean, amPm: Int, part: Int) {}
            override fun onDtSelectionChanged(changePicker: Boolean, updatePicker: Boolean) {}
        }

        headerSelectionListener = object : HeaderSelectionListener {
            override fun onHeaderSelection(index: Int) {
                isHeaderSelectionListenerCalled = true
                item = index
            }
        }
        headerRangeSelectionListener = object : HeaderRangeSelectionListener {
            override fun onHeaderSelection(index: Int, part: Int) {
                isHeaderRangeSelectionListenerCalled = true
                rangeItem = index
                rangePart = part
            }
        }
    }

    //onToggleAmPm

    @Test
    fun onPickAmPm_prevAm_pickAmPm_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(Calendar.AM, controller.currentSelectedDt.amPm)
        assertEquals(8, controller.currentSelectedDt.hour24)

        controller.onToggleAmPm(START)

        assertEquals(Calendar.PM, controller.currentSelectedDt.amPm)
        assertEquals(20, controller.currentSelectedDt.hour24)
    }

    @Test
    fun onPickAmPm_prevPm_pickAmPm_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 20
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(Calendar.PM, controller.currentSelectedDt.amPm)
        assertEquals(20, controller.currentSelectedDt.hour24)

        controller.onToggleAmPm(START)

        assertEquals(Calendar.AM, controller.currentSelectedDt.amPm)
        assertEquals(8, controller.currentSelectedDt.hour24)
    }

    @Test
    fun onPickAmPm_pmDisable_pickAmPm_valueIsValid() {
        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val max = DateTime(System.currentTimeMillis())
        max.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .maxStartDt(max)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(Calendar.AM, controller.currentSelectedDt.amPm)
        assertEquals(8, controller.currentSelectedDt.hour24)

        controller.onToggleAmPm(START)

        assertEquals(Calendar.AM, controller.currentSelectedDt.amPm)
        assertEquals(8, controller.currentSelectedDt.hour24)
    }

    @Test
    fun onPickAmPm_amDisable_pickAmPm_valueIsValid() {
        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 20
            minute = 0
            second = 0
        }

        val min = DateTime(System.currentTimeMillis())
        min.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 20
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .minStartDt(min)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(Calendar.PM, controller.currentSelectedDt.amPm)
        assertEquals(20, controller.currentSelectedDt.hour24)

        controller.onToggleAmPm(START)

        assertEquals(Calendar.PM, controller.currentSelectedDt.amPm)
        assertEquals(20, controller.currentSelectedDt.hour24)
    }

    @Test
    fun onPickAmPm_prevAm_pickAmPm_needToRound_valueIsValid() {
        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val selectable1 = DateTime(System.currentTimeMillis())
        selectable1.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val selectable2 = DateTime(System.currentTimeMillis())
        selectable2.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 21
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectableStartDt(arrayOf(selectable1, selectable2))

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(Calendar.AM, controller.currentSelectedDt.amPm)
        assertEquals(8, controller.currentSelectedDt.hour24)

        controller.onToggleAmPm(START)

        assertEquals(Calendar.PM, controller.currentSelectedDt.amPm)
        assertEquals(21, controller.currentSelectedDt.hour24)
    }

    @Test
    fun onPickAmPm_prevPm_pickAmPm_needToRound_valueIsValid() {
        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 21
            minute = 0
            second = 0
        }

        val selectable1 = DateTime(System.currentTimeMillis())
        selectable1.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val selectable2 = DateTime(System.currentTimeMillis())
        selectable2.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 21
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectableStartDt(arrayOf(selectable1, selectable2))

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(Calendar.PM, controller.currentSelectedDt.amPm)
        assertEquals(21, controller.currentSelectedDt.hour24)

        controller.onToggleAmPm(START)

        assertEquals(Calendar.AM, controller.currentSelectedDt.amPm)
        assertEquals(8, controller.currentSelectedDt.hour24)
    }

    //onYearSelected

    @Test
    fun onYearSelected_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.currentSelectedDt.year)

        controller.onYearSelected(2014)

        assertEquals(2014, controller.currentSelectedDt.year)
    }

    @Test
    fun onYearSelected_needToInvalidateDayOfMonth_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2020
            month = Calendar.FEBRUARY
            day = 29
            hour24 = 8
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2020, controller.currentSelectedDt.year)
        assertEquals(29, controller.currentSelectedDt.day)

        controller.onYearSelected(2021)

        assertEquals(2021, controller.currentSelectedDt.year)
        assertEquals(28, controller.currentSelectedDt.day)
    }

    @Test
    fun onYearSelected_needToRound_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val selectable1 = DateTime(System.currentTimeMillis())
        selectable1.apply {
            year = 2013
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val selectable2 = DateTime(System.currentTimeMillis())
        selectable2.apply {
            year = 2020
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectableStartDt(arrayOf(selectable1, selectable2))

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.currentSelectedDt.year)
        assertEquals(Calendar.FEBRUARY, controller.currentSelectedDt.month)

        controller.onYearSelected(2020)

        assertEquals(2020, controller.currentSelectedDt.year)
        assertEquals(Calendar.SEPTEMBER, controller.currentSelectedDt.month)
    }

    //onMonthSelected

    @Test
    fun onMonthSelected_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.currentSelectedDt.year)
        assertEquals(Calendar.SEPTEMBER, controller.currentSelectedDt.month)

        controller.onMonthSelected(2015, Calendar.NOVEMBER)

        assertEquals(2015, controller.currentSelectedDt.year)
        assertEquals(Calendar.NOVEMBER, controller.currentSelectedDt.month)
    }

    @Test
    fun onMonthSelected_needToInvalidateDayOfMonth_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2020
            month = Calendar.JANUARY
            day = 31
            hour24 = 8
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2020, controller.currentSelectedDt.year)
        assertEquals(Calendar.JANUARY, controller.currentSelectedDt.month)
        assertEquals(31, controller.currentSelectedDt.day)

        controller.onMonthSelected(2020, Calendar.FEBRUARY)

        assertEquals(2020, controller.currentSelectedDt.year)
        assertEquals(Calendar.FEBRUARY, controller.currentSelectedDt.month)
        assertEquals(29, controller.currentSelectedDt.day)
    }

    @Test
    fun onMonthSelected_needToRound_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val selectable1 = DateTime(System.currentTimeMillis())
        selectable1.apply {
            year = 2013
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val selectable2 = DateTime(System.currentTimeMillis())
        selectable2.apply {
            year = 2013
            month = Calendar.MARCH
            day = 2
            hour24 = 8
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectableStartDt(arrayOf(selectable1, selectable2))

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.currentSelectedDt.year)
        assertEquals(Calendar.FEBRUARY, controller.currentSelectedDt.month)
        assertEquals(1, controller.currentSelectedDt.day)

        controller.onMonthSelected(2013, Calendar.MARCH)

        assertEquals(2013, controller.currentSelectedDt.year)
        assertEquals(Calendar.MARCH, controller.currentSelectedDt.month)
        assertEquals(2, controller.currentSelectedDt.day)
    }

    @Test
    fun selectedDt_initial_correctSelected() {
        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDate(year = 2020, month = Calendar.DECEMBER)

        Controller(context, builder, selectionChangeListener).let { controller ->
            assertEquals(2020, controller.currentSelectedDt.year)
            assertEquals(Calendar.DECEMBER, controller.currentSelectedDt.month)
        }

        builder.selectedStartDate(year = 2020, month = Calendar.NOVEMBER)
        Controller(context, builder, selectionChangeListener).let { controller ->
            assertEquals(2020, controller.currentSelectedDt.year)
            assertEquals(Calendar.NOVEMBER, controller.currentSelectedDt.month)
        }

        builder.selectedStartDate(year = 2020, month = Calendar.OCTOBER)
        Controller(context, builder, selectionChangeListener).let { controller ->
            assertEquals(2020, controller.currentSelectedDt.year)
            assertEquals(Calendar.OCTOBER, controller.currentSelectedDt.month)
        }

        builder.selectedStartDate(year = 2020, month = Calendar.SEPTEMBER)
        Controller(context, builder, selectionChangeListener).let { controller ->
            assertEquals(2020, controller.currentSelectedDt.year)
            assertEquals(Calendar.SEPTEMBER, controller.currentSelectedDt.month)
        }
    }

    //onDaySelected

    @Test
    fun onDaySelected_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.currentSelectedDt.year)
        assertEquals(Calendar.SEPTEMBER, controller.currentSelectedDt.month)
        assertEquals(1, controller.currentSelectedDt.day)

        controller.onDaySelected(2015, Calendar.NOVEMBER, 21)

        assertEquals(2015, controller.currentSelectedDt.year)
        assertEquals(Calendar.NOVEMBER, controller.currentSelectedDt.month)
        assertEquals(21, controller.currentSelectedDt.day)
    }

    @Test
    fun onDaySelected_needToRound_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val selectable1 = DateTime(System.currentTimeMillis())
        selectable1.apply {
            year = 2013
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val selectable2 = DateTime(System.currentTimeMillis())
        selectable2.apply {
            year = 2013
            month = Calendar.FEBRUARY
            day = 2
            hour24 = 18
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectableStartDt(arrayOf(selectable1, selectable2))

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.currentSelectedDt.year)
        assertEquals(Calendar.FEBRUARY, controller.currentSelectedDt.month)
        assertEquals(1, controller.currentSelectedDt.day)
        assertEquals(8, controller.currentSelectedDt.hour24)

        controller.onDaySelected(2013, Calendar.FEBRUARY, 2)

        assertEquals(2013, controller.currentSelectedDt.year)
        assertEquals(Calendar.FEBRUARY, controller.currentSelectedDt.month)
        assertEquals(2, controller.currentSelectedDt.day)
        assertEquals(18, controller.currentSelectedDt.hour24)
    }

    //onTimeSelected

    @Test
    fun onTimeSelected_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 15
            second = 15
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.currentSelectedDt.year)
        assertEquals(Calendar.SEPTEMBER, controller.currentSelectedDt.month)
        assertEquals(1, controller.currentSelectedDt.day)

        assertEquals(8, controller.currentSelectedDt.hour24)
        assertEquals(15, controller.currentSelectedDt.minute)
        assertEquals(15, controller.currentSelectedDt.second)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 18
            minute = 45
            second = 45
        }

        controller.onTimeSelected(newTime, HOUR)

        assertEquals(2013, controller.currentSelectedDt.year)
        assertEquals(Calendar.SEPTEMBER, controller.currentSelectedDt.month)
        assertEquals(1, controller.currentSelectedDt.day)

        assertEquals(18, controller.currentSelectedDt.hour24)
        assertEquals(45, controller.currentSelectedDt.minute)
        assertEquals(45, controller.currentSelectedDt.second)
    }

    @Test
    fun onTimeSelected_hour_needToRound_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val selectable1 = DateTime(System.currentTimeMillis())
        selectable1.apply {
            year = 2013
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val selectable2 = DateTime(System.currentTimeMillis())
        selectable2.apply {
            year = 2013
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 18
            minute = 45
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectableStartDt(arrayOf(selectable1, selectable2))

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.currentSelectedDt.year)
        assertEquals(Calendar.FEBRUARY, controller.currentSelectedDt.month)
        assertEquals(1, controller.currentSelectedDt.day)

        assertEquals(8, controller.currentSelectedDt.hour24)
        assertEquals(0, controller.currentSelectedDt.minute)
        assertEquals(0, controller.currentSelectedDt.second)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 18
            minute = 0
            second = 0
        }

        controller.onTimeSelected(newTime, HOUR)

        assertEquals(2013, controller.currentSelectedDt.year)
        assertEquals(Calendar.FEBRUARY, controller.currentSelectedDt.month)
        assertEquals(1, controller.currentSelectedDt.day)

        assertEquals(18, controller.currentSelectedDt.hour24)
        assertEquals(45, controller.currentSelectedDt.minute)
        assertEquals(0, controller.currentSelectedDt.second)
    }

    @Test
    fun onTimeSelected_minute_needToRound_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val selectable1 = DateTime(System.currentTimeMillis())
        selectable1.apply {
            year = 2013
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val selectable2 = DateTime(System.currentTimeMillis())
        selectable2.apply {
            year = 2013
            month = Calendar.FEBRUARY
            day = 1
            hour24 = 8
            minute = 45
            second = 45
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectableStartDt(arrayOf(selectable1, selectable2))

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.currentSelectedDt.year)
        assertEquals(Calendar.FEBRUARY, controller.currentSelectedDt.month)
        assertEquals(1, controller.currentSelectedDt.day)

        assertEquals(8, controller.currentSelectedDt.hour24)
        assertEquals(0, controller.currentSelectedDt.minute)
        assertEquals(0, controller.currentSelectedDt.second)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 8
            minute = 45
            second = 0
        }

        controller.onTimeSelected(newTime, MINUTE)

        assertEquals(2013, controller.currentSelectedDt.year)
        assertEquals(Calendar.FEBRUARY, controller.currentSelectedDt.month)
        assertEquals(1, controller.currentSelectedDt.day)

        assertEquals(8, controller.currentSelectedDt.hour24)
        assertEquals(45, controller.currentSelectedDt.minute)
        assertEquals(45, controller.currentSelectedDt.second)
    }

    //updateStartToEndDistance

    //new distance valid

    @Test
    fun updateStartToEndDistance_clickAmPm_newDistanceValid_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 22
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        controller.onToggleAmPm(START)

        assertEquals(20, controller.selectedDts[START].hour24)
        assertEquals(22, controller.selectedDts[END].hour24)

        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 10
            minute = 0
            second = 0
        }

        controller.onToggleAmPm(START)

        assertEquals(22, controller.selectedDts[START].hour24)
        assertEquals(22, controller.selectedDts[END].hour24)
    }

    @Test
    fun updateStartToEndDistance_selectYear_newDistanceValid_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2015
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        controller.onYearSelected(2014)

        assertEquals(2014, controller.selectedDts[START].year)
        assertEquals(2015, controller.selectedDts[END].year)

        controller.onYearSelected(2015)

        assertEquals(2015, controller.selectedDts[START].year)
        assertEquals(2015, controller.selectedDts[END].year)
    }

    @Test
    fun updateStartToEndDistance_selectMonth_newDistanceValid_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.DECEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        controller.onMonthSelected(2013, Calendar.OCTOBER)

        assertEquals(Calendar.OCTOBER, controller.selectedDts[START].month)
        assertEquals(Calendar.DECEMBER, controller.selectedDts[END].month)

        controller.onMonthSelected(2013, Calendar.DECEMBER)

        assertEquals(Calendar.DECEMBER, controller.selectedDts[START].month)
        assertEquals(Calendar.DECEMBER, controller.selectedDts[END].month)
    }

    @Test
    fun updateStartToEndDistance_selectDay_newDistanceValid_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 10
            hour24 = 8
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        controller.onDaySelected(2013, Calendar.SEPTEMBER, 5)

        assertEquals(5, controller.selectedDts[START].day)
        assertEquals(10, controller.selectedDts[END].day)

        controller.onDaySelected(2013, Calendar.SEPTEMBER, 10)

        assertEquals(10, controller.selectedDts[START].day)
        assertEquals(10, controller.selectedDts[END].day)
    }

    @Test
    fun updateStartToEndDistance_selectHour_newDistanceValid_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 18
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 12
            minute = 0
            second = 0
        }

        controller.onTimeSelected(newTime, HOUR)

        assertEquals(12, controller.selectedDts[START].hour24)
        assertEquals(18, controller.selectedDts[END].hour24)

        newTime.apply {
            hour24 = 18
            minute = 0
            second = 0
        }

        controller.onTimeSelected(newTime, HOUR)

        assertEquals(18, controller.selectedDts[START].hour24)
        assertEquals(18, controller.selectedDts[END].hour24)
    }

    @Test
    fun updateStartToEndDistance_selectMinute_newDistanceValid_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 30
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 8
            minute = 15
            second = 0
        }

        controller.onTimeSelected(newTime, MINUTE)

        assertEquals(15, controller.selectedDts[START].minute)
        assertEquals(30, controller.selectedDts[END].minute)

        newTime.apply {
            hour24 = 8
            minute = 30
            second = 0
        }

        controller.onTimeSelected(newTime, MINUTE)

        assertEquals(30, controller.selectedDts[START].minute)
        assertEquals(30, controller.selectedDts[END].minute)
    }

    @Test
    fun updateStartToEndDistance_selectSecond_newDistanceValid_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 30
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 8
            minute = 0
            second = 15
        }

        controller.onTimeSelected(newTime, SECOND)

        assertEquals(15, controller.selectedDts[START].second)
        assertEquals(30, controller.selectedDts[END].second)

        newTime.apply {
            hour24 = 8
            minute = 0
            second = 30
        }

        controller.onTimeSelected(newTime, SECOND)

        assertEquals(30, controller.selectedDts[START].second)
        assertEquals(30, controller.selectedDts[END].second)
    }

    //new distance invalid

    @Test
    fun updateStartToEndDistance_clickAmPm_newDistanceInvalid_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 11
            minute = 0
            second = 0
        }

        val oldDistance: Long = end.distance(start)

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        controller.onToggleAmPm(START)

        val newDistance: Long =
            controller.selectedDts[END]
                .distance(controller.selectedDts[START])

        assertEquals(oldDistance, newDistance)
    }

    @Test
    fun updateStartToEndDistance_selectYear_newDistanceInvalid_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2015
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val oldDistance: Long = end.distance(start)

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        controller.onYearSelected(2016)

        val newDistance: Long =
            controller.selectedDts[END]
                .distance(controller.selectedDts[START])

        assertEquals(oldDistance, newDistance)
    }

    @Test
    fun updateStartToEndDistance_selectMonth_newDistanceInvalid_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.OCTOBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val oldDistance: Long = end.distance(start)

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        controller.onMonthSelected(2013, Calendar.NOVEMBER)

        val newDistance: Long =
            controller.selectedDts[END]
                .distance(controller.selectedDts[START])

        assertEquals(oldDistance, newDistance)
    }

    @Test
    fun updateStartToEndDistance_selectDay_newDistanceInvalid_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 10
            hour24 = 8
            minute = 0
            second = 0
        }

        val oldDistance: Long = end.distance(start)

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        controller.onDaySelected(2013, Calendar.SEPTEMBER, 15)

        val newDistance: Long =
            controller.selectedDts[END]
                .distance(controller.selectedDts[START])

        assertEquals(oldDistance, newDistance)
    }

    @Test
    fun updateStartToEndDistance_selectHour_newDistanceInvalid_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 14
            minute = 0
            second = 0
        }

        val oldDistance: Long = end.distance(start)

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 16
            minute = 0
            second = 0
        }

        controller.onTimeSelected(newTime, HOUR)

        val newDistance: Long =
            controller.selectedDts[END]
                .distance(controller.selectedDts[START])

        assertEquals(oldDistance, newDistance)
    }

    @Test
    fun updateStartToEndDistance_selectMinute_newDistanceInvalid_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 15
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 30
            second = 0
        }

        val oldDistance: Long = end.distance(start)

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 8
            minute = 35
            second = 0
        }

        controller.onTimeSelected(newTime, MINUTE)

        val newDistance: Long =
            controller.selectedDts[END]
                .distance(controller.selectedDts[START])

        assertEquals(oldDistance, newDistance)
    }

    @Test
    fun updateStartToEndDistance_selectSecond_newDistanceInvalid_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 15
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 30
        }

        val oldDistance: Long = end.distance(start)

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 8
            minute = 0
            second = 35
        }

        controller.onTimeSelected(newTime, SECOND)

        val newDistance: Long =
            controller.selectedDts[END]
                .distance(controller.selectedDts[START])

        assertEquals(oldDistance, newDistance)
    }


    //need to round

    @Test
    fun updateStartToEndDistance_clickAmPm_newDistanceInvalid_needToRound_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 11
            minute = 0
            second = 0
        }

        val max = DateTime(System.currentTimeMillis())
        max.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 22
            minute = 0
            second = 0
        }

        val oldDistance: Long = end.distance(start)

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)
            .maxEndDt(max)

        val controller = Controller(context, builder, selectionChangeListener)

        controller.onToggleAmPm(START)

        val newDistance: Long =
            controller.selectedDts[END]
                .distance(controller.selectedDts[START])

        val distanceToMax: Long =
            max.distance(controller.selectedDts[START])

        assertNotEquals(oldDistance, newDistance)
        assertEquals(distanceToMax, newDistance)
    }

    @Test
    fun updateStartToEndDistance_selectYear_newDistanceInvalid_needToRound_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2015
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val max = DateTime(System.currentTimeMillis())
        max.apply {
            year = 2020
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 22
            minute = 0
            second = 0
        }

        val oldDistance: Long = end.distance(start)

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)
            .maxEndDt(max)

        val controller = Controller(context, builder, selectionChangeListener)

        controller.onYearSelected(2019)

        val newDistance: Long =
            controller.selectedDts[END]
                .distance(controller.selectedDts[START])

        val distanceToMax: Long =
            max.distance(controller.selectedDts[START])

        assertNotEquals(oldDistance, newDistance)
        assertEquals(distanceToMax, newDistance)
    }

    @Test
    fun updateStartToEndDistance_selectMonth_newDistanceInvalid_needToRound_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.JANUARY
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.APRIL
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val max = DateTime(System.currentTimeMillis())
        max.apply {
            year = 2013
            month = Calendar.JULY
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val oldDistance: Long = end.distance(start)

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)
            .maxEndDt(max)

        val controller = Controller(context, builder, selectionChangeListener)

        controller.onMonthSelected(2013, Calendar.MAY)

        val newDistance: Long =
            controller.selectedDts[END]
                .distance(controller.selectedDts[START])

        val distanceToMax: Long =
            max.distance(controller.selectedDts[START])

        assertNotEquals(oldDistance, newDistance)
        assertEquals(distanceToMax, newDistance)
    }

    @Test
    fun updateStartToEndDistance_selectDay_newDistanceInvalid_needToRound_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 10
            hour24 = 8
            minute = 0
            second = 0
        }

        val max = DateTime(System.currentTimeMillis())
        max.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 20
            hour24 = 8
            minute = 0
            second = 0
        }


        val oldDistance: Long = end.distance(start)

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)
            .maxEndDt(max)

        val controller = Controller(context, builder, selectionChangeListener)

        controller.onDaySelected(2013, Calendar.SEPTEMBER, 15)

        val newDistance: Long =
            controller.selectedDts[END]
                .distance(controller.selectedDts[START])

        val distanceToMax: Long =
            max.distance(controller.selectedDts[START])

        assertNotEquals(oldDistance, newDistance)
        assertEquals(distanceToMax, newDistance)
    }

    @Test
    fun updateStartToEndDistance_selectHour_newDistanceInvalid_needToRound_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 12
            minute = 0
            second = 0
        }

        val max = DateTime(System.currentTimeMillis())
        max.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 16
            minute = 0
            second = 0
        }

        val oldDistance: Long = end.distance(start)

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)
            .maxEndDt(max)

        val controller = Controller(context, builder, selectionChangeListener)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 14
            minute = 0
            second = 0
        }

        controller.onTimeSelected(newTime, HOUR)

        val newDistance: Long =
            controller.selectedDts[END]
                .distance(controller.selectedDts[START])

        val distanceToMax: Long =
            max.distance(controller.selectedDts[START])

        assertNotEquals(oldDistance, newDistance)
        assertEquals(distanceToMax, newDistance)
    }

    @Test
    fun updateStartToEndDistance_selectMinute_newDistanceInvalid_needToRound_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 15
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 30
            second = 0
        }

        val max = DateTime(System.currentTimeMillis())
        max.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 40
            second = 0
        }

        val oldDistance: Long = end.distance(start)

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)
            .maxEndDt(max)

        val controller = Controller(context, builder, selectionChangeListener)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 8
            minute = 35
            second = 0
        }

        controller.onTimeSelected(newTime, MINUTE)

        val newDistance: Long =
            controller.selectedDts[END]
                .distance(controller.selectedDts[START])

        val distanceToMax: Long =
            max.distance(controller.selectedDts[START])

        assertNotEquals(oldDistance, newDistance)
        assertEquals(distanceToMax, newDistance)
    }

    @Test
    fun updateStartToEndDistance_selectSecond_newDistanceInvalid_needToRound_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 15
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 30
        }

        val max = DateTime(System.currentTimeMillis())
        max.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 8
            minute = 0
            second = 40
        }

        val oldDistance: Long = end.distance(start)

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)
            .maxEndDt(max)

        val controller = Controller(context, builder, selectionChangeListener)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 8
            minute = 0
            second = 35
        }

        controller.onTimeSelected(newTime, SECOND)

        val newDistance: Long =
            controller.selectedDts[END]
                .distance(controller.selectedDts[START])

        val distanceToMax: Long =
            max.distance(controller.selectedDts[START])

        assertNotEquals(oldDistance, newDistance)
        assertEquals(distanceToMax, newDistance)
    }

    //updatePossibleSelection

    //without min

    @Test
    fun updatePossibleSelection_withoutMin_clickAmPm_endMinChanged_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 11
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        controller.onToggleAmPm(START)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(21, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)
    }

    @Test
    fun updatePossibleSelection_withoutMin_selectYear_endMinChanged_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2014
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        controller.onYearSelected(2015)

        assertEquals(2015, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)
    }

    @Test
    fun updatePossibleSelection_withoutMin_selectMonth_endMinChanged_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.OCTOBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        controller.onMonthSelected(2013, Calendar.NOVEMBER)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.NOVEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)
    }

    @Test
    fun updatePossibleSelection_withoutMin_selectDay_endMinChanged_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 2
            hour24 = 9
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        controller.onDaySelected(2013, Calendar.SEPTEMBER, 10)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(10, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)
    }

    @Test
    fun updatePossibleSelection_withoutMin_selectHour_endMinChanged_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 10
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 11
            minute = 0
            second = 0
        }

        controller.onTimeSelected(newTime, HOUR)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(11, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)
    }

    @Test
    fun updatePossibleSelection_withoutMin_selectMinute_endMinChanged_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 10
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 9
            minute = 20
            second = 0
        }

        controller.onTimeSelected(newTime, MINUTE)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(20, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)
    }

    @Test
    fun updatePossibleSelection_withoutMin_selectSecond_endMinChanged_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 10
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 9
            minute = 0
            second = 20
        }

        controller.onTimeSelected(newTime, SECOND)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(20, controller.minDt[END]?.second)
    }

    //with min

    @Test
    fun updatePossibleSelection_withMin_clickAmPm_endMinChanged_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 11
            minute = 0
            second = 0
        }

        val min = DateTime(System.currentTimeMillis())
        min.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 20
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)
            .minEndDt(min)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(20, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        controller.onToggleAmPm(START)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(21, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        controller.onToggleAmPm(START)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(20, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)
    }

    @Test
    fun updatePossibleSelection_withMin_selectYear_endMinChanged_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2014
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val min = DateTime(System.currentTimeMillis())
        min.apply {
            year = 2015
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)
            .minEndDt(min)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2015, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        controller.onYearSelected(2017)

        assertEquals(2017, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        controller.onYearSelected(2013)

        assertEquals(2015, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)
    }

    @Test
    fun updatePossibleSelection_withMin_selectMonth_endMinChanged_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.OCTOBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val min = DateTime(System.currentTimeMillis())
        min.apply {
            year = 2013
            month = Calendar.NOVEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)
            .minEndDt(min)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.NOVEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        controller.onMonthSelected(2013, Calendar.DECEMBER)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.DECEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        controller.onMonthSelected(2013, Calendar.SEPTEMBER)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.NOVEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)
    }

    @Test
    fun updatePossibleSelection_withMin_selectDay_endMinChanged_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 2
            hour24 = 9
            minute = 0
            second = 0
        }

        val min = DateTime(System.currentTimeMillis())
        min.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 5
            hour24 = 9
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)
            .minEndDt(min)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(5, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        controller.onDaySelected(2013, Calendar.SEPTEMBER, 10)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(10, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        controller.onDaySelected(2013, Calendar.SEPTEMBER, 1)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(5, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)
    }

    @Test
    fun updatePossibleSelection_withMin_selectHour_endMinChanged_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 10
            minute = 0
            second = 0
        }

        val min = DateTime(System.currentTimeMillis())
        min.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 12
            minute = 0
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)
            .minEndDt(min)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(12, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 15
            minute = 0
            second = 0
        }

        controller.onTimeSelected(newTime, HOUR)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(15, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        newTime.apply {
            hour24 = 9
            minute = 0
            second = 0
        }

        controller.onTimeSelected(newTime, HOUR)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(12, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)
    }

    @Test
    fun updatePossibleSelection_withMin_selectMinute_endMinChanged_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 10
            second = 0
        }

        val min = DateTime(System.currentTimeMillis())
        min.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 20
            second = 0
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)
            .minEndDt(min)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(20, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 9
            minute = 40
            second = 0
        }

        controller.onTimeSelected(newTime, MINUTE)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(40, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)

        newTime.apply {
            hour24 = 9
            minute = 0
            second = 0
        }

        controller.onTimeSelected(newTime, MINUTE)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(20, controller.minDt[END]?.minute)
        assertEquals(0, controller.minDt[END]?.second)
    }

    @Test
    fun updatePossibleSelection_withMin_selectSecond_endMinChanged_valueIsValid() {

        val start = DateTime(System.currentTimeMillis())
        start.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 0
        }

        val end = DateTime(System.currentTimeMillis())
        end.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 10
        }

        val min = DateTime(System.currentTimeMillis())
        min.apply {
            year = 2013
            month = Calendar.SEPTEMBER
            day = 1
            hour24 = 9
            minute = 0
            second = 20
        }

        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .selectedStartDt(start)
            .selectedEndDt(end)
            .minEndDt(min)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(20, controller.minDt[END]?.second)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 9
            minute = 0
            second = 40
        }

        controller.onTimeSelected(newTime, SECOND)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(40, controller.minDt[END]?.second)

        newTime.apply {
            hour24 = 9
            minute = 0
            second = 0
        }

        controller.onTimeSelected(newTime, SECOND)

        assertEquals(2013, controller.minDt[END]?.year)
        assertEquals(Calendar.SEPTEMBER, controller.minDt[END]?.month)
        assertEquals(1, controller.minDt[END]?.day)

        assertEquals(9, controller.minDt[END]?.hour24)
        assertEquals(0, controller.minDt[END]?.minute)
        assertEquals(20, controller.minDt[END]?.second)
    }

    //advancePicker

    @Test
    fun advancePicker_start_allowAutoAdvance_allItemsAvailable_valueIsValid() {
        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .currentItem(YEAR)

            .selectedStartDt(DateTime(System.currentTimeMillis()))
            .selectedEndDt(DateTime(System.currentTimeMillis()))

            .allowAutoAdvance(true)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(START, controller.currentRangePart)
        assertEquals(YEAR, controller.currentItem)

        controller.onYearSelected(2013)

        assertEquals(START, controller.currentRangePart)
        assertEquals(MONTH, controller.currentItem)

        controller.onMonthSelected(2013, Calendar.JANUARY)

        assertEquals(START, controller.currentRangePart)
        assertEquals(DAY, controller.currentItem)

        controller.onDaySelected(2013, Calendar.JANUARY, 1)

        assertEquals(START, controller.currentRangePart)
        assertEquals(HOUR, controller.currentItem)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 18
            minute = 45
            second = 45
        }

        controller.onTimeSelected(newTime, HOUR, true)

        assertEquals(START, controller.currentRangePart)
        assertEquals(MINUTE, controller.currentItem)

        controller.onTimeSelected(newTime, MINUTE, true)

        assertEquals(START, controller.currentRangePart)
        assertEquals(SECOND, controller.currentItem)

        controller.onTimeSelected(newTime, SECOND, true)

        assertEquals(START, controller.currentRangePart)
        assertEquals(SECOND, controller.currentItem)
    }

    @Test
    fun advancePicker_end_allowAutoAdvance_allItemsAvailable_valueIsValid() {
        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(END)
            .currentItem(YEAR)

            .selectedStartDt(DateTime(System.currentTimeMillis()))
            .selectedEndDt(DateTime(System.currentTimeMillis()))

            .allowAutoAdvance(true)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(END, controller.currentRangePart)
        assertEquals(YEAR, controller.currentItem)

        controller.onYearSelected(2013)

        assertEquals(END, controller.currentRangePart)
        assertEquals(MONTH, controller.currentItem)

        controller.onMonthSelected(2013, Calendar.JANUARY)

        assertEquals(END, controller.currentRangePart)
        assertEquals(DAY, controller.currentItem)

        controller.onDaySelected(2013, Calendar.JANUARY, 1)

        assertEquals(END, controller.currentRangePart)
        assertEquals(HOUR, controller.currentItem)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 18
            minute = 45
            second = 45
        }

        controller.onTimeSelected(newTime, HOUR, true)

        assertEquals(END, controller.currentRangePart)
        assertEquals(MINUTE, controller.currentItem)

        controller.onTimeSelected(newTime, MINUTE, true)

        assertEquals(END, controller.currentRangePart)
        assertEquals(SECOND, controller.currentItem)

        controller.onTimeSelected(newTime, SECOND, true)

        assertEquals(END, controller.currentRangePart)
        assertEquals(SECOND, controller.currentItem)
    }

    @Test
    fun advancePicker_disallowAutoAdvance_allItemsAvailable_valueIsValid() {
        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .currentItem(YEAR)

            .selectedStartDt(DateTime(System.currentTimeMillis()))
            .selectedEndDt(DateTime(System.currentTimeMillis()))

            .allowAutoAdvance(false)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(START, controller.currentRangePart)
        assertEquals(YEAR, controller.currentItem)

        controller.onYearSelected(2013)

        assertEquals(START, controller.currentRangePart)
        assertEquals(YEAR, controller.currentItem)

        controller.onMonthSelected(2013, Calendar.JANUARY)

        assertEquals(START, controller.currentRangePart)
        assertEquals(YEAR, controller.currentItem)

        controller.onDaySelected(2013, Calendar.JANUARY, 1)

        assertEquals(START, controller.currentRangePart)
        assertEquals(YEAR, controller.currentItem)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 18
            minute = 45
            second = 45
        }

        controller.onTimeSelected(newTime, HOUR, true)

        assertEquals(START, controller.currentRangePart)
        assertEquals(YEAR, controller.currentItem)

        controller.onTimeSelected(newTime, MINUTE, true)

        assertEquals(START, controller.currentRangePart)
        assertEquals(YEAR, controller.currentItem)

        controller.onTimeSelected(newTime, SECOND, true)

        assertEquals(START, controller.currentRangePart)
        assertEquals(YEAR, controller.currentItem)
    }

    @Test
    fun advancePicker_allowAutoAdvance_year_monthDisable_valueIsValid() {
        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .currentItem(YEAR)

            .selectedStartDate(year = 2013, day = 21)
            .selectedStartTime(12, 12, 12)

            .selectedEndDate(year = 2013, day = 21)
            .selectedEndTime(12, 12, 12)

            .allowAutoAdvance(true)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(START, controller.currentRangePart)
        assertEquals(YEAR, controller.currentItem)

        controller.onYearSelected(2013)

        assertEquals(START, controller.currentRangePart)
        assertEquals(YEAR, controller.currentItem)
    }

    @Test
    fun advancePicker_allowAutoAdvance_month_dayDisable_valueIsValid() {
        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .currentItem(MONTH)

            .selectedStartDate(2013, Calendar.JANUARY)
            .selectedStartTime(12, 12, 12)

            .selectedEndDate(2013, Calendar.JANUARY)
            .selectedEndTime(12, 12, 12)

            .allowAutoAdvance(true)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(START, controller.currentRangePart)
        assertEquals(MONTH, controller.currentItem)

        controller.onMonthSelected(2013, Calendar.JANUARY)

        assertEquals(START, controller.currentRangePart)
        assertEquals(MONTH, controller.currentItem)
    }

    @Test
    fun advancePicker_allowAutoAdvance_day_hourDisable_valueIsValid() {
        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .currentItem(DAY)

            .selectedStartDate(2013, Calendar.JANUARY, 1)
            .selectedStartTime(minute = 12, second = 12)

            .selectedEndDate(2013, Calendar.JANUARY, 1)
            .selectedEndTime(minute = 12, second = 12)

            .allowAutoAdvance(true)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(START, controller.currentRangePart)
        assertEquals(DAY, controller.currentItem)

        controller.onDaySelected(2013, Calendar.JANUARY, 1)

        assertEquals(START, controller.currentRangePart)
        assertEquals(DAY, controller.currentItem)
    }

    @Test
    fun advancePicker_allowAutoAdvance_hour_minuteDisable_valueIsValid() {
        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .currentItem(HOUR)

            .selectedStartDate(2013, Calendar.JANUARY, 1)
            .selectedStartTime(hour = 12, second = 12)

            .selectedEndDate(2013, Calendar.JANUARY, 1)
            .selectedEndTime(hour = 12, second = 12)

            .allowAutoAdvance(true)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(START, controller.currentRangePart)
        assertEquals(HOUR, controller.currentItem)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 18
            minute = 45
            second = 45
        }

        controller.onTimeSelected(newTime, HOUR, true)

        assertEquals(START, controller.currentRangePart)
        assertEquals(HOUR, controller.currentItem)
    }

    @Test
    fun advancePicker_allowAutoAdvance_minute_secondDisable_valueIsValid() {
        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .currentRangePart(START)
            .currentItem(MINUTE)

            .selectedStartDate(2013, Calendar.JANUARY, 1)
            .selectedStartTime(12, 12)

            .selectedEndDate(2013, Calendar.JANUARY, 1)
            .selectedEndTime(12, 12)

            .allowAutoAdvance(true)

        val controller = Controller(context, builder, selectionChangeListener)

        assertEquals(START, controller.currentRangePart)
        assertEquals(MINUTE, controller.currentItem)

        val newTime = DateTime(System.currentTimeMillis())
        newTime.apply {
            hour24 = 18
            minute = 45
            second = 45
        }

        controller.onTimeSelected(newTime, MINUTE, true)

        assertEquals(START, controller.currentRangePart)
        assertEquals(MINUTE, controller.currentItem)
    }

    //availability

    @Test
    fun setDateAndTime_availabilityValueIsValid() {
        val builder: PickerBuilder = PickerBuilder(context)
            .selectedDate(2013, Calendar.SEPTEMBER, 1)
            .selectedTime(10, 10, 10)

        val controller = Controller(context, builder, selectionChangeListener)

        assertTrue(controller.startDtItemsAvailability[YEAR])
        assertTrue(controller.startDtItemsAvailability[MONTH])
        assertTrue(controller.startDtItemsAvailability[DAY])

        assertTrue(controller.startDtItemsAvailability[HOUR])
        assertTrue(controller.startDtItemsAvailability[MINUTE])
        assertTrue(controller.startDtItemsAvailability[SECOND])
    }

    @Test
    fun setOnlyDate_availabilityValueIsValid() {
        val builder: PickerBuilder = PickerBuilder(context)
            .selectedDate(2013, Calendar.SEPTEMBER, 1)

        val controller = Controller(context, builder, selectionChangeListener)

        assertTrue(controller.startDtItemsAvailability[YEAR])
        assertTrue(controller.startDtItemsAvailability[MONTH])
        assertTrue(controller.startDtItemsAvailability[DAY])

        assertFalse(controller.startDtItemsAvailability[HOUR])
        assertFalse(controller.startDtItemsAvailability[MINUTE])
        assertFalse(controller.startDtItemsAvailability[SECOND])
    }

    @Test
    fun setOnlyTime_availabilityValueIsValid() {
        val builder: PickerBuilder = PickerBuilder(context)
            .selectedTime(10, 10, 10)

        val controller = Controller(context, builder, selectionChangeListener)

        assertFalse(controller.startDtItemsAvailability[YEAR])
        assertFalse(controller.startDtItemsAvailability[MONTH])
        assertFalse(controller.startDtItemsAvailability[DAY])

        assertTrue(controller.startDtItemsAvailability[HOUR])
        assertTrue(controller.startDtItemsAvailability[MINUTE])
        assertTrue(controller.startDtItemsAvailability[SECOND])
    }

    @Test
    fun setOnlyYear_availabilityValueIsValid() {
        val builder: PickerBuilder = PickerBuilder(context)
            .selectedDate(2013)

        val controller = Controller(context, builder, selectionChangeListener)

        assertTrue(controller.startDtItemsAvailability[YEAR])
        assertFalse(controller.startDtItemsAvailability[MONTH])
        assertFalse(controller.startDtItemsAvailability[DAY])

        assertFalse(controller.startDtItemsAvailability[HOUR])
        assertFalse(controller.startDtItemsAvailability[MINUTE])
        assertFalse(controller.startDtItemsAvailability[SECOND])
    }

    @Test
    fun setOnlyMonth_availabilityValueIsValid() {
        val builder: PickerBuilder = PickerBuilder(context)
            .selectedDate(month = Calendar.SEPTEMBER)

        val controller = Controller(context, builder, selectionChangeListener)

        assertFalse(controller.startDtItemsAvailability[YEAR])
        assertTrue(controller.startDtItemsAvailability[MONTH])
        assertFalse(controller.startDtItemsAvailability[DAY])

        assertFalse(controller.startDtItemsAvailability[HOUR])
        assertFalse(controller.startDtItemsAvailability[MINUTE])
        assertFalse(controller.startDtItemsAvailability[SECOND])
    }

    @Test
    fun setOnlyDay_availabilityValueIsValid() {
        val builder: PickerBuilder = PickerBuilder(context)
            .selectedDate(day = 1)

        val controller = Controller(context, builder, selectionChangeListener)

        assertFalse(controller.startDtItemsAvailability[YEAR])
        assertFalse(controller.startDtItemsAvailability[MONTH])
        assertTrue(controller.startDtItemsAvailability[DAY])

        assertFalse(controller.startDtItemsAvailability[HOUR])
        assertFalse(controller.startDtItemsAvailability[MINUTE])
        assertFalse(controller.startDtItemsAvailability[SECOND])
    }

    @Test
    fun setOnlyHour_availabilityValueIsValid() {
        val builder: PickerBuilder = PickerBuilder(context)
            .selectedTime(10)

        val controller = Controller(context, builder, selectionChangeListener)

        assertFalse(controller.startDtItemsAvailability[YEAR])
        assertFalse(controller.startDtItemsAvailability[MONTH])
        assertFalse(controller.startDtItemsAvailability[DAY])

        assertTrue(controller.startDtItemsAvailability[HOUR])
        assertFalse(controller.startDtItemsAvailability[MINUTE])
        assertFalse(controller.startDtItemsAvailability[SECOND])
    }

    @Test
    fun setOnlyMinute_availabilityValueIsValid() {
        val builder: PickerBuilder = PickerBuilder(context)
            .selectedTime(minute = 10)

        val controller = Controller(context, builder, selectionChangeListener)

        assertFalse(controller.startDtItemsAvailability[YEAR])
        assertFalse(controller.startDtItemsAvailability[MONTH])
        assertFalse(controller.startDtItemsAvailability[DAY])

        assertFalse(controller.startDtItemsAvailability[HOUR])
        assertTrue(controller.startDtItemsAvailability[MINUTE])
        assertFalse(controller.startDtItemsAvailability[SECOND])
    }

    @Test
    fun setOnlySecond_availabilityValueIsValid() {
        val builder: PickerBuilder = PickerBuilder(context)
            .selectedTime(second = 10)

        val controller = Controller(context, builder, selectionChangeListener)

        assertFalse(controller.startDtItemsAvailability[YEAR])
        assertFalse(controller.startDtItemsAvailability[MONTH])
        assertFalse(controller.startDtItemsAvailability[DAY])

        assertFalse(controller.startDtItemsAvailability[HOUR])
        assertFalse(controller.startDtItemsAvailability[MINUTE])
        assertTrue(controller.startDtItemsAvailability[SECOND])
    }

    //HeaderSelectionListener, HeaderRangeSelectionListener

    @Test
    fun withoutRange_setSelectedDateTime_pickHeaderItem_necessaryListenerTriggered() {
        val builder: PickerBuilder = PickerBuilder(context)
            .selectedDateTime(2013, Calendar.SEPTEMBER, 1, 8, 0, 0)

        val controller = Controller(
            context,
            builder,
            selectionChangeListener,
            headerSelectionListener, headerRangeSelectionListener
        )

        assertFalse(isHeaderSelectionListenerCalled)
        assertFalse(isHeaderRangeSelectionListenerCalled)

        controller.onPickHeaderItem(YEAR, START)

        assertTrue(isHeaderSelectionListenerCalled)
        assertFalse(isHeaderRangeSelectionListenerCalled)
    }

    @Test
    fun withRange_setSelectedDateTime_pickHeaderItem_necessaryListenerTriggered() {
        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .selectedStartDateTime(2013, Calendar.SEPTEMBER, 1, 8, 0, 0)
            .selectedEndDateTime(2013, Calendar.SEPTEMBER, 2, 8, 0, 0)

        val controller = Controller(
            context,
            builder,
            selectionChangeListener,
            headerSelectionListener, headerRangeSelectionListener
        )

        assertFalse(isHeaderSelectionListenerCalled)
        assertFalse(isHeaderRangeSelectionListenerCalled)

        controller.onPickHeaderItem(YEAR, END)

        assertFalse(isHeaderSelectionListenerCalled)
        assertTrue(isHeaderRangeSelectionListenerCalled)
    }

    @Test
    fun withoutRange_withAutoAdvance_setSelectedDateTime_onYearSelected_necessaryListenerTriggered() {
        val builder: PickerBuilder = PickerBuilder(context)
            .selectedDateTime(2013, Calendar.SEPTEMBER, 1, 8, 0, 0)
            .currentItem(YEAR)
            .allowAutoAdvance(true)

        val controller = Controller(
            context,
            builder,
            selectionChangeListener,
            headerSelectionListener, headerRangeSelectionListener
        )

        assertFalse(isHeaderSelectionListenerCalled)
        assertFalse(isHeaderRangeSelectionListenerCalled)

        controller.onYearSelected(2014)

        assertTrue(isHeaderSelectionListenerCalled)
        assertFalse(isHeaderRangeSelectionListenerCalled)
    }

    @Test
    fun withRange_withAutoAdvance_setSelectedDateTime_onYearSelected_necessaryListenerTriggered() {
        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .selectedStartDateTime(2013, Calendar.SEPTEMBER, 1, 8, 0, 0)
            .selectedEndDateTime(2013, Calendar.SEPTEMBER, 2, 8, 0, 0)
            .currentItem(YEAR)
            .currentRangePart(END)
            .allowAutoAdvance(true)

        val controller = Controller(
            context,
            builder,
            selectionChangeListener,
            headerSelectionListener, headerRangeSelectionListener
        )

        assertFalse(isHeaderSelectionListenerCalled)
        assertFalse(isHeaderRangeSelectionListenerCalled)

        controller.onYearSelected(2014)

        assertFalse(isHeaderSelectionListenerCalled)
        assertTrue(isHeaderRangeSelectionListenerCalled)
    }

    @Test
    fun withoutRange_setSelectedDateTime_pickHeaderItem_listenerGetValidValues() {
        val builder: PickerBuilder = PickerBuilder(context)
            .selectedDateTime(2013, Calendar.SEPTEMBER, 1, 8, 0, 0)

        val controller = Controller(
            context,
            builder,
            selectionChangeListener,
            headerSelectionListener, headerRangeSelectionListener
        )

        assertEquals(INVALID, item)

        controller.onPickHeaderItem(HOUR, START)

        assertEquals(HOUR, item)
    }

    @Test
    fun withRange_setSelectedDateTime_pickHeaderItem_listenerGetValidValues() {
        val builder: PickerRangeBuilder = PickerRangeBuilder(context)
            .selectedStartDateTime(2013, Calendar.SEPTEMBER, 1, 8, 0, 0)
            .selectedEndDateTime(2013, Calendar.SEPTEMBER, 2, 8, 0, 0)

        val controller = Controller(
            context,
            builder,
            selectionChangeListener,
            headerSelectionListener, headerRangeSelectionListener
        )

        assertEquals(INVALID, rangeItem)
        assertEquals(INVALID, rangePart)

        controller.onPickHeaderItem(HOUR, START)

        assertEquals(HOUR, rangeItem)
        assertEquals(START, rangePart)
    }
}