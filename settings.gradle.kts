rootProject.name = "date-time-picker-android"

include(
    ":date-time-picker",
    ":sample"
)